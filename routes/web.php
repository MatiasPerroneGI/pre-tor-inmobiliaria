<?php
//------------------------
//----------Test----------
//------------------------

use App\Plan;
use Illuminate\Support\Facades\Auth;

Route::get('change-password', function () {
	$user = \App\User::find(1);
	$user->email = env('ADMIN_EMAIL');
	$user->password = bcrypt(env('ADMIN_PASSWORD'));
	$user->save();
	return 'ok';
});



Route::group(['namespace' => 'Test', 'prefix' => 'test', 'middleware' => 'test'], function () {
	Route::get('ejemplo', 'ExampleController');

	Route::get('mail', function () {
		$email = new \App\Mail\TestMail('Lorem', 'ipsum');
		$email->replyTo(env('REPLY_TO_EMAIL'));
		\Mail::send($email);
	});

	// Contracts
	Route::get('contracts/create', 'ContractController@create');

	// suscripiones
	Route::get("/subscribe/cancel/{user}", "SubscriptionController@cancel"); //Cancelar suscripción de usuario
	Route::get("/subscribe/{user}/{plan}", "SubscriptionController@subscribe"); //Suscribir usuario
	Route::get("/subscribe/{user}/change/{plan}", "SubscriptionController@change"); //Cambiar plan de  usuario

	//emails logs
	Route::get('emails-log/json', 'EmailLogController@json');
	Route::resource('emails-log', 'EmailLogController');

	//users
	Route::get('users/json', 'UserController@json');
	Route::get('users/{user}/login-as', 'UserController@loginAs');
	Route::resource('users', 'UserController');
});


//login de admin y clientes
\App\Classes\Auth::routes(['users' => 'perfil']);

//------------------------
//----------Admin---------
//------------------------
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
	//Redirec to admin default route
	Route::get('/', function () {
		return redirect(route('admin.home'));
	});

	// Home
	Route::get('dashboard', 'DashboardController')->name('admin.home');

	// Products
	Route::get('products/json', 'ProductController@json');
	Route::resource('products', 'ProductController');

	// Planes
	Route::get('plans/json', 'PlanController@json');
	Route::resource("plans", 'PlanController')->only(['index', 'edit', 'update']);

	// Users
	Route::get('users/json', 'UserController@json');
	Route::resource('users', 'UserController');

	// Backups
	Route::get('backups', 'BackupController@index');
	Route::get('backup/download/{file_name}', 'BackupController@download');
	Route::get('backup/delete/{file_name}', 'BackupController@delete');

	//Formularios
	Route::get('form/toSelect', 'FormController@toSelect');
});

//manejo de imagenes
Route::patch('admin/images/{image}', 'Admin\ImageController@update');
Route::post('admin/images/upload', 'Admin\ImageController@upload');
Route::post('admin/video/upload', 'Admin\ImageController@uploadVideo');
Route::post('admin/images/{image}/delete', 'Admin\ImageController@destroy');

//-----------------------------
//----------Backoffice---------
//-----------------------------

//verified:backoffice.verification.notice
Route::group(['namespace' => 'User', 'prefix' => 'backoffice', 'middleware' => ['auth:user']], function () {
	//Redirec to admin default route
	Route::get('/', function () {
		return redirect(route('user.home'));
	});

	// Home
	Route::get('dashboard', 'DashboardController')->name('user.home');

	// Contracts
	Route::get('contracts/json', 'ContractController@json');
	Route::resource('contracts', 'ContractController');
	Route::post('contracts/{contract}/confirm', 'ContractController@confirm');

	// Versions
	Route::get('versions/json', 'VersionController@json');
	Route::resource('versions', 'VersionController');

	// Properties
	Route::get('properties/json', 'PropertyController@json');
	Route::resource('properties', 'PropertyController');

	// Chat
	Route::get('chat/json', 'ChatController@json');
	Route::get("chat/searchContacts", 'ChatController@searchContacts');
	Route::get("chat/searchConversations", 'ChatController@searchConversations');

	Route::get("chat/answer/{message}", 'ChatController@answer')->name('chat.answer');
	Route::resource('chat', 'ChatController');

	// Payments
	Route::get('payments/json', 'PaymentController@json');
	Route::resource('payments', 'PaymentController')->only('index', 'edit', 'update');

	// Eventos
	Route::get('events/json', 'EventController@json');
	Route::resource('events', 'EventController')->only('index', 'show', 'destroy');

	// Agenda de eventos
	Route::get('schedules/json', 'ScheduleController@json');
	Route::resource('schedules', 'ScheduleController')->except("show");

	// Usuarios inmo
	Route::get('users/json', 'UserController@json');
	Route::get('users/findByEmail', 'UserController@findByEmail');
	Route::resource('users', 'UserController');

	// Clients
	Route::get('clients/json', 'ClientController@json');
	Route::resource('clients', 'ClientController');

	// Intimations
	Route::get('intimations/json', 'IntimationController@json');
	Route::get('intimations/{intimation}/resolve', 'IntimationController@resolve');
	Route::get('intimations/getIntimatedUsers/{contract}', 'IntimationController@getIntimatedUsers');
	Route::resource('intimations', 'IntimationController');

	Route::get('autocomplete/clients', 'AutocompleteController@clients');
	Route::get('autocomplete/clientes', 'AutocompleteController@clientes');
	Route::get('autocomplete/properties', 'AutocompleteController@properties');
	Route::get('autocomplete/contracts', 'AutocompleteController@contracts');
});

// Files
Route::group(['middleware' => 'auth:user,admin'], function () {
	Route::post('files/upload', 'FileController@upload');
	Route::post('files/{image}/delete', 'FileController@destroy');

	Route::get('files/download/{file}', 'FileController@download');
	Route::get('files/downloadAll/{file}', 'FileController@downloadAll');
});

Route::group(['namespace' => 'Front'], function () {
	//statics
	Route::get('/', 'HomeController');
	Route::view('/registro', 'front.plans', ['plans' => Plan::all()])->name('registro');

	Route::view('aboutus', 'front.static.aboutus');
	Route::view('faq', 'front.static.faq');
});