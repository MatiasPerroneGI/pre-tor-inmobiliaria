KT.create('#datatable', {
    resource: 'emails-log',
    ajax: '/test/emails-log/json',
    columns: [
        'subject',
        'to',
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
			        {
			        	'title': 'Ver',
			        	'class': 'success',
			        	'href': '/test/emails-log/${row.id}',
			        	'icon': 'fa-eye',
			        	'target': "_blank"
			       	},
			    ],
			    delete: false, //booleano para configurar la visibilidad del botón borrar
			    edit: false //booleano para configurar la visibilidad del botón editar
			}
		},
    ]
});