KT.create('#datatable', {
    resource: 'users',
    ajax: '/test/users/json',
    columns: [
        'name',
        'lastname',
        'email',
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
			        {
			        	'title': 'Login As',
			        	'class': 'success',
			        	'href': '/test/users/${row.id}/login-as',
			        	'icon': 'fa-sign-in-alt',
			       	},
			    ],
			    delete: false,
			    edit: false
			}
		},
    ]
});