(function () {
	$.fn.datepicker.dates['es'] = {
	    days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	    daysShort: ['do', 'lu', 'ma', 'mi', 'ju', 'vi', 'sá'],
	    daysMin: ['do', 'lu', 'ma', 'mi', 'ju', 'vi', 'sá'],
	    months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	    monthsShort: ['Ene','Feb','Mar','Abril','Mayo','Jun','Jul','Ago','Sept','Oct','Nov','Dic'],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd-mm-yyyy",
	    titleFormat: "MM yyyy", //Leverages same syntax as 'format'
	    weekStart: 0
	};
    
    $('input[data-datepicker]').datepicker({
    	language: 'es'
    });

    if ($('[data-bind]').length) {
        var name, $this;
        $('[data-bind]').each(function () {
            $this = $(this);
            name = $(this).data('bind');
            $('label[for="'+$this.attr('name')+'"]').append($('<i></i>').attr('class', 'fal fa-spin fa-spinner').hide())
            $('[name="' + name + '"]').on('change', function () {
                addOptions($this, name, $(this).val());
            });
            addOptions($this, name, $('select[name="'+name+'"]').val());
        });
    }

    function addOptions ($select, name, value) {
        $.ajax({
            url: '/admin/form/toSelect',
            type: 'GET',
            data: {
                name: name,
                value: value
            },
            success: function (response) {
                $select.empty(); // remove old options
                $(response).each(function() {
                  $select.append($("<option></option>")
                     .attr("value", this.val).text(this.text));
                });
            }
        });
    }

    $('[data-toggle-triger]').on('change', function () {
        var triger = $(this).data('toggle-triger');
        var prop = $(this).data('toggle-prop');
        toggle[prop]($(this), $('[data-toggle-target="'+triger+'"]'));
    });

    $('body').on('change', '[data-shower-triger]', function () {
        var selected = $(this).val();
        var target = $(this).data('shower-triger');
        var $elements = $('[data-shower-target="'+target+'"]');

        $elements.each(function () {
            var values = $(this).data('shower-value') + '';
            values = values.split(',');
            if (values.indexOf(selected) === -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        //toggle[prop]($(this), $('[data-shower-target="'+triger+'"]'));
    });

    var toggle = {
        disabled: function ($triger, $target) {
            $target.attr('disabled', !$triger.is(':checked'));
        },
        datediff: function ($triger, $target) {
            $target.attr('disabled', !$triger.is(':checked'));
        }
    }

    $(document).ready(function() {
        //init default
        $('.wysiwyg').summernote({
            height: 200,
            tabsize: 2,
            dialogsFade: true,
            toolbar: [
                //['style', ['style']],
                //['font', ['strikethrough', 'superscript', 'subscript']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                //['fontsize', ['fontsize']],
                //['fontname', ['fontname']],
                //['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
                ['table', ['table']],
                //['insert', ['link', 'picture', 'video']],
                ['insert', ['link']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
        });
    });
})();