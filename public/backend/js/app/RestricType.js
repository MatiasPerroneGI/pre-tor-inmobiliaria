(function (w, $, undefined) {
    var type, restrictions = {
        int: function (event) {
            return /[0-9]+/.test(event.key);
        },
        float: function (event) { // ej.: 10.5 - 0.5 - 10 - 10.50
            var checkValue = event.target.value + event.key;
            return /^\d+(\.\d{0,2}){0,1}$/.test(checkValue);
        }
    }

    $('[data-restrict-type]').on('keypress', function (event) {
        type = $(this).data('restrict-type');
        return restrictions[type](event);
    });
})(window, jQuery);