var Autocomplete = (function (w, $, undefined) {

    var Wrapper = function($el) {
        this.$el = $el;
        var prop, $syncElements = $('[data-autocomplete-target="'+$el.data('autocomplete-trigger')+'"]');
        var callbacks = [];

        var dispatch = function (event) {
            if (callbacks[event]) {
                for (var i in callbacks[event]) {
                    callbacks['sync'][i]();
                }
            }
        }

        var sync = function (item) {
            if (item) {
                $syncElements.each(function (i, el) {
                    prop =  $(el).data('autocomplete-prop');
                    if (item[prop]) $(el).val(item[prop]);
                });
            }
            dispatch('sync');
        }

        ajaxSource = function (url) {
            return function (request, response) {
                $.ajax({
                    url: url,
                    type: 'get',
                    data : {
                        searchTerm: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            }
        }

        this.autocomplete = function (source) {
            if (typeof source === 'string') ajaxSource(source);
            this.$el.autocomplete({
                source: source,
                response: function (event, ui) {
                    if (ui.content.length) $(this).data({currentItem:ui.content[0]});
                },
                focus: function (event, ui) {
                    event.target.value = ui.item.label;
                    return false;
                },
                select: function (event, ui) {
                    event.target.value = ui.item.label;
                    sync(ui.item);
                    return false;
                },
                change: function (event, ui) {
                    if (ui.item != null) {
                        event.target.value = ui.item.label;
                        sync($(this).data('currentItem'));
                    }
                },
                _renderItem: function( ul, item ) {
                    return $( "<li>" )
                    .append( "<a>" + item.label+"</a>")
                    .appendTo( ul );
                }
            });
        }

        this.on = function (event, callback) {
            if (callbacks[event]) {
                callbacks[event].push(callback)
            } else {
                callbacks[event] = [callback];
            }
        }
    }

    return {
        create: function ($el, source) {
            var wrapper = new Wrapper($el);
            wrapper.autocomplete(source);
            return wrapper;
        }
    }
})(window, jQuery);