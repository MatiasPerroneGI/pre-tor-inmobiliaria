KT.create('#datatable', {
    resource: 'users',
    ajax: '/admin/users/json',
    columns: [
        'nombre',
        'dni',
        'email',
        {
            column: 'subscriptions',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    return row.current_subscription.length ? row.current_subscription[0].plan.name : 'Sin suscripción'
                }
            }
        },
        'id|actions',
    ]
});