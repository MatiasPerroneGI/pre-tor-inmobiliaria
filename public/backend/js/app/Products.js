KT.create('#datatable', {
    resource: 'products',
    columns: [
		//'thumb|image',
		//'thumb|image:/content/admins/thumb/',
		{
			column: 'thumb',
			filter: 'image',
			setting: {
				path: '/content/admins/thumb/'
			}
		},
        'name|limit:2',
        /*
        {
			column: 'name',
			filter: 'limit',
			setting: {
				length: 2
			}
		},
		*/
		'price',
        'stock',
        'is_visible|stateSwitcher',
        //'id|actions',
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
			        {
			        	'title': 'Estadísticas',
			        	'class': 'success',
			        	'href': '/admin/products/${row.id}/statistics',
			        	'icon': 'fa-chart-bar',
			        	onPress: function (target, row) {
			        		console.log(target, row);
			        	}
			       	}
			    ],
			    /*
			    onDelete: function (target, row) {
			    	console.log(target, row)
			    }
			    */
			}
		},
    ],
    sortable: true
});