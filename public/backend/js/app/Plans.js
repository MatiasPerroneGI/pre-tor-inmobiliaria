KT.create('#datatable', {
    resource: 'plans',
    columns: [
        'name',
        'description|limit',
        'price',
        {
			column: 'id',
			filter: 'actions',
			setting: {
			    delete: false,
			    edit: true
			}
		},
    ]
});