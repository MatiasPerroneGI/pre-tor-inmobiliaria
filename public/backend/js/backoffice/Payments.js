KT.create('#datatable', {
    resource: 'payments',
    ajax: '/backoffice/payments/json',
    columns: [
        'service.value',
        'contract.name',
        'expiration|moment:DD-MM-YYYY',
        {
            column: 'subscriptions',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    var states = {
                        0:{label:'danger', value:'Impago'},
                        1:{label:'success', value:'Pago'}
                    }
                    var currentState = row.payment_date ? states[1] : states[0];
                    return '<span class="badge badge-'+currentState.label+'">'+currentState.value+'</span>';
                }
            }
        },
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
                    {
                        title: 'Pagar',
                        class: 'dark',
                        href: '/backoffice/payments/${row.id}/edit',
                        icon: 'fa-credit-card',
                        showIf: '!row.payment_date'
                    },
			    ],
			    delete:false,
			    edit: false,
			}
		},
    ]
});
