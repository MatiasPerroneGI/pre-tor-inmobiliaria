var Schedule = (function (w, $, undefined) {
    var frequencySelect = $("form select[name='frequency']");
    var periodSelect = $("form select[name='param']");
    var toggleType = $("input[name=toggle_type]");

    function init () {
        setDate();
        fillFrequency();

        listeners();
        toggleFormat(toggleType.is(":checked"));
    }

    function setDate(){
        defaultDate = $('input[name=date]').attr('default') 
            ? new Date($('input[name=date]').attr('default'))
            : new Date();
        defaultDate.setDate(defaultDate.getDate() + 1);
            
        date = new Date();
        date.setDate(date.getDate() + 1);
        $('input[name=date]').datepicker().datepicker("setDate", defaultDate);
        $('input[name=date]').datepicker().datepicker('setStartDate', date);
    }

    function fillFrequency(){
        if (!periodSelect.children().length) {populatePeriod(frequencySelect.children("option:selected").val())}
    }

    function populatePeriod(selected){
        if (!selected) return;

        selected = frequenciesOptions[selected];
        periodValues = selected.values;
        if(!periodValues) {
            periodSelect.val(null);
            periodSelect.fadeOut(100);
        }else{
            periodSelect.html("");
            $.each(periodValues,function(i,el){
                option = `<option value="` + (i) + `">` + el  + `</option>`;
                periodSelect.append(option);
            })
            periodSelect.fadeIn(100);
        }
    }

    //listeners
    function listeners () {
        frequencySelect.change(function(){
            populatePeriod($(this).children("option:selected").val())
        });

        $('.saveForm').on('click', function () {
            $('#form').submit();
        });

        toggleType.change(function(){
            toggleFormat($(this).is(":checked"))
        })
    }

    function toggleFormat(fixedDate){
    	label = $(".custom-switch label");
        if (fixedDate) {
        	label.text(label.attr("data-swchon-text"));
            $(".fixed-container").show(150);
            $(".freq-container").hide(150);
        } else {
        	label.text(label.attr("data-swchoff-text"));
            $(".fixed-container").hide(150);
            $(".freq-container").show(150);
        }
    }

    return {
        init : function () {
            init();
        }
    }
})(window, jQuery, undefined);

$(document).ready(function () {
    Schedule.init();
});
