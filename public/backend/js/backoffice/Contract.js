(function (w, $, undefined) {
    $(document).ready(function () {
        var $form = $('#form-contract')
        var $wzd = $('#wizard-contract'); //wizard
        var textoFinalizar = 'Finalizar';
        var textoSiguiente = 'Siguiente';
        var textoAnterior = 'Anterior';

        // Steps
        w.currentStep = 0;
        w.finalStep = $wzd.children('ul').first().children('li').length - 1;

        function paso1() {
            $wzd.show();
            $wzd.smartWizard({
                selected: w.currentStep, // Initial selected step, 0 = first step
                keyNavigation: true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                autoAdjustHeight: false, // Automatically adjust content height
                cycleSteps: false, // Allows to cycle the navigation of steps
                backButtonSupport: true, // Enable the back button support
                useURLhash: true, // Enable selection of the step based on url hash
                showStepURLhash: true,
                lang: { // Language variables
                    next: textoSiguiente,
                    previous: textoAnterior
                },
                toolbarSettings: {
                    toolbarPosition: 'bottom', // none, top, bottom, both
                    toolbarButtonPosition: 'right', // left, right
                    showNextButton: true, // show/hide a Next button
                    showPreviousButton: true, // show/hide a Previous button
                },
                anchorSettings: {
                    anchorClickable: false, // Enable/Disable anchor navigation
                    enableAllAnchors: false, // Activates all anchors clickable all times
                    markDoneStep: true, // add done css
                    enableAnchorOnDoneStep: true, // Enable/Disable the done steps navigation
                    markAllPreviousStepsAsDone: false, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
                },
                contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
                contentCache: true, //ajax content
                disabledSteps: [], // Array Steps disabled
                errorSteps: [], // Highlight step with errors
                theme: 'default', //dots, default, circles
                transitionEffect: 'fade', // Effect on navigation, none/slide/fade
                transitionSpeed: '280'
            });

            $wzd.on('leaveStep', function (e, obj, currentStep, direction) {
                var $input = $('#step-' + String(currentStep + 1) + ' :input[required]')
                var elementosVacios = $input.filter((ix, elem) => elem.value == '')
                var hayElementosVacios = elementosVacios.length ? true : false

                // console.info('currentStep', currentStep, hayElementosVacios)

                $input.not(elementosVacios).removeClass('is-invalid')
                if (hayElementosVacios)
                {
                    elementosVacios.addClass('is-invalid');
                    elementosVacios.first().focus();
                    if (direction != 'backward')
                    {
                        return false
                    }
                }
            });

            $wzd.on('showStep', function (e, obj, currentStep) {

                w.currentStep = currentStep

                if (w.currentStep < w.finalStep) {
                    $('.sw-btn-next').text(textoSiguiente).removeAttr('finalizar')
                }
                if (w.currentStep == w.finalStep) {
                    var $next = $('.sw-btn-next')
                    $next.text(textoFinalizar)

                    setTimeout(function () {
                        $next.prop('disabled', false);
                        $next.removeAttr('disabled');
                        $next.attr('finalizar');
                    }, 100)
                }
            });

            $(document).on('click', '.sw-btn-next[finalizar]', function (e) {
                $form.submit();
            });

        }

        //----------------------
        //-----pasos 2 3 y 6 ---> Quitar o agregar personas
        //----------------------
        function paso2_3_y_6() {
            var personInfo = {
                owner: {
                    collection: $('#owners'),
                    template: $('#tpl-owner').val()
                },
                tenant: {
                    collection: $('#tenants'),
                    template: $('#tpl-tenant').val()
                },
                guarantor: {
                    collection: $('#guarantors'),
                    template: $('#tpl-guarantor').val()
                },
            };
            var $agregar = $('a[data-add-person]');
            var quitarSelector = 'a[data-remove-person]';

            $agregar.on('click', function (e) {
                e.preventDefault();
                var person = $(this).data('add-person')
                var i  = $('[data-person-box="' + person +'"]').length;
                personInfo[person].collection.append(personInfo[person].template.replace(/\$\{i\}/g, i));
            });

            $(document).on('click', quitarSelector, function (e) {
                e.preventDefault();
                var $this = $(this)
                var person = $this.data('remove-person')

                $this.parents('[data-person-box="' + person +'"]').remove();
            });
        }

        //----------------
        //-----paso 4-----
        //----------------
        // Propiedad?

        //----------------
        //-----paso 5-----
        //----------------
        function paso5() {
            var $contract_duration = $('#contract_duration');
            var $contract_start = $('#contract_start');
            var $contract_end = $('#contract_end');
            var $contract_price = $('#contract_price');
            var $contract_deposit_time = $('#contract_deposit_time');
            var $contract_deposit_value = $('#contract_deposit_value');
            var $contract_increase = $('#contract_increase');
            var $contract_time = $('#contract_time');
            var $contract_total = $('#contract_total');

            function calculateDepositValue() {
                var price = parseFloat($contract_price.val());
                var time = parseFloat($contract_deposit_time.val());

                if (price && time) {
                    $contract_deposit_value.val(price * time);
                }
            }

            function calculateFinalPrice() {
                calculateDepositValue();

                var duration = parseInt($contract_duration.val());
                var increase = parseFloat($contract_increase.val()) || 0;
                var times = parseFloat($contract_time.val()) || 1;
                var price = parseFloat($contract_price.val());
                var deposit = parseFloat($contract_deposit_value.val());
                var total = 0

                if (duration && price) {
                    var parts = Math.ceil(duration / times );
                    var rest = duration % times;
                    var total = times * price; //El primer total no tiene interés

                    //calculo el interés por cada parte del contrato, salvo la primera (que ya calculé) y la última en caso que haya un resto
                    var l = (rest > 0) ? parts - 1 : parts;
                    for (var i=1; i<l; i++) {
                        price = price * (1 + increase / 100);
                        total += times * price;
                    }

                    //si hay resto lo agrego
                    if (rest > 0) {
                        price = price * (1 + increase / 100);
                        total += rest * price;
                    }

                    total += deposit ? deposit : 0;

                }
                $contract_total.val(total);
            }

            function calculateEnd() {
                var months = parseInt($contract_duration.val())
                var startDate = moment($contract_start.val(), "DD-MM-YYYY")

                $contract_end.val((months && startDate.isValid()) ? startDate.add(months, 'M').format('DD-MM-YYYY') : '');
            }

            //calcular fin del contrato
            $contract_duration.on('input', calculateEnd);
            $contract_start.on('change', calculateEnd);

            //calcular valor del depósito
            $contract_price.on('input', calculateDepositValue);
            $contract_deposit_time.on('change', calculateDepositValue);

            //calcular costo total del contrato
            $contract_duration.on('input', calculateFinalPrice);
            $contract_deposit_value.on('input', calculateFinalPrice);
            $contract_increase.on('input', calculateFinalPrice);
            $contract_time.on('input', calculateFinalPrice);
        }

        w.setTimeout(paso1, 100);
        w.setTimeout(paso2_3_y_6, 200);
        w.setTimeout(paso5, 300);


    })
})(window, jQuery);
