KT.create('#datatable', {
    resource: 'intimations',
    ajax: '/backoffice/intimations/json',
    columns: [
        'created_at|moment',
        'contract.name',
        'title|limit',
        {
            column: 'is_resolved',
            filter: 'callback',
            setting: {
                callback: function (row) {
                	if (row.is_resolved) 
            			return `<div class="badge badge-warning p-1">Resuelta</div>`
                	return `<div class="badge badge-danger p-1">No resuelta</div>`
                }
            }
        },
        
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
                  {
                    'showIf': function(row){
                        return !row.is_resolved && row.user_id == userId;
                    },
                    'title': 'Resolver',
                    'class': 'primary',
                    'href': '/backoffice/intimations/${row.id}/resolve', 
                    'icon': 'fa-check'
                  },
                  {
                    'title': 'Ver intimación',
                    'class': 'dark',
                    'href': '/backoffice/intimations/${row.id}',
                    'icon': 'fa-eye'
                   },
                   {
                    'title': 'Ir a conversation',
                    'class': 'dark',
                    'href': '/backoffice/chat/${row.conversation_id}',
                    'icon': 'fa-comment'
                   },
			    ],
			    delete:false,
			    edit: false 
			}
		},
    ]
});
