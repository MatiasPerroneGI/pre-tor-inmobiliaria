KT.create('#datatable', {
    resource: 'clients',
    path:'/content/users/thumb/',
    ajax: '/backoffice/clients/json',
    columns: [
        'images[0].src|image:/content/users/thumb/',
        'nombre|limit',
        'dni',
        'email',
        // 'id|actions',
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [{
					'title': 'Inmuebles',
					'class': 'dark',
					'href': '/backoffice/properties?user_id=${row.id}',
					'icon': 'fa-building'
				},
				{
					'title': 'Crear contrato',
					'class': 'dark',
					'href': '/backoffice/contracts/create?client_id=${row.id}',
					'icon': 'fa-file'
				}],
			    delete: true,
			    edit: true,
			}
		},
    ],
});