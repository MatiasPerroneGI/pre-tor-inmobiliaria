var Client = (function (w, $, undefined) {
    var destinatario = $(".js-data-example-ajax");
	
    function init () {
        $("input[name=email]").change(function(){
            email = $(this).val();
			if(!email){
                $("input[name=sendMail]").attr("disabled",true)
				return removeDisabled();
            }

            $("input[name=sendMail]").removeAttr("disabled")

            // Find by email
            $.ajax({
                url:'/backoffice/users/findByEmail',
                data:{email:email},
                success:function(data){
                    if (data.user) {
                        $("input[name=name]").val(data.user.name).attr("disabled",true);
                        $("input[name=lastname]").val(data.user.lastname).attr("disabled",true);
                        $("input[name=dni]").val(data.user.dni).attr("disabled",true);
                        $("input[name=client_id]").val(data.user.id);
                    }else{
                        removeDisabled();
                    }
                },
                error:function(err){
                    console.error(err);
                }
            });
        })
    }

    function removeDisabled(){
    	// $("input[name=sendMail]").attr("disabled",true)
    	$("input[name=client_id]").val('');
        $("input[name=name], input[name=lastname],input[name=dni]").removeAttr("disabled")
    }

    return {
        init : function () {
            init();
        },
    }
})(window, jQuery, undefined);

$(document).ready(function () {
    Client.init();
});