$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var Chat = (function (w, $, undefined) {
    var destinatario = $(".js-data-example-ajax");

    function init () {
        selectDestinatario();
        sendForm();
        paginacion();
        buscador();
    }

    function buscador(){
        var ajax;

        // on click button clear input
        $(".clear-search").click(function(){
            $(".search-conversations").val("");
            hideResults();
        })

        //  on keypress search conversations
        $(".search-conversations").on('input',function(e){
            elem=$(this);
            let minLength = 3;
            term = this.value;

            // Si no hay texto para buscar escondo resultados
            if(!term.length){
                if (ajax) ajax.abort()
                hideResults();
                return;
            }

            if (term.length >= minLength || elem.hasClass("searched")) {
                if (ajax) ajax.abort()

                ajax = $.ajax({
                    url:'/backoffice/chat/searchConversations',
                    data:{term:term},
                    success:function(data){
                        text="Resultados"
                        if(!data.cant){
                            text="No hay resultados";
                        }
                        $(".conversations-searcher .text").text(text);
                        elem.addClass("searched")
                        $(".conversations-searcher")
                            .css("display",'flex')
                            .find(".notification")
                                .html(data.html);
                    }
                })
            }
        })
    }
    
    // Hide and clear results html
    function hideResults(){
        $(".conversations-searcher")
                    .css("display",'none')
                    .find('.notification')
                        .html("");
        $(".search-conversations").removeClass("searched")
    }

    // Paginacion
    function paginacion(){
        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            let url = $(this).attr('href');  
            getConversations(url, function (data) {
                $('.pagination a:first').attr('href', data.info.prev_page_url);
                $('.pagination a:last').attr('href', data.info.next_page_url);

                $('.paginator-info').html(data.info.from + ' - ' + data.info.to)

                $('.pagination a').each(function () {
                    if (!$(this).attr('href')) {
                        $(this).addClass('disabled')
                    } else {
                        $(this).removeClass('disabled')
                    }
                });
            });
        });
    }

    function getConversations(url, callback) {
        $.ajax({
            url : url,
            dataType: 'json',
        }).done(function (response) {
            $("#js-emails").html(response.html);
            if (callback) callback(response);
        }).fail(function (data) {
            alert('error.');
        });
    }
    
    function sendForm(){
        $("form.message-form").submit(function(){
            text = $.trim($("#fake_textarea").text());
            $("textarea[name=message]").val(text);
    
            // Send message from index view
            if($(this).attr("method") == 'ajax'){
                $.ajax({
                    url:$(this).attr("action"),
                    type:'post',
                    data:$(this).serialize(),
                    success:function(data){
                        $("#panel-compose .close-panel").trigger("click");
                        // Force to go to page 1
                        getConversations('/backoffice/chat?page=1');

                        // Clear modal-compose
                        destinatario.val(null).trigger('change');
                        $("#panel-compose .message-form input[name=subject]").val("");
                        $("#panel-compose .message-form #fake_textarea").text("");
                    },
                    error:function(data){
                        toastr["error"](data.responseJSON.errors[Object.keys(data.responseJSON.errors)[0]][0], "Error")
                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": true,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": 500,
                          "hideDuration": 100,
                          "timeOut": 5000,
                          "extendedTimeOut": 1000,
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    }
                })
                return false;
            }
        })
    }

    function selectDestinatario(){
        destinatario.select2({
            ajax:{
                url: "/backoffice/chat/searchContacts",
                dataType: 'json',
                delay: 250,
                data: function(params){
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params){
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination:{
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "Destinatario",
            escapeMarkup: function(markup){
                return markup;
            }, 
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    
        checkRecipient();
    }

    function checkRecipient(){
        if($("input[name=recipient_id]").val()){
            destinatario.select2().select2("val", null);
            destinatario.select2().select2("val", $("input[name=recipient_id]").val());
            $("input[name=recipient]").val($("input[name=recipient_id]").val());
        }
    }

    function formatRepo(repo){
        if (repo.loading) return repo.text;

        var markup = "<div class='select2-result-repository clearfix d-flex'>" +
            "<div class='select2-result-repository__meta'>"+
            "<div class='select2-result-repository__title fs-lg fw-500'>" + repo.name + "<br></div>"+
            "<div class='select2-result-repository__forks mr-2'><small>" + repo.email+ "</small></div>"+
            "</div></div>";

        return markup;
    }

    function formatRepoSelection(repo){
        return repo.name || repo.text;
    }

    return {
        init : function () {
            init();
        },
    }
})(window, jQuery, undefined);

$(document).ready(function () {
    Chat.init();
});