KT.create('#datatable', {
    resource: 'properties',
    ajax: '/backoffice/properties/json',
    columns: [
        'images[0].src|image',
        'address|limit',
        'area',
        {
            column: 'owners',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    return row.owners.map(owner => `<a data-toggle="tooltip" data-placement="top" title="Perfil" href="/backoffice/users/`+owner.id+`">`+owner.name + ` ` + owner.lastname+`</a>`)
                }
            }
        },
        //'id|actions', //forma abreviada: trae por defecto los botones de editar y borrar
        {
			column: 'id',
			filter: 'actions',
			setting: {
				extraButtons: [
					// {
			  //       	'showIf': 'row.owners.length',
			  //       	'title': 'Perfil dueño',
			  //       	'class': 'dark',
			  //       	'href': '/backoffice/user/${row.owners[0].id}', //TODO
			  //       	'icon': 'fa-user'
			  //      	},

                  {
                    'title': 'Crear contrato',
                    'class': 'dark',
                    'href': '/backoffice/contracts/create?property_id=${row.id}',
                    'icon': 'fa-file'
                   },
			    ],
			    delete:true,
			    edit: true 
			}
		},
    ],
    data: {
    	'user_id': $('input[name="user_id"]').val()
    }
});
