(function () {
	var $component = $('#header-notifications');
	
	$component.on('click', '[data-open]', function () {

		if (!$('ul.notification li', $component).length) {
			showLoader();
			getConversations(function () {
				showConverstions();
			});
			getEvents();
			getIntimations();
		}
	});

	function showLoader() {
		$('.nav-tabs', $component).removeClass('active');
		$('.tab-pane', $component).removeClass('active');

		$('[data-loader]', $component).addClass('active');
	}

	function showConverstions() {
		$('.nav-tabs .nav-link', $component).removeClass('active');
		$('.nav-tabs .nav-item:first .nav-link', $component).addClass('active');
		$('#tab-conversations', $component).addClass('active');
		
		$('[data-loader]', $component).removeClass('active');
	}

	function getConversations(callback) {
		$.ajax({
			url: '/backoffice/chat',
			type: 'get',
			data: {
				view: 'backoffice.chat.partials.conversation-single-header'
			},
			success: function (response) {
				$('[data-conversations]', $component).html(response.html);
				if (callback) callback();
			}
		})
	}

	function getEvents() {
		$.ajax({
			url: '/backoffice/events',
			type: 'get',
			data: {
				view: 'backoffice.events.partials.event-single-header'
			},
			success: function (response) {
				$('[data-events]', $component).html(response.html);
			}
		})
	}

	function getIntimations(){
		$.ajax({
			url:'/backoffice/intimations',
			type:"get",
			success:function(response){
				$('[data-intimations]', $component).html(response.html);
			}
		})
	}
})();