
KT.create('#datatable', {
    resource: 'users',
    ajax: '/backoffice/users/json',
    columns: [
        'images[0].src|image',
        'nombre|limit',
        'dni',
        'email',
        'id|actions',
    ]
});