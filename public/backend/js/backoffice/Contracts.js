KT.create('#datatable', {
    resource: 'contracts',
    ajax: '/backoffice/contracts/json',
    columns: [
        'created_at|moment',
        'name|limit',
        {
            column: 'owners',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    return row.owners.map(owner => owner.name + ' ' + owner.lastname).join(', ')
                }
            }
        },
        {
            column: 'tenants',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    return row.tenants.map(tenant => tenant.name + ' ' + tenant.lastname).join(', ')
                }
            }
        },
        {
            column: 'id',
            filter: 'actions',
            setting: {
                edit: false,
                extraButtons: [
                    {
                        'title': 'Versiones',
                        'class': 'success',
                        'href': '/backoffice/versions?contract_id=${row.id}',
                        'icon': 'fa-list',
                        'showIf': function (row) {
                            return !row.confirmed && row.start >= new Date().toISOString().slice(0,10)
                        }
                    },
                    {
                        'title': 'Crear intimacion',
                        'class': 'success',
                        'href': '/backoffice/intimations/create?contract_id=${row.id}',
                        'icon': 'fa-exclamation-circle',
                        'showIf': function (row) {
                            return row.confirmed && row.start <= new Date().toISOString().slice(0,10)
                        }
                    },
                ]
            }
        },
    ]
});

if (location.hash == '#confirmed') {
    toastr.success('El contrato se confirmó con éxito.', 'Contrato Confirmado', {timeOut: 3000});
    location.hash = '';
}