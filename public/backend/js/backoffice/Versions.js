KT.create('#datatable', {
    resource: 'versions',
    ajax: '/backoffice/versions/json',
    data: {
        contract_id: $('input[name="contract_id"]').val()
    },
    columns: [
        'created_at|moment',
        'updated_at|moment',
        {
            column: 'owners',
            filter: 'callback',
            setting: {
                callback: function (row) {
                    return '<span class="switch-state badge badge-warning">Pendiente</span>'
                }
            }
        },
        {
            column: 'id',
            filter: 'actions',
            setting: {
                edit: false,
                delete: false,
                extraButtons: [
                    {
                        'title': 'Ver',
                        'class': 'success',
                        'href': '/backoffice/versions/${row.id}',
                        'icon': 'fa-eye'
                    },
                    {
                        'title': 'PDF',
                        'class': 'success',
                        'href': '/content/contracts/${row.pdf}',
                        'icon': 'fa-file-pdf'
                    },
                ]
            }
        },
    ]
});