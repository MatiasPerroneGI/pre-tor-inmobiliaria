<?php

$aspectRatio = function ($constraint){
    $constraint->aspectRatio();
    $constraint->upsize();
};

$aspectRatioUpsize = function ($constraint){
    $constraint->aspectRatio();
};

return [
    'properties' => [
        'thumb' => [
            'fit' => [220,220]
        ],
    ],

    'users' => [
        'thumb' => [
            'fit' => [220,220]
        ],
    ],
];
