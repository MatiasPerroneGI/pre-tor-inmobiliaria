<?php

declare(strict_types=1);

return [

    // Subscriptions Database Tables
    'tables' => [

        'plans' => 'plans',
        'plan_features' => 'plan_features',
        'plan_subscriptions' => 'plan_subscriptions',
        'plan_subscription_usage' => 'plan_subscription_usage',

    ],

    // Subscriptions Models
    'models' => [

        'plan' => \App\Plan::class,
        'plan_feature' => \App\PlanFeature::class,
        'plan_subscription' => \App\PlanSubscription::class,
        'plan_subscription_usage' => \App\PlanSubscriptionUsage::class,

    ],

];
