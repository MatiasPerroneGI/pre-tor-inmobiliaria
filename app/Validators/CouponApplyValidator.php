<?php

namespace App\Validators;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Validation\ValidationException;

class CouponApplyValidator
{

    protected static $coupon;

    protected static $error = false;

    protected static $rules = [
        'isValid',
        'isExpired',
        'exceedsMinExpense',
    ];

    public static function validate($coupon)
    {
        self::$coupon = $coupon;

        foreach (self::$rules as $rule) {
            if (self::$error) throw self::$error;
        }
    }

    public static function isValid()
    {
        $hasClient = !self::$coupon->clients()->count() || self::$coupon->hasClient(\Auth::guard('clients')->user());

        if (!self::$coupon || !$hasClient) {
            self::$error = ValidationException::withMessages([
               'not_valid' => ['El cupón ingresado no es válido'],
            ]);
        }
    }

    public static function isExpired()
    {
        if (self::$coupon->expiration != '0000-00-00' && self::$coupon->expiration < date('Y-m-d')) {
            self::$error = ValidationException::withMessages([
               'expire' => ['El cupón ingresado expiró'],
            ]);
        }
    }

    public function exceedsMinExpense()
    {
        if (self::$coupon->min_expense && Cart::subtotal() * 1 < self::$coupon->min_expense) {
            $error = ValidationException::withMessages([
               'min_expense' => ['El cupón sólo es válido para compras superiores a los $' . self::$coupon->min_expense],
            ]);
        }
    }

    public static function exceedsMaximumRefund($value='')
    {
        if (self::$coupon->maximum_refund && Cart::subtotal() * 1 > self::$coupon->maximum_refund) {
            $error = ValidationException::withMessages([
               'maximum_refund' => ['El cupón sólo es válido para un reinegro superiores a los $' . self::$coupon->min_expense],
            ]);
        }
    }
}
