<?php

namespace App\Validators;

use Illuminate\Validation\ValidationException;

class AddToCartValidator
{

    protected static $product;

    protected static $error = false;

    protected static $rules = [
        'hasStock',
        'isVisible'
    ];

    public static function validate($product)
    {
        self::$product = $product;

        foreach (self::$rules as $rule) {
            if (self::$error) throw self::$error;
        }
    }

    public static function hasStock()
    {
        if (self::$product->stock <= 0) {
            self::$error = ValidationException::withMessages([
               'out_of_stock' => ['El producto que intenta agregar al carrito no tiene stock'],
            ]);
        }
    }

    public static function isVisible()
    {
        if (!self::$product->is_visible) {
            self::$error = ValidationException::withMessages([
               'unexpected_error' => ['Ocurrió un error inesperado'],
            ]);
        }
    }
}
