<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes, Tabulate;

    protected $fillable = [
        'address',
        'area',
        'description',
        'accessories',
        'agency_id',
    ];

    public function owners()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('agencia');
    }

    // TODO: Chequear si esta ok. debería ser where not null agencia?
    public function agencies()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('agencia');
    }

    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('order');
    }

    //---Scopes---
    public function scopeOwn($query,$user_id = null)
    {
        return $query->whereHas('owners', function ($query) use ($user_id) {
            // Props de mi cliente
            if ($user_id) {
                $query->where('user_id', $user_id);
                if ($agency = \Auth('user')->user()->agency_id) {
                    $query->whereAgencia($agency); 
                }
                return $query;
            }
            
            $query->where('user_id', \Auth("user")->id());
        });
    }
    //-------------

    //Tabulate options
    public function scopeTabulateQuery($query)
    {
        $query->with('owners', 'images');
    }

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('address', 'like', "%$searchTerm%");
    }

    public function getOrderBy()
    {
        return ['address', 'desc'];
    }
    //----------------
}
