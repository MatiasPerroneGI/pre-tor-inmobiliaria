<?php

namespace App;

use App\Classes\Tabulate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use Tabulate;

    protected $fillable = [
        'name', //nombre del contrato
        'commerce', //booleano de vivienda o comercio
        'activity', //actividad que se desarrollara en el comercio
        'duration', //cuanto dura el contrato
        'start', //cuando empieza
        'end', //cuando termina, conjuncion de inicio + duracion
        'price', //precio del primer tramo de alquiler
        'increase', //porcentaje de actualizacion
        'time', //plazo de actualizacion
        'penalty', //porcentaje de aumento que corre dia a dia tras iniciar la mora
        'deposit_time', //mes deposito (en meses)
        'deposit_value', //mes deposito (valor total)
        'property_id', //propiedad a alquilar
        'total',
        'anticipated_payment', //Pago a mes vencido o mes actual (boolean)
        'payment_method_id', //Pago a mes vencido o mes actual (boolean)
        'extra_data',
        'confirmed',
    ];

    protected $dates = ['start', 'end'];

    //relationships
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function insurance()
    {
        return $this->hasOne(Insurance::class);
    }

    //datos del fiador del modelo Guarantor //si el guarantor es aseguradora debe llenarse en el nombre  de insurance:_carrier automaticamente, sino se pedira direccion y matricula de la propiedad en garantia
    public function guarantors()
    {
        return $this->hasMany(Guarantor::class);
    }

    public function periods()
    {
        return $this->hasMany(Period::class)->orderBy('step', 'desc');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function versions()
    {
        return $this->hasMany(Version::class);
    }

    public function owners()
    {
        return $this->belongsToMany(User::class, 'owners')->withPivot('legal_address');
    }

    public function tenants()
    {
        return $this->belongsToMany(User::class, 'tenants')->withPivot('legal_address');
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }
    //--------------------

    //Getters y setters
    public function setStartAttribute($value)
    {
        $this->attributes['start'] = Carbon::createFromFormat('d-m-Y', $value);
    }

    public function setEndAttribute($value)
    {
        $this->attributes['end'] = Carbon::createFromFormat('d-m-Y', $value);
    }

    //TODO: remover cuando esté corregido el pdf
    public function getGuarantorAttribute()
    {
        return $this->guarantors->first();
    }
    //-----------------

    public function getCurrentMonthAttribute()
    {
        return $this->start->diffInMonths(Carbon::now()) + 1;
    }

    public function getCurrentPeriodAttribute()
    {
        $now = Carbon::now();
        return $this->periods->first(function ($period) use ($now) {
            return $now->between($period->start, $period->end);
        });
    }

    //---Helpers---
    public function notifiables()
    {
        //TODO: acá agregar lógica para saber que partes del contrato tiene que ser notificadas ej: if ($contract->hasAgency) {...}
        return $this->owners->concat($this->tenants)->concat($this->guarantors);
    }

    public function userIsOwner($user_id = null)
    {
        $user_id = $user_id ?? \Auth()->id();
        return $this->owners->first(function ($user) use ($user_id) {
            return $user->id == $user_id;
        });
    }

    public function userIsTenant($user_id = null)
    {
        $user_id = $user_id ?? \Auth()->id();
        return $this->tenants->first(function ($user) use ($user_id) {
            return $user->id == $user_id;
        });
    }

    public function hasAgency()
    {
        return $this->property->agency_id;
    }

    public function getIntimatedUsers()
    {
        $users = [];
        $currentUser = \Auth()->user();

        if ($currentUser->agency_id || $this->userIsOwner()) {
            $users = $this->tenants;
        } else {
            // Soy inquilino
            if ($this->hasAgency()) {
                // Agrego a todos los usuarios de la agencia
                // $users = collect([new \App\User( ['name'=>'Inmobiliaria','lastname'=>$this->property->agency->name])]);
                $users = collect(\App\User::where("agency_id", $this->property->agency_id)->with("agency")->get());
            } else {
                // Agrego a los owners
                $users = $this->owners;
            }
        }
        return $users;
    }
    //-------------

    //---Scopes---
    public function scopeOwn($query)
    {
        $user_id = \Auth::id() ?? request()->user()->id;

        $query = $query->whereHas('owners', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        })->orWhereHas('tenants', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        });

        if ($agency_id = (\Auth()->user()->agency_id)) {
            $query->orWhereHas("property", function ($query) use ($agency_id) {
                $query->where("agency_id", $agency_id);
            });
        }

        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('confirmed', 1)->whereDate('start', '<=', date('Y-m-d'))->whereDate('end', '>=', date('Y-m-d'));
    }
    //-------------

    //---Tabulate---
    public function scopeTabulateQuery($query)
    {
        return $query->with('owners', 'tenants');
    }

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('name', 'like', "%$searchTerm%");
    }

    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    //-------------
}