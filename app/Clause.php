<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class Clause extends Model
{
    use Tabulate;

    private $contractPreloaded;
    
    protected $fillable = [
        'title',
        'content',
        'type',
        'approved',
        'edited',
        'version_id',
    ];

    public function version()
    {
        return $this->belongsTo(Version::class);
    }

    public static function cloneFromTemplate($newContents=false)
    {
        $clauses = \App\ClauseTemplate::current()->get();

        return $clauses->map(function ($clause, $i) use ($newContents) {
            return new \App\Clause([
                'title' => $clause->title,
                'content' => ($newContents && isset($newContents[$i]) && $clause->type == 'editable') ? $newContents[$i] : $clause->content,
                'type' => $clause->type,
            ]);
        });
    }

    public function render()
    {
        $response = $this->content;

        if ($this->type == 'template') {
            $contract = $this->getContractPreloaded();
            $response = view('templates.' . $this->content, compact('contract'))->render();
        }

        return $response;
    }

    public function getContractPreloaded()
    {
        if (!$this->contractPreloaded) {
            $this->contractPreloaded = $this->version->contract->load('owners', 'tenants', 'guarantors', 'services');
        }
        return $this->contractPreloaded;
    }

    //Tabulate options
    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    //----------------
}
