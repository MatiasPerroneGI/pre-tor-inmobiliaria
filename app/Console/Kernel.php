<?php

namespace App\Console;

use App\Jobs\AddPaymentsToContracts;
use App\Jobs\EventsGeneratorJob;
use App\Jobs\Notifications\ContractAboutToExpire;
use App\Jobs\Notifications\ContractExpired;
use App\Jobs\Notifications\ContractStart;
use App\Jobs\Notifications\UnpaidContract;
use App\Jobs\Notifications\UnpaidServices;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Schema;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sitemap:generate')->sundays();
        $schedule->command('backup:run --only-files')->saturdays();
        $schedule->command('backup:run --only-db')->dailyAt('23:30');
        $schedule->job(new ContractStart)->dailyAt('0:20'); //Notifica el contrato próximo a inicar
        $schedule->job(new ContractAboutToExpire)->dailyAt('0:40'); //Notifica el contrato próximo a vencer
        $schedule->job(new ContractExpired)->dailyAt('1:00'); //Notifica que el contrato ya venció
        $schedule->job(new AddPaymentsToContracts)->dailyAt('1:20'); //Agrega los payments para que se puedan pagar los servicios
        $schedule->job(new UnpaidServices)->monthlyOn(11, '1:40'); //Notifica que hay servicios impagos
        $schedule->job(new UnpaidContract)->monthlyOn(11, '2:00'); //Notifica que el pago del alquiler actual no está pago
        
        if (Schema::hasTable('schedules')) {
            // Eventos con frecuencia
            \App\Schedule::whereNotNull('frequency')->get()->each(function ($userSchedule) use ($schedule) {
                $callbackEvent = $schedule->job(new EventsGeneratorJob($userSchedule));
                $arguments = (strlen($userSchedule->param)) ? explode(',', $userSchedule->param) : [];
                call_user_func_array([$callbackEvent, $userSchedule->frequency], $arguments);
            });

            // Eventos con fecha fija
            \App\Schedule::whereDate('date',\Carbon\Carbon::today())->get()->each(function($userSchedule) use ($schedule){
                $schedule->job(new EventsGeneratorJob($userSchedule))->daily();
            });
        }
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
