<?php

namespace App;

use Rinvex\Subscriptions\Models\PlanFeature as RinvexPlanFeature;

class PlanFeature extends RinvexPlanFeature
{
    protected $cacheLifetime = 0;

    public function cacheQuery($builder, array $columns, \Closure $closure)
    {
    	return false;
    }
}
