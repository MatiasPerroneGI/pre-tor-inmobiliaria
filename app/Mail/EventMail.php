<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventMail extends Mailable
{
    use Queueable, SerializesModels;

    
    public function __construct($event)
    {
        $this->event = $event;
    }

    public function build()
    {
        $to = (config('app.env') == 'local') ? env('MAIL_TEST') : $this->event->user->email;

        return $this->markdown('mails.event')
            ->with(['event'=>$this->event])
            ->subject($this->event->subject)
            ->to($to)
        ;
    }
}
