<?php

namespace App\Mail\Transport;

use Illuminate\Mail\Transport\Transport;
use Illuminate\Support\Facades\Log;
use Swift_Mime_SimpleMessage;
use App\Emails;

class DBTransport extends Transport
{
    public function __construct()
    {
        
    }

    public function send(Swift_Mime_SimpleMessage $message, &$failedRecipients = null)
    {
        \App\EmailLog::create([
            'body'    => $message->getBody(),
            'to'      => implode(',', array_keys($message->getTo())),
            'subject' => $message->getSubject()
        ]);
    }
}
