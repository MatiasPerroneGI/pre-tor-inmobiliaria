<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContractConfirmedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $contract;
    private $recipents;

    public function __construct($contract, $recipents)
    {
        $this->contract = $contract;
        $this->recipents = $recipents;
    }

    public function build()
    {
        return $this->markdown('mails.contract-confirmed')
            ->subject('El conrato ' . $this->contract->name . ' fue confirmado.')
            ->to($this->recipents)
        ;
    }
}
