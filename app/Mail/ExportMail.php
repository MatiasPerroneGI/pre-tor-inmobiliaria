<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExportMail extends Mailable
{
    use Queueable, SerializesModels;

    public $election;
    public $excel;

    public function __construct($election, $excel)
    {
        $this->election = $election;
        $this->excel = $excel;
    }

    public function build()
    {
        return $this->markdown('mails.export')->attach(storage_path('app/attachments/'.$this->excel));
    }
}
