<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $prop1;
    public $prop2;

    public function __construct($prop1, $prop2)
    {
        $this->prop1 = $prop1;
        $this->prop2 = $prop2;
    }

    public function build()
    {
        $this->validate();

        return $this->markdown('mails.test')->to(env('CONTACT_EMAIL'));
    }

    private function validate()
    {
        $variables = ['CONTACT_EMAIL', 'REPLY_TO_EMAIL'];

        foreach ($variables as $variable) {
            if (!env($variable)) {
                throw new \Exception("Te falta agregar la variable de entorno $variable en tu archivo .env");
            }
        }
    }
}
