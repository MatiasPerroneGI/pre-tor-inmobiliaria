<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlanChangedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $users, $plan, $changed;

    public function __construct($users,$plan,$changed){
        $this->users = $users;
        $this->plan = $plan;
        $this->changed = $changed;
    }

    public function build(){
        $to = (config('app.env') == 'local') ? [env('MAIL_TEST')] : $this->users;

        return $this->markdown('mails.planChanged')
            ->with(['plan'=>$this->plan,'changed'=>$this->changed])
            ->subject(config('app.name').' | Cambio de plan '.$this->plan->name)
            ->bcc($to)
        ;
    }
}
