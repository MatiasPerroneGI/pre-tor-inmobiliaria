<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($user,$pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    public function build()
    {
        $to = (config('app.env') == 'local') ? env('MAIL_TEST') : $this->user->email;

        return $this->markdown('mails.newUser')
            ->with(['user'=>$this->user,'pass'=>$this->pass])
            ->subject('Bienvenido a '.config('app.name'))
            ->to($to)
        ;
    }
}
