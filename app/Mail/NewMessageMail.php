<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    private $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function build()
    {
        $to = $this->message->recipient()->email;
        $mail = $this->markdown('mails.newMessage')
            ->with(['message'=>$this->message])
            ->subject('Nuevo mensaje - '. $this->message->conversation->subject)
            ->to($to)
        ;

        foreach($this->message->files as $file){
            $mail->attach(public_path().'/storage/'.$file->src, [
                'as' => $file->original_name,
            ]);
        }

        return $mail;
    }
}
