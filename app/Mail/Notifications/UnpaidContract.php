<?php

namespace App\Mail\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnpaidContract extends Mailable
{
    use Queueable, SerializesModels;

    private $contract;

    public function __construct($contract)
    {
        $this->contract = $contract;
    }

    public function build()
    {
        return $this->subject('El mes de alquiler está impago')->markdown('mails.notifications.unpaid-contract');
    }
}
