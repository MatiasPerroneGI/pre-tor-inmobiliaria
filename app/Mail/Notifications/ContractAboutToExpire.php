<?php

namespace App\Mail\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContractAboutToExpire extends Mailable
{
    use Queueable, SerializesModels;

    private $contract;

    public function __construct($contract)
    {
        $this->contract = $contract;
    }

    public function build()
    {
        return $this->subject('Contrato próximo a finalizar')->markdown('mails.notifications.contract-about-to-expire');
    }
}
