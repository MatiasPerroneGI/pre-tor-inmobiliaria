<?php

namespace App\Mail\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnpaidServices extends Mailable
{
    use Queueable, SerializesModels;

    private $contract;

    public function __construct($contract)
    {
        $this->contract = $contract;
    }

    public function build()
    {
        return $this->subject('Tenés facturas impagas')->markdown('mails.notifications.unpaid-services');
    }
}
