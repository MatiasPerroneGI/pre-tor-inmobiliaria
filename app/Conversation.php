<?php

namespace App;

use App\conversation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	use SoftDeletes;

    protected $fillable = ['subject'];

    private $integranteCache = null;

    public function getIntegrantesAttribute()
    {
        $user_id = \Auth()->id() ?? request()->user()->id;

        if (!$this->integranteCache) {
            $integrante = $this->participants()
                ->where('user_id','!=',$user_id)
                ->with("user")
                ->get()
                ->pluck("user")
                // ->first()
                // ->user
            ;
            $this->integranteCache = $integrante;
        }

        return $this->integranteCache;
    }

    public function getIntegrantesNamesAttribute(){
        return ($this->integrantes)->map(function ($item, $key) {
            return $item->name.' '.$item->lastname;
        })->implode(", ");
    }

    // La vista chat/show.blade muestra los mensajes en orden ascendente
    public function messages()
    {
    	return $this->hasMany(Message::class);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->latest();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'participants');
    }

    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    public function intimation()
    {
        return $this->hasOne(Intimation::class);
    }
}
