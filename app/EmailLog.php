<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    use Tabulate;

	protected $table = 'emails_log';

    protected $fillable = [
        'body',
        'to',
        'subject'
    ];

    //Tabulate options
    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('subject', 'like', "%$searchTerm%");
    }

    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    //----------------
}
