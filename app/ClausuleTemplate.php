<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class ClauseTemplate extends Model
{
    use Tabulate;

    protected $table = 'clause_templates';
    
    protected $fillable = [
        'title',
        'content',
        'type',
        'order',
        'is_visible',
    ];

    //Scopes
    public function scopeCurrent($query)
    {
        $query->where('is_visible', 1)->orderBy('order');
    }
    //------

    //Tabulate options
    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    //----------------
}
