<?php

namespace App\Policies;

use App\Admin;
use App\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function viewAny(Admin $user)
    {
        return $user->can('viewAny categories');
    }

    public function view(Admin $user, Category $category)
    {
        return $user->can('view categories');
    }

    public function create(Admin $user)
    {
        return $user->can('create categories');
    }

    public function update(Admin $user, Category $category)
    {
        return $user->can('update categories');
    }

    public function delete(Admin $user, Category $category)
    {
        return $user->can('delete categories');
    }

    public function restore(Admin $user, Category $category)
    {
        return $user->can('restore categories');
    }

    public function forceDelete(Admin $user, Category $category)
    {
        return $user->can('forceDelete categories');
    }
}
