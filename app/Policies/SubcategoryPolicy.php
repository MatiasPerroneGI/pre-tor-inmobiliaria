<?php

namespace App\Policies;

use App\Admin;
use App\Subcategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubcategoryPolicy
{
    use HandlesAuthorization;

    public function viewAny(Admin $user)
    {
        return $user->can('viewAny subcategories');
    }

    public function view(Admin $user, Subcategory $subcategory)
    {
        return $user->can('view subcategories');
    }

    public function create(Admin $user)
    {
        return $user->can('create subcategories');
    }

    public function update(Admin $user, Subcategory $subcategory)
    {
        return $user->can('update subcategories');
    }

    public function delete(Admin $user, Subcategory $subcategory)
    {
        return $user->can('delete subcategories');
    }

    public function restore(Admin $user, Subcategory $subcategory)
    {
        return $user->can('restore subcategories');
    }

    public function forceDelete(Admin $user, Subcategory $subcategory)
    {
        return $user->can('forceDelete subcategories');
    }
}
