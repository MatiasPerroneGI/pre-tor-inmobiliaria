<?php

namespace App\Policies;

use App\Admin;
use App\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function viewAny(Admin $user)
    {
        return $user->can('viewAny products');
    }

    public function view(Admin $user, Product $product)
    {
        return $user->can('view products');
    }

    public function create(Admin $user)
    {
        return $user->can('create products');
    }

    public function update(Admin $user, Product $product)
    {
        return $user->can('update products');
    }

    public function delete(Admin $user, Product $product)
    {
        return $user->can('delete products');
    }

    public function restore(Admin $user, Product $product)
    {
        return $user->can('restore products');
    }

    public function forceDelete(Admin $user, Product $product)
    {
        return $user->can('forceDelete products');
    }
}
