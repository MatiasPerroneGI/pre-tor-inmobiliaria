<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rinvex\Subscriptions\Models\PlanSubscription as RinvexPlanSubscription;

class PlanSubscription extends RinvexPlanSubscription
{
    protected $cacheLifetime = 0;

    public function cacheQuery($builder, array $columns, \Closure $closure)
    {
    	return false;
    }

    public function __construct(array $attributes = [])
    {
    	parent::__construct($attributes);
    }

    public static function bootCacheableEloquent(): void
    {
        static::updated(function (Model $cachedModel) {
            $cachedModel::forgetCache();
        });

        static::created(function (Model $cachedModel) {
            $cachedModel::forgetCache();
        });

        static::deleted(function (Model $cachedModel) {
            $cachedModel::forgetCache();
        });
    }
}
