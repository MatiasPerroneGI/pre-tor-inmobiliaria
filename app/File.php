<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $fillable = ['src','fileable_id','fileable_type','pending','original_name'];

    public function fileable()
    {
        return $this->morphTo();
    }

    public function getFilenameAttribute()
    {
        return $this->original_name ? $this->original_name : $this->src;
    }

    public static function nextId ()
    {
        if (!$file = self::select('id')->orderBy('id', 'desc')->first()) {
            $file = new self;
            $file->id = 0;
        }
        return $file->id + 1;
    }
}
