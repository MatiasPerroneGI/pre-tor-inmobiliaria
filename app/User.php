<?php

namespace App;

use App\Classes\Tabulate;
use App\Notifications\User\ResetPasswordNotification;
use App\Notifications\User\VerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Spatie\Permission\Traits\HasRoles;
use App\Conversation;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,
        SoftDeletes,
        HasRoles,
        HasSubscriptions,
        Tabulate;

    public const TENANT = 'tenant';
    public const OWNER = 'owner';
    public const GUARANTOR = 'guarantor';

    protected $guard_name = 'user';
    protected $fillable = ['name', 'lastname', 'dni', 'cuit', 'email', 'password', 'agency_id'];
    protected $hidden = ['password', 'remember_token'];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification);
    }

    //Relaciones
    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    // Chat relations
    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }

    public function owners()
    {
        return $this->hasMany(Owner::class);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class)
            ->withPivot('agencia');
    }

    public function subscriptions()
    {
        return $this->morphMany(PlanSubscription::class, 'user');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
    //----------

    public function currentSubscription()
    {
        $now = now();
        return $this->subscriptions()
            ->where('ends_at', '>', $now)            // Not ended or
            ->orWhere('trial_ends_at', '>', $now)     // En período de prueba
        ;
    }

    public function conversations()
    {
        // Get conversations order by lastMessage -> created_at desc
        return $this->belongsToMany(Conversation::class, 'participants')
            ->selectRaw("conversations.*, (SELECT created_at from messages WHERE messages.conversation_id = conversations.id ORDER BY created_at DESC limit 1) as latest_message_on")
            ->orderBy("latest_message_on", 'DESC');
    }

    // TODO: Obtener contactos relacionando al usuario con sus contratos vigentes
    public function contacts()
    {
        return User::all()->except(\Auth()->id());
    }

    //getters y setters
    public function getThumbAttribute()
    {
        return $this->image ?? 'imagen-no-disponible.jpg';
    }

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->lastname;
    }

    public function hasAgency()
    {
        return $this->agency;
    }

    //Tabulate options
    public function scopeTabulateQuery($query)
    {
        return $query->selectRaw("concat(name, ' ' ,lastname) as nombre, users.*")->with('currentSubscription.plan', 'images');
    }

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('name', 'like', "%$searchTerm%");
    }

    public function getOrderBy()
    {
        return ['name', 'asc'];
    }
    //----------------
}