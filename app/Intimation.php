<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class Intimation extends Model
{
	use Tabulate;

    protected $fillable = [
    	'conversation_id',
    	'contract_id',
    	'user_id', //Solo el usuario que crea la intimación puede resolverla. También para conocer qué parte creó la intimación (inquilino o dueño/inmo)
    	'is_resolved',
    	'include_guarantors', //Cuando se crea y se resuelve (asumo) se notifica a los garantes por mail
        'title', //la desripción de la intimación esta en el primer mensaje de la conversacion
    ];

    public function files()
    {
        return $this->morphMany(\App\File::class, 'fileable');
    }

    public function conversation()
    {
        return $this->belongsTo(\App\Conversation::class);
    }

    public function contract()
    {
    	return $this->belongsTo(\App\Contract::class);
    }

    public function user()
    {
    	return $this->belongsTo(\App\User::class);
    }

    //getters y setters
    public function getDescriptionAttribute(){
        return $this->conversation ? $this->conversation->messages()->first()->message : '';
    }

    //-------------
    //Tabulate options
    public function scopeTabulateQuery($query)
    {
        $query->with('user','contract');
    }

    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
	//----------------
}
