<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{

    protected $fillable = [
        'title',
        'view',
        'content',
    ];

    //Generar comentarios, modelo migracion y relacion, cada version puede tener multiples comentarios de ambas partes

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}
