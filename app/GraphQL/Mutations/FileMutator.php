<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FileMutator
{
    public function upload($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $resource = strtolower($args['resource']);

        $response = [];

        \Log::debug(count($args['files']));

        foreach ($args['files'] as $file) {
            $name =  $resource . '.' . \App\File::nextId() . '.' . $file->getClientOriginalExtension();

            $file->storeAs("public/files/resource", $name);

            $response[] = \App\File::create(['src' => "files/$resource/$name", 'original_name' => $file->getClientOriginalName()]);
        }

        return $response;
    }
}
