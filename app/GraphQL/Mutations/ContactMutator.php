<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ContactMutator
{
    public function myConversations($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return $context->user()->conversations()->with(['lastMessage' => function ($query) {
                $query->withCount('files');
            }])
            ->with(['participants' => function ($query) use ($context) {
                $query->where('user_id','!=', $context->user()->id)->with('user');
            }])
        ;
    }

    public function search($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $term = $args['q'];

        return \App\User::where('id', '!=', \Auth()->id())
            ->where(function ($query) use ($term) {
                $query->where('name', 'like', "%$term%")
                    ->orWhere('email', 'like', "%$term%");
            })
            ->limit(10)
            ->orderBy('name')
            ->get()
        ;
    }

    public function markConversationAsRead($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $conversation = \App\Conversation::where('id', $args['id'])->first();
        
        return $conversation->messages()
            ->where("read",0)
            ->where("user_id",'!=', $context->user()->id)
            ->update(['read'=>1])
        ;
    }
}
