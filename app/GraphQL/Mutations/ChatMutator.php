<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Version
{
    use VersionTrait;

    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $version = \App\Version::findOrFail($args['contract_id']);

        return $this->createNewVersionOfContract($version);
    }
}
