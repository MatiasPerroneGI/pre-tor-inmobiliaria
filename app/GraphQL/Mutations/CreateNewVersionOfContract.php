<?php

namespace App\GraphQL\Mutations;

use App\Classes\Version as VersionTrait;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CreateNewVersionOfContract
{
    use VersionTrait;

    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        request()->request->add(['contents' => $args['contents']]); //Esto no es la mejor alternativa

        $version = \App\Version::findOrFail($args['version_id']);

        //TODO: este user id está hardcodeado, hace falta agregar la autenticación para graphql
        $user_id = $context->user()->id;
        return $this->createNewVersionOfContract($version, $user_id);
    }
}
