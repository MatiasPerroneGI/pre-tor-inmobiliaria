<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    // type 0, garante por bienes propios
    // type 1, garante con propiedad, requiere address y plate
    // type 2, aseguradora, solo requiere nombre ¿cuil? y email

    protected $fillable = [
        'legal_address',
        'contract_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}
