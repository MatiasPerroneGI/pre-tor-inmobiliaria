<?php

namespace App\Events;

use App\Agency;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AgencyCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $agency;
    protected $creator;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Agency $agency, User $creator)
    {
        $this->agency = $agency;
        $this->creator = $creator;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}