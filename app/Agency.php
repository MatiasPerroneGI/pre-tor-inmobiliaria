<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    // type 0, garante por bienes propios
    // type 1, garante con propiedad, requiere address y plate
    // type 2, aseguradora, solo requiere nombre ¿cuil? y email

    protected $fillable = [
        'name',
        'cuit',
        'legal_address',
        'img',
        'user_id',
    ];

    public function clients()
    {
        return $this->belongsToMany(User::class, 'clients', 'agency_id', 'client_id');
    }

    public function setCuitAttribute($cuit)
    {
        $this->cuit = str_replace('-', '', $cuit);
    }
}