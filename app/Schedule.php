<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classes\Tabulate;

class Schedule extends Model
{
    use Tabulate;

    protected $fillable = [
        'subject',
        'message',
        'date',
        'frequency',
        'param',
        'user_id',
    ];

    // protected $dates = ['date'];

    private static $frequenciesTags=[
        'daily'=>[
            'title'=>'Todos los días',
            'values'=>null,
        ],
        'weeklyOn'=>[
            'title'=>'Todas las semanas',
            'values'=>['domingos','lunes','martes','miércoles','jueves','viernes','sábados']
        ],
        'monthlyOn'=>[
            'title'=>'Todos los meses',
            'values'=>[1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18,19=>19,20=>20,21=>21,22=>22,23=>23,24=>24,25=>25,26=>26,27=>27,28=>28,29=>29,30=>30,31=>31]
        ],
    ];

    // Relations
    public function user(){
        return $this->belongsTo(\App\User::class);
    }

    public function events(){
        return $this->hasMany(\App\Event::class);
    }

    //Tabulate options
    public function scopeTabulateQuery($query)
    {
        return $query->where('user_id',\Auth()->user()->id);
    }

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('subject', 'like', "%$searchTerm%");
    }

    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    // End tabulate options

    // Attributes
    public function getFrecuenciaAttribute(){
        if ($this->date)
            return $this->date;
        
        $index = $this->frequency;
        $text = $this::$frequenciesTags[$index]['title'];
        
        if ($this->param) {
            $params = '';
            foreach (explode(',', $this->param) as $p) $params .= $this::$frequenciesTags[$index]['values'][$p - 1].', ';
            $text.=': '.substr($params,0,strlen($params)-2);
        }
        return $text;
    }

    public static function getFrequenciesOptions(){
        return json_encode(self::$frequenciesTags);
    }

    public function getFrequenciesSelect(){
        $select = [];
        foreach ($this::$frequenciesTags as $title => $freq) 
            $select[$title] = $freq['title'];
        
        return $select;
    }

    public function getPeriodSelect(){
        return $this->frequency && $this::$frequenciesTags[$this->frequency]['values'] ? $this::$frequenciesTags[$this->frequency]['values'] : [];
    }
}
