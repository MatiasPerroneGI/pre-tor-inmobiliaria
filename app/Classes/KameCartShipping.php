<?php

namespace App\Classes;

class KameCartShipping
{
	public $description;
	public $price;
	public $options;

	public function __construct($description, $price, $options=[])
	{
		$this->description = $description;
		$this->price = $price;
		$this->options = (object)$options;
	}

	public function total($decimals = null, $decimalPoint = null, $thousandSeperator = null)
	{
		return $this->price($decimals, $decimalPoint, $thousandSeperator);
	}
	
	public function price($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->price, $decimals, $decimalPoint, $thousandSeperator);
    }

    private function numberFormat($value, $decimals, $decimalPoint, $thousandSeperator)
    {
        if (is_null($decimals)) {
            $decimals = config('cart.format.decimals', 2);
        }

        if (is_null($decimalPoint)) {
            $decimalPoint = config('cart.format.decimal_point', '.');
        }

        if (is_null($thousandSeperator)) {
            $thousandSeperator = config('cart.format.thousand_separator', ',');
        }

        return number_format($value, $decimals, $decimalPoint, $thousandSeperator);
    }

    public function toArray()
    {
    	return [
    		'description' => $this->description,
    		'price' => $this->price,
    		'options' => (array) $this->options,
    	];
    }
}