<?php

namespace App\Classes;

trait Related
{
    public function addRelation($id)
    {
        if ($this->id == $id) return false;

        $min = min($this->id, $id);
        $max = max($this->id, $id);

        $relation = Related::firstOrCreate([
            ['related1', $min],
            ['related2', $max],
            ['type', self::class]
        ]);

        $relation->increment('times');
    }

    public function getRelated()
    {
        $key = 'related-' .\Str::slug(self::class) . '-' . $this->id;

        if (\Cache::has($key)) return \Cache::get($key);

        $related1 = Related::select('related2_id as id')
            ->where('related1_id', $this->id)
            ->orderBy('times', 'desc')
            ->limit(20)
            ->get()
        ;

        $related2 = Related::select('related1_id as id')
            ->where('related2_id', $this->id)
            ->orderBy('times', 'desc')
            ->limit(20)
            ->get()
        ;

        $relatedAll = $related1->merge($related2);
        $ids = $relatedAll->pluck('id')->toArray();

        $related = self::whereIn('id', $ids)
            ->where('id', '!=', $this->id)
            ->orderByRaw('FIND_IN_SET(id, "'.implode(',',  $ids) . '")')
            ->limit(10)
            ->get()
        ;

        \Cache::put($key, $related, 60 * 60 * 24);

        return $related;
    }
}