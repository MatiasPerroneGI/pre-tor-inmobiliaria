<?php

namespace App\Classes;

trait Tabulate
{
    public function scopeFiltered($query)
    {
        $query->tabulateQuery();

        if ($searchTerm = request()->input('search.value')) $query->searchQuery($searchTerm);

        return $query;
    }

    public function scopeTabulated($query)
    {
        $largo = request()->input('length', false);
        $saltar = request()->input('start', 0);
        $filtered = $largo ?
            $query->filtered()
            ->take($largo)
            ->skip($saltar) : $query->filtered();

        return $filtered->orderBy(...$this->getOrderBy());;
    }

    public function scopeTabulateQuery($query)
    {
        return $query;
    }

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query;
    }

    private function getOrderBy()
    {
        return [request()->input('order.0.column'), request()->input('order.0.dir')];
    }
}