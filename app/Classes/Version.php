<?php

namespace App\Classes;

use App\Jobs\ContractToVersion;

trait Version
{
    public function createNewVersionOfContract($version, $user_id=null)
    {
    	//PDF
        $pdf = \PDF::loadView('backoffice.contracts.pdf', ['version' => $version]);
        $versionNumber = $version->contract->versions()->count() + 1;
        $pdf->save(public_path('content/contracts/contract.' . $version->contract->id . '.' . $versionNumber . '.pdf'));

    	$newVersion = ContractToVersion::dispatchNow($version->contract, $user_id);

    	$newClauses = \App\Clause::cloneFromTemplate(request()->input('contents'));

        foreach ($version->clauses as $key => $clause) {
            if ($clause->type == 'editable' && $newClauses[$key]->content != $clause->content) {
                $newClauses[$key]->edited = 1;
            }
        }

    	$newVersion->clauses()->createMany($newClauses->toArray());

    	return $newVersion;
    }
}
