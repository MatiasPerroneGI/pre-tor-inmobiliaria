<?php

namespace App\Classes;

class KameForm
{
    public function model($model, $options){
        return \Form::model($model, $options);
    }

    public function text($name, $options = [])
    {
        return $this->input(\Form::text($name, null, $options), $name);
    }

    public function file($name, $options = [])
    {
        return $this->input(\Form::file($name, null, $options), $name);
    }

    public function textarea($name, $options = []){
        return $this->input(\Form::textarea($name, null, $options), $name);
    }

    public function select($name, $values=[], $options = [])
    {
        $options = array_merge($options, ['class' => 'custom-select form-control']);

        return $this->input(\Form::select($name, $values, null, $options), $name);
    }

    public function editable($name, $options = [])
    {
        $options = array_merge($options, ['class' => 'wysiwyg form-control']);

        return $this->input(\Form::textarea($name, null, $options), $name);
    }

    public function datepicker($name, $options = [])
    {
        $options = array_merge($options, ['class' => 'datepicker form-control']);
        
        return $this->input(\Form::text($name, null, $options), $name);
    }

    public function radio($name, $value=1, $checked=false, $options=[])
    {
        return $this->radioCheckboxInput(\Form::radio($name,$value,$checked,$options+['class'=>'custom-control-input','id'=>$name]), $name, 'radio');
    }

    public function checkbox($name, $value=1, $checked=false, $options=[])
    {
        return $this->radioCheckboxInput(\Form::checkbox($name,$value,$checked,$options+['class'=>'custom-control-input','id'=>$name]), $name, 'checkbox');
    }

    public function submit($name, $options=[])
    {
        return '
            <div class="col-md-12">
                <button type="submit" class="btn btn-success waves-effect waves-themed">'.$name.'</button>
            </div>
        ';
    }

    public function close()
    {
        return \Form::close();
    }

    private function input($input, $name)
    {
        return new KameFormInput($input, $name);
    }

    private function radioCheckboxInput($input, $name, $type)
    {
        return new KameFormRadioCheckbox($input, $name, $type);
    }
}