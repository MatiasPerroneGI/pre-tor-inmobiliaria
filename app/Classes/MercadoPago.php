<?php

namespace App\Classes;

class MercadoPago extends \MP
{
    private $preference;

    public function __construct($inscription)
    {
        $this->preference = $this->create($inscription);
    }

    public function getId()
    {
        return $this->preference['response']['id'];
    }

    public function getUrl()
    {
        return (env('MP_SANDBOX')) ? $this->preference['response']['sandbox_init_point'] : $this->preference['response']['init_point'];
    }

    private function create($inscription)
    {
        $mpItems = [[
            'id' => $inscription->id,
            'title' => $inscription->tournament->title,
            'description' => \Str::limit($inscription->tournament->info, 80),
            'picture_url' => url('content/tournaments/500x500/'.$inscription->tournament->thumb),
            'quantity' => 1,
            'currency_id' => 'ARS',
            'unit_price' => $inscription->tournament->ars * 1
        ]];

        //MP: preferencia de pago
        $preferenceData = [
            'order' => [
                'type' => 'mercadopago',
                'id' => $inscription->id
            ],
            'items' => $mpItems,
            'payer' => [
                'name' => request()->input('name'),
                'surname' => request()->input('lastname'),
                'email' => request()->input('email')
            ],
            'shipments' => [
                'local_pickup' =>  true,
            ],
            "auto_return"=>"approved",
            'back_urls' => [
                'success' => url('inscripcion/exitosa'),
                'failure' => url('inscripcion/cancelada')
            ],
            'external_reference' => $inscription->id,
            'notification_url' => url('webhooks/mercadopago'),
            'id' => uniqid()
        ];

        return self::create_preference($preferenceData);
    }

    public static function webhook()
    {
        if (isset($_GET['type']) && $_GET['type'] == 'payment' && isset($_GET['data_id'])) {
            $paymentId = $_GET['data_id'];
            $accessToken = env('MP_ACCESS_TOKEN');

            $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.mercadopago.com/v1/']);
            $response = $client->get("payments/$paymentId?access_token=$accessToken");

            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody());
                $pagado = $response->transaction_amount;

                $inscription = \App\Inscription::find($response->external_reference);
                if ($pagado >= $inscription->tournament->ars) {
                    $inscription->payed = 1;
                    $inscription->save();
                }
            }
        }
    }
}
