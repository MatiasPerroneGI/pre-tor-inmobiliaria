<?php

namespace App\Classes;

use Gloudemans\Shoppingcart\Cart;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Session\SessionManager;

class KameCart extends Cart
{
	public function __construct(SessionManager $session, Dispatcher $events)
	{
		parent::__construct($session, $events);
	}
	
	public function hasCoupon()
	{
		return session()->has('kamecart.coupon');
	}

	public function getCoupon()
	{
		return \App\Coupon::find(session()->get('kamecart.coupon'));
	}

	public function applyCoupon($coupon)
	{
		$coupon = is_object($coupon) ? $coupon : \App\Coupon::where('code', $coupon)->first();

        $this->setGlobalDiscount($coupon->discount);

		session()->put('kamecart.coupon', $coupon->id);
	}

	public function hasShipping()
	{
		return session()->has('kamecart.shipping');
	}

	public function shipping()
	{
		$shipping = session()->get('kamecart.shipping');

		return new KameCartShipping($shipping['description'], $shipping['price'], $shipping['options']);
	}

	public function setShipping($description, $price, $options=[])
	{
		$data = compact('description', 'price', 'options');

		session()->put('kamecart.shipping', $data);
	}

	public function toCollection()
	{
		return [
			'initial' => $this->initial(),
			'total' => $this->total(),
			'count' => $this->count(),
			'discount' => $this->discount(),
			'shipping' => $this->hasShipping() ? $this->shipping()->total() : 0,
		];
	}
}