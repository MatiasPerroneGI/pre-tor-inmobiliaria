<?php

namespace App\Classes;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PayPal
{
    private $tournament;
    private $apiContext;
    private $payment;

    public function __construct($inscription)
    {
        $this->tournament = $inscription->tournament;
        $this->setApiContext();
        $this->createPayment();
    }

    public function getId()
    {
        return $this->payment ? $this->payment->id : null;
    }

    public function getUrl()
    {
        return $this->payment ? $this->payment->getApprovalLink() : url('inscripcion/cancelada');
    }

    private function setApiContext()
    {
        $credential = new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret'));
        $this->apiContext = new ApiContext($credential);
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    private function createPayment()
    {
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item = new Item();

        $item->setName($this->tournament->title)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($this->tournament->usd)
        ;

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($this->tournament->usd)
        ;

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($this->tournament->title)
            ->setInvoiceNumber(uniqid())
        ;

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('inscripcion/exitosa'))
            ->setCancelUrl(url('inscripcion/cancelada'))
        ;

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction])
        ;

        try {
            $payment->create($this->apiContext);
            $this->payment = $payment;
        } catch (\Exception $ex) {
            //to do
        }
    }

    public static function webhook()
    {
        \Log::debug(print_r(request()->all(), true));
    }
}
