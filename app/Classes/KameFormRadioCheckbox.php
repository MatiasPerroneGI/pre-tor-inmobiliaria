<?php

namespace App\Classes;

use Illuminate\Support\HtmlString;

class KameFormRadioCheckbox extends HtmlString
{
    private $tpl = '
        <div class="mb-3 col-md-12">
        	<div class="custom-control custom-${type}">
	            ${input}
	            <label class="custom-control-label" for="${for}">${label}</label>

	            <div class="invalid-feedback"></div>
            	${help}
	        </div>  
        </div>
    ';

    private $name;
    private $type;

    private $extraData = '';

    public function __construct($html, $name, $type)
    {
        $this->html = str_replace('${input}', $html, $this->tpl);

        $this->name = $name;
        $this->type = $type;
    }

    public function help($text='')
    {
        $tpl = '<div class="help-block">${text}</div>';

        $this->html = str_replace('${help}', $tpl, $this->html);
        $this->html = str_replace('${text}', $text, $this->html);

        return $this;
    }

    public function id($id)
    {
        $this->id = $id;

        return $this;
    }

    public function label($label)
    {
        $this->label = $label;

        return $this;
    }

    public function col($sizes='')
    {
        $sizes = 'col-' . str_replace(' ', ' col-', $sizes);

        $this->html = str_replace('col-md-12', $sizes, $this->html);

        return $this;
    }

    public function bind($name)
    {
        $this->extraData .= 'data-bind="'. $name .'"';

        return $this;
    }

    public function toHtml()
    {
        $this->parse();

        return preg_replace('/\$\{[a-zA-Z0-9_-]+\}/', '', $this->html);
    }

    private function parse()
    {
        $id = $this->id ?? $this->name;
        $label = $this->label ?? \Str::title(__('kameform.' . $this->name));

        $replace = (strstr($this->html, 'form-control') !== false) ? '<$1 $2 ${data} id="'.$id.'">' : '<$1 $2 ${data} class="form-control" id="'.$id.'">';

        $this->html = preg_replace('/<(input|select|textarea)(\b[^><]*)>/i', $replace, $this->html);

        $this->html = str_replace('${type}', $this->type, $this->html);
        $this->html = str_replace('${for}', $id, $this->html);
        $this->html = str_replace('${label}', $label, $this->html);

        if ($this->extraData) {
            $this->html = str_replace('${data}', $this->extraData, $this->html);
        }
    }
}