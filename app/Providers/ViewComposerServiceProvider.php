<?php

namespace App\Providers;

use App\Http\ViewComposers\FormComposer;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //admin
        view()->composer('*kame-form*', FormComposer::class);
    }

    public function register()
    {
        //
    }

}
