<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        */
        'App\Events\ContractCreated' => [
            // 'App\Listeners\NotifyContractCreation',
            'App\Listeners\ContractToVersion',
            'App\Listeners\AddClausesToVersion',
            'App\Listeners\ContractToPdf',
        ],
        'App\Events\ContractConfirmed' => [
            // 'App\Listeners\NotifyContractConfirmation',
            'App\Listeners\AddPaymentsToContract',
        ],
        // 'App\Events\OwnerCreated' => [
        //     'App\Listeners\SendVerificationMail',
        // ],
        // 'App\Events\TenantCreated' => [
        //     'App\Listeners\SendVerificationMail',
        // ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}