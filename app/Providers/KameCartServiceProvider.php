<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class KameShopServiceProvider extends ServiceProvider
{
    public function register()
    {
        //----------------------
        //-----Dependencies-----
        //----------------------

        //kamecart
        $this->app->bind('kamecart', 'App\Classes\KameCart');

        //kameform
        $this->app->bind('kameform', 'App\Classes\KameForm');

        //----------------
        //-----Config-----
        //----------------

        //kamecart
        $config = config_path('cart.php');
        $this->mergeConfigFrom($config, 'cart');

        //----------------
        //-----Events-----
        //----------------

        //kamecart
        $this->app['events']->listen(Logout::class, function () {
            if ($this->app['config']->get('cart.destroy_on_logout')) {
                $this->app->make(SessionManager::class)->forget('cart');
            }
        });
    }
}
