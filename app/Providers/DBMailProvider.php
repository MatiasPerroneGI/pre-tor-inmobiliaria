<?php

namespace App\Providers;

use App\Mail\Transport\DBTransport;
use Illuminate\Mail\MailServiceProvider;

class DBMailProvider extends MailServiceProvider
{

    public function register()
    {
        parent::registerIlluminateMailer();
        parent::registerMarkdownRenderer();

        if ($this->app['config']['mail.driver'] == 'db') {
            $this->registerDBSwiftMailer();
        } else {
            parent::registerSwiftMailer();
        }
    }

    private function registerDBSwiftMailer()
    {
        /*
        $this->app['swift.mailer'] = $this->app->share(function ($app) {
            return new \Swift_Mailer(new DBTransport());
        });
        */


        //----
        $this->app->singleton('swift.mailer', function () {

            return new \Swift_Mailer(new DBTransport());
        });
    }



    public function boot()
    {
        //
    }
}
