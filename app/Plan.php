<?php

namespace App;

use App\Classes\Tabulate;
use Rinvex\Subscriptions\Models\Plan as RinvexPlan;

class Plan extends RinvexPlan
{
    use Tabulate;

    protected $cacheLifetime = 0;

    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('name->es', 'like', "$searchTerm%");
    }

    public function isForAgencies()
    {
        return boolval($this->getFeatureBySlug('is_agency_plan') ?: false);
    }

    private function getOrderBy()
    {
        return ['price', 'asc'];
    }
}