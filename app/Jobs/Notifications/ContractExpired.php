<?php

namespace App\Jobs\Notifications;

use App\Mail\Notifications\ContractExpired as Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ContractExpired implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {
        //
    }

    public function handle()
    {
        $date = \Cabon\Carbon::now()->subDays(1)->format('Y-m-d');
        $contracts = \App\Contract::where('end', $date)->with('property', 'owners', 'tenants', 'guarantors')->get();

        $contracts->each(function ($contract) {
            $mail = new Notification($contract);
            $contract->notifiables()->each(function ($notifiable) use ($mail) {
                Mail::to($notifiable)->send($mail);
            });
        });
    }
}
