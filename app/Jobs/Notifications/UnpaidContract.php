<?php

namespace App\Jobs\Notifications;

use App\Mail\Notifications\UnpaidContract as Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnpaidContract implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {
        //
    }

    public function handle()
    {
        $contracts = \App\Contract::whereHas('servicies', function ($query) {
            $query->where('id', 1)->whereHas('payments', function ($query) {
                $query->where('expiration', '<=', date('Y-m-d'))->whereNotNull('payment_date');
            });
        });

        $contracts->each(function ($contract) {
            $mail = new Notification($contract);
            Mail::to($contract->tenants)->send($mail);
        });
    }
}
