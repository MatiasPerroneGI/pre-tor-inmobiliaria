<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ContractToVersion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contract;
    private $user_id;

    public function __construct($contract, $user_id = null)
    {
        $this->contract = $contract;
        $this->user_id = $user_id;
    }

    public function handle()
    {
        $versionNumber = $this->contract->versions()->count() + 1;

        $version = $this->contract->versions()->create([
            'pdf' => 'contract.' . $this->contract->id . '.' . $versionNumber . '.pdf',
            'content' => '',
            'number' => $versionNumber,
            'user_id' => $this->user_id ?? \Auth::id()
        ]);


        $view = view('backoffice.contracts.pdf', ['version' => $this->contract->versions->last()])->render();
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($view);
        $nodes = $dom->getElementsByTagName('p');

        $text = '';

        foreach ($nodes as $node) {
            $text .= '<p>' . $node->textContent . '</p>';
        }

        $version->update(['content' => $text]);

        return $version;
    }
}
