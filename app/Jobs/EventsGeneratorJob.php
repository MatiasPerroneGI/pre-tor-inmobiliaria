<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Event;
use App\Mail\EventMail;

class EventsGeneratorJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $schedule;

    public function __construct($schedule)
    {
        $this->schedule = $schedule;
    }
    
    public function handle()
    {
        if ($this->schedule->frequency == null) $this->schedule->delete();

        $event = Event::create([
            'subject' => $this->schedule->subject,
            'message' => $this->schedule->message,
            'date' => date('Y-m-d'),
            'user_id' => $this->schedule->user_id,
            'schedule_id' => $this->schedule->id,
        ]);

        $email = new EventMail($event);
        \Mail::send($email);
    }
}
