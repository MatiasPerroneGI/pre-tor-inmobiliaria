<?php

namespace App\Jobs;

use App\Contract;
use App\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
* AddPaymentsToContract agrega los payments a los contratos.
* Si el job recibe un contract será a ese al que le agregue los payments y pondrá como expire 10 días luego del start del mismo.
* Si el job no recibe un contract agregará los payments a todos los contracto activos y marcará como expire el 10 del mes actual.
*/

class AddPaymentsToContracts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contract;

    public function __construct($contract = null)
    {
        $this->contract = $contract;
    }

    public function handle()
    {
        $expiration = ($this->contract) ? $this->contract->start->startOfMonth() : \Carbon\Carbon::now();
        $expiration = $expiration->addDays(10);
        
        if ($this->contract) {
            $contracts = collect([$this->contract->load('services')]);
        } else {
            $contracts = Contract::acvite()->with('services')->get();
        }

        $contracts->each(function ($contract) use ($expiration) {
            $payments = $contract->services->map(function ($service) use ($contract, $expiration) {
                return new Payment([
                    'service_id' => $service->id,
                    'contract_id' => $contract->id,
                    'tenants_id' => $contract->tenants->pluck('id'),
                    'expiration' => $expiration
                ]);
            });

            $payments[] = new Payment([
                'service_id' => 1,
                'contract_id' => $contract->id,
                'tenants_id' => $contract->tenants->pluck('id'),
                'expiration' => $expiration
            ]);

            $contract->payments()->saveMany($payments);
        });
    }
}
