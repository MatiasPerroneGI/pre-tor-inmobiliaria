<?php

namespace App;

use App\Classes\Unite;
use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Unite, Tabulate;

    protected $fillable = [
        'value',
    ];

    const CANON = 1;

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    //Tabulate options
    public function scopeSearchQuery($query, $searchTerm)
    {
        return $query->where('value', 'like', "%$searchTerm%");
    }
}
