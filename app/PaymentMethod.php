<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $fillable = ['value'];
    public $timestamps = false;

    const EFECTIVO = 1;
    const TRANSFERENCIA = 2;
}
