<?php

namespace App;

use Rinvex\Subscriptions\Models\PlanSubscriptionUsage as RinvexPlanSubscriptionUsage;

class PlanSubscriptionUsage extends RinvexPlanSubscriptionUsage
{
    protected $cacheLifetime = 0;
}
