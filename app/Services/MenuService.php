<?php

namespace App\Services;

use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth;

class MenuService
{
    private $prefijo = '';

    public function get($guard)
    {
        $menu = [];
        $user = Auth::guard($guard)->user();
        switch ($guard) {
            case 'admin':
                $menu = $user ? $this->_admin($user) : $menu;
                break;
            case 'user':
                $menu = $user ? $this->_user($user) : $menu;
                break;
        }
        return $menu;
    }

    private function _admin(Admin $user)
    {
        $menu = [];

        // Producción
        $this->prefijo = 'admin';
        $menu[] = $this->_menu('dashboard', 'Home', 'home dashboard perfil');
        $menu[] = $this->_menu('plans', 'Planes de Pago', 'planes pago');
        $menu[] = $this->_menu('subscriptions', 'Subscripciones', 'planes pago suscripciones subscripciones');

        // Dev
        $this->prefijo = 'test';
        $menu[] = $this->_menu('-', null, null, false);
        $menu[] = $this->_menu('emails-log', 'Log de emails', 'mails', false);
        $menu[] = $this->_menu('users', 'Login as', 'usuarios login', false);

        $this->prefijo = 'admin';
        $menu[] = $this->_menu('-');
        $menu[] = $this->_menu('logout', 'Salir (Admin)', 'salir logout logoff');

        return array_filter($menu);
    }

    private function _user(User $user)
    {
        $menu = [];
        $tienePropietarios = $user->owners()->count() ? true : false;
        $tieneInquilinos = $user->tenants()->count() ? true : false;

        // Producción
        $this->prefijo = 'backoffice';
        $menu[] = $this->_menu('dashboard', 'Home', 'home dashboard perfil');
        $menu[] = $this->_menu('contracts', 'Contratos', 'contratos alquileres');

        $menu[] = $user->hasAgency() ? $this->_menu('clients', 'Clientes', 'clientes') : null;
        $menu[] = ($user->hasAgency() || $tienePropietarios) ?
            $this->_menu('properties', 'Inmuebles', 'propiedades departamentos locales casas') : null;
        $menu[] = ($user->hasAgency() || $tienePropietarios) ?
            $this->_menu('locatarios', 'Locatarios', 'propiedades departamentos locales casas') : null;

        $menu[] = $this->_menu('chat', 'Mensajes', 'chat conversacion mensajes');
        $menu[] = $this->_menu('intimations', 'Intimaciones', 'intimaciones');
        $menu[] = $this->_menu('payments', ($tieneInquilinos ? 'Pagos' : 'Recibos') . ' de servicios y alquiler', 'pagos impuestos servicios alquiler');
        $menu[] = $this->_menu('schedules', 'Calendario', 'eventos recordatorios notificaciones programar');
        $menu[] = $user->hasAgency() ? $this->_menu('-') : null;
        $menu[] = $user->hasAgency() ? $this->_menu('users', 'Usuarios', 'usuarios login') : null;

        return array_filter($menu);
    }

    private function _menu($link, $titulo = null, $tags = null, $produccion = true)
    {
        return [
            'prefijo' => $this->prefijo,
            'link' => $link,
            'titulo' => $titulo,
            'tags' => $tags,
            'produccion' => $produccion,
        ];
    }
}