<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class UserService
{
    public function name($logueadoComoUsuario = true)
    {
        $nombre = '';
        if (Auth::guard('user')->check() and Auth::guard('admin')->check()) {
            if ($logueadoComoUsuario)
                $nombre = Auth::guard('admin')->user()->name . ' (Como ' . Auth::guard('user')->user()->name . ')';
            else
                $nombre = Auth::guard('user')->user()->name;
        } else {
            $nombre = $this->_user()->name;
        }
        return $nombre;
    }

    public function thumb()
    {
        $thumb = 'imagen-no-disponible.jpg';
        $user = $this->_user();
        $thumbDir = "/content/" . (Auth::guard('admin')->check() ? 'admins' : 'users') . "/thumb/";
        if (
            $user and
            property_exists($user, 'thumb') and
            file_exists(public_path($thumbDir . $user->thumb))
        ) {
            $thumb = $user->thumb;
        }
        return $thumbDir . $thumb;
    }

    public function logoutRoute()
    {
        return route((Auth::guard('user')->check() ? 'backoffice' : 'admin') . '.logout');
    }

    private function &_user()
    {
        $user = Auth::guard('user')->check() ? Auth::guard('user')->user() : Auth::guard('admin')->user();
        return $user;
    }
}