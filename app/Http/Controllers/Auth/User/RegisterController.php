<?php

namespace App\Http\Controllers\Auth\User;

use App\Agency;
use App\Events\AgencyCreated;
use App\Events\UserRegistered;
use App\User;
use App\Http\Controllers\Controller;
use App\Plan;
use App\PlanSubscription;
use App\PlanSubscriptionUsage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GlobalInnovation\Validation\Rules\CUIT;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = 'backoffice';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'numeric', 'unique:users'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'tyc' => ['required'],
        ];

        if (request()->input('premium')) {
            $rules = $rules + [
                'agency.cuit' => ['required', 'unique:agencies', 'regex:/^[0-9]{2}-?[0-9]{8}-?[0-9]$/', new CUIT],
                'agency.name' => 'required',
                'agency.legal_address' => 'required',
            ];
        }
        return Validator::make($data, $rules);
    }

    protected function create(array $data)
    {
        $user = null;
        $agency = null;
        $subscription = null;
        $plan = $this->getPlanOrRedirect();

        DB::transaction(function () use (&$data, &$user, &$agency, &$subscription, &$plan) {
            $user = User::create([
                'name' => $data['name'],
                'lastname' => $data['lastname'],
                'dni' => $data['dni'],
                'email' => $data['email'],
                'image' => $data['image'] ?? '',
                'password' => Hash::make($data['password']),
            ]);

            // $isAgency = request()->input('premium') ? true : false;

            if ($plan->isForAgencies()) {
                $agency = new Agency($data['agency']);
                $agency->user_id = $user->id;
                $agency->save();

                $user->update(['agency_id' => $agency->id]);
            }

            // Crear suscripción al plan elegido
            // $subscription = new PlanSubscription() Como funciona??
        });

        if ($user) {
            event(new UserRegistered($user));
        }
        if ($agency) {
            event(new AgencyCreated($agency, $user));
        }
        if ($subscription) {
            // event(new PlanSubscriptionGenerated($user, $agency, $plan));
        }

        return $user;
    }

    public function showRegistrationForm()
    {
        $plan = $this->getPlanOrRedirect();
        return view('backoffice.auth.register' . ($plan->isForAgencies() ? 'Agencies' : ''), ['plan' => $plan]);
    }

    protected function guard()
    {
        return Auth::guard('user');
    }

    protected function &getPlanOrRedirect()
    {
        static $plan = null;
        if (!intval(request()->input('plan'))) {
            redirect('registro');
        }
        if (!$plan) {
            $plan = Plan::find(intval(request()->input('plan')));
        }
        return $plan;
    }
}