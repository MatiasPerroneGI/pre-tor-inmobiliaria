<?php

namespace App\Http\Controllers\User;

use App\Schedule;
use App\Http\Controllers\AppController;
use Carbon\Carbon;

class ScheduleController extends AppController
{
    protected $class = Schedule::class;
    protected $viewFolder = 'backoffice.schedules';
    protected $mustValidate = true;

    public function beforeSave($schedule)
    {
    	$schedule->user_id = \Auth()->id();

    	return $this->setDate($schedule);
    }

    private function setDate($schedule)
    {
    	if (request()->toggle_type) {
            $schedule->frequency = null;
            $schedule->param = null;
            $schedule->date = Carbon::create(request()->date);
        } else {
            $schedule->date = null;
            $schedule->frequency = request()->frequency;
            $schedule->param = request()->param;
        }

        return $schedule;
    }
}
