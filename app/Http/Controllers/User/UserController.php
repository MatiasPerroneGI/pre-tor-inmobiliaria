<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Auth;

class UserController extends AppController
{
    protected $class = User::class;
    protected $viewFolder = 'backoffice.users';
    protected $mustValidate = true;

    public function json()
    {
        $agency = Auth::guard('user')->user()->hasAgency();
        abort_unless($agency ? true : false, 403);
        $data = $this->jsonQuery($this->class::tabulated())->where('agency_id', '=', $agency->id)->get();
        $recordsTotal = $this->jsonQuery($this->class::query())->count();
        $recordsFiltered = $this->jsonQuery($this->class::filtered())->count();
        return compact('data', 'recordsTotal', 'recordsFiltered');
    }

    public function show(User $user)
    {
        $this->_verifySameIsAgency($user);
        return view($this->viewFolder . '.show', compact($user));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $this->_verifySameIsAgency($user);
        return parent::edit($id);
    }

    public function update($id)
    {
        $user = User::find($id);
        $this->_verifySameIsAgency($user);
        return parent::edit($id);
    }

    protected function beforeCreate($model)
    {
        $user = Auth::guard('user')->user();
        if ($user->hasAgency()) {
            $model->agency_id = $user->hasAgency()->id;
        }
        return $model;
    }

    protected function beforeUpdate($model)
    {
        $user = Auth::guard('user')->user();
        if ($model->id != $user->id) {
            abort_unless(boolval(Auth::guard('user')->user()->hasAgency()), 401);
        }
        if ($user->hasAgency()) {
            $model->agency_id = $user->hasAgency()->id;
        }
        return $model;
    }

    protected function afterCreate($model)
    {
        $password = Str::random(6);
        $model->password = Hash::make($password);
        $model->agency_id = \Auth()->user()->agency_id;

        $model->save();

        return $model;
    }

    protected function afterSave($model)
    {
        if (request()->permissions) {
            $model->syncPermissions(config('permission.models.permission')::whereIn("id", request()->permissions)->get());
        }

        return $model;
    }

    public function findByEmail()
    {
        $agency = Auth::guard('user')->user()->hasAgency();
        abort_unless($agency ? true : false, 403);
        return ['user' => User::where("email", request()->email)->where('agency_id', '=', $agency->id)->first()];
    }


    protected function _verifySameIsAgency(User $model)
    {
        $user = Auth::guard('user')->user();
        if ($model->id != $user->id) {
            $agency = $user->hasAgency();
            $esMismaAgencia = boolval($model->hasAgency() and $model->hasAgency()->id == $agency->id);
            abort_unless($agency ? true : false, 403);
            abort_unless($esMismaAgencia, 403);
        }
    }
}