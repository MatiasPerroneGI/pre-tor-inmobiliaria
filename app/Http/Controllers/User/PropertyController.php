<?php

namespace App\Http\Controllers\User;

use App\Property;
use App\User;
use App\Http\Controllers\AppController;

class PropertyController extends AppController
{
    protected $class = Property::class;
    protected $viewFolder = 'backoffice.properties';

    public function jsonQuery($query)
    {
        $agency = \Auth('user')->user()->agency_id;
        // Inmuebles de clientes
        if (request()->input('user_id') && $agency)
            return $query->own(request()->input('user_id'));

        return $agency ?
            $query->where('agency_id', $agency) : //De la agencia
            $query->own();                       //De un usuario cliente
    }

    public function beforeSave($model)
    {
        $model->agency_id = \Auth('user')->user()->agency_id;
        return $model;
    }

    public function afterSave($model)
    {
        if (request()->owners) {
            $agency = \Auth('user')->user()->agency_id;
            $ids  = User::whereIn('id', request()->owners)->pluck('id')->all();
            $model->owners()->sync(array_fill_keys($ids, ['agencia' => $agency]));
        }

        return $model;
    }
}