<?php

namespace App\Http\Controllers\User;

use App\Contract;
use App\Events\ContractConfirmed;
use App\Events\ContractCreated;
use App\Events\OwnerCreated;
use App\Events\TenantCreated;
use App\Guarantor;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContractCreationRequest;
use App\Jobs\ContractToPdf;
use App\Mail\VerifyEmail;
use App\Property;
use App\Service;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

//@TODO: ¿Qué pasa con el modelo del contrato?

/**
 * Notificaciones:
 * Cuando se crean nuevos tenants -> envío correo: te agregaron a pretor
 * Al crear contrato -> envío correo al tenant: tenés un nuevo contrato (sólo si la inmobiliaria lo indica)
 *     -> Creo un evento para el owner: se creó un nuevo contrato (sólo si la inmobiliaria lo indica)
 *     -> Creo un evento para la inmobiliaria: se creó un nuevo contrato
 */

class ContractController extends Controller
{
    public function json()
    {
        $data = Contract::own()->tabulated()->get();
        $recordsTotal = Contract::own()->count();
        $recordsFiltered = Contract::own()->filtered()->count();
        return compact('data', 'recordsTotal', 'recordsFiltered');
    }

    public function index()
    {
        return view('backoffice.contracts.index');
    }

    public function create()
    {
        $model = new Contract; //Modelo de contrato?
        $services = Service::where('id', '!=', 1)->get(); //service 1 = cannon mensual

        $properties = [];
        $owners = [new User];
        $tenants = [new User];
        $guarantors = [];

        // Con propiedad
        if (request()->has("property_id")) {
            $prop = Property::find(request()->property_id);
            $owners = $prop->owners;
            $properties = collect([$prop]);
        }

        // Con cliente
        if (request()->has("client_id")) {
            $owners = [User::find(request()->client_id)];
            $properties = Property::own(request()->client_id)->get();
        }

        return view('backoffice.contracts.kame-form', compact('model', 'services', 'properties', 'owners', 'tenants', 'guarantors'));
    }

    public function store(ContractCreationRequest $request)
    {
        try {

            DB::transaction(function () {

                $contract = Contract::create(request()->input('contract'));

                $owners = $this->createUsers(User::OWNER, request()->input('owners'));
                $owners->each(function ($owner, $i) use ($contract) {
                    $contract->owners()->attach($owner->id, ['legal_address' => request()->input("legal_address.$i")]);
                });

                $contract->services()->sync(request()->input('services'));

                //property
                $property_id = intval(request()->input('property_id'));
                if ($property_id) {
                    $property = Property::find($property_id);
                }

                if (!$property_id) {
                    $property = Property::create(request()->input('property') + ['agency_id' => \Auth('user')->user()->agency_id]);
                }

                $property->owners()->syncWithoutDetaching($owners->pluck('id'));
                $contract->property_id = $property->id;

                //tenants
                $tenants = $this->createUsers(User::TENANT, request()->input('tenants'));
                $tenants->each(function ($tenant) use ($contract) {
                    $contract->tenants()->attach($tenant->id, ['legal_address' => $contract->property->address]);
                });

                $contract->save();

                //guarantors
                foreach (request()->input('guarantors') as $guarantor) {
                    $guarantor['contract_id'] = $contract->id;
                    Guarantor::create($guarantor);
                }

                //periods
                $periods = $this->calculatePeriod();
                $contract->periods()->createMany($periods);

                //insurance
                if (request()->input('has_fire_insurance')) {
                    $contract->insurance()->create(request()->input('fire_insurance'));
                }

                event(new ContractCreated($contract)); // [NotifyContractCreation, ContractToPdf, ContractToVersion, AddClausesToVersion]

            });
            return redirect('backoffice/contracts#new');
        } catch (\Exception $e) {
        }
    }

    public function confirm(Contract $contract)
    {
        $contract->versions()->latest()->limit(1)->update(['confirmed' => true]);
        $contract->confirmed = true;
        $contract->save();

        event(new ContractConfirmed($contract)); // [NotifyContractConfirmation, AddPaymentsToContract]

        return redirect('backoffice/contracts#confirmed');
    }

    private function createUsers($type, array $usersData)
    {
        $users = collect();

        foreach ($usersData as $data) {
            if (!$user = User::where('email', $data['email'])->first()) {
                $user = User::create($data);
                $this->notifyUserCreation($type, $user);
                // $user->sendEmailVerificationNotification(); // Debería enviarlo el listener
            }
            $users->push($user);
        }

        return $users;
    }

    /**
     * @param string $type
     * @param App\User $user
     * @return void
     * @todo TODO Falta esta lógica:
     *   Si el contrato lo está creando un inmobiliaria tiene que poder marcar si quiere o no notificar a los dueños
     *   Si el contrato lo está creando un particular notifica o no al resto de los owners?
     *   Si bien esto es redundante está así para el momento en que agregué la lógica que falta lo que va a hacer que crear owner y crear tenant tengan efectos diferentes
     */
    private function notifyUserCreation(string $type, User $user)
    {
        if ($type == User::OWNER) {
            event(new OwnerCreated($user));
        } elseif ($type == User::TENANT) {
            event(new TenantCreated($user));
        }
    }

    private function calculatePeriod()
    {
        $duration = request()->input('contract.duration');
        $times = request()->input('contract.time') * 1;
        $timesQty = ($duration / $times) - 1;
        $price = request()->input('contract.price');
        $increase = (request()->input('contract.increase') / 100) + 1;
        $start = Carbon::parse(request()->input('contract.start'));

        $periods[] = [
            'step' => 1,
            'start' => $start,
            'end' => $start->copy()->addMonth($times),
            'price' => $price,
        ];

        while ($timesQty > 0) {
            $lastPeriod = $periods[count($periods) - 1];

            $periods[] = [
                'step' => ++$lastPeriod['step'],
                'start' => $lastPeriod['end'],
                'end' => $lastPeriod['end']->copy()->addMonth($times),
                'price' => $lastPeriod['price'] * $increase,
            ];

            $timesQty--;
        }

        return $periods;
    }
}