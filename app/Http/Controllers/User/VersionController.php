<?php

namespace App\Http\Controllers\User;

use App\Classes\Version as VersionTrait;
use App\Http\Controllers\AppController;
use App\Version;

class VersionController extends AppController
{
    use VersionTrait;

    protected $class = Version::class;
    protected $viewFolder = 'backoffice.versions';
    protected $mustValidate = false;

    public function jsonQuery($query)
    {
        return $query->where('contract_id', request()->input('contract_id'));
    }

    public function show(Version $version)
    {
        return view('backoffice.versions.show', compact('version'));
    }

    public function update($id)
    {
        $version = $this->class::find($id);

        $this->createNewVersionOfContract($version);

    	return redirect($this->getPath() . '?contract_id=' . $version->contract->id);
    }
}
