<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\AppController;

class ClientController extends AppController
{
    protected $class = User::class;
    protected $viewFolder = 'backoffice.clients';
    protected $mustValidate = true;

    public function json()
    {
        $data = $this->jsonQuery(\Auth()->user()->agency->clients()->tabulated())->get();
        $recordsTotal = $data->count();
        $recordsFiltered = $this->jsonQuery(\Auth()->user()->agency->clients()->filtered())->count();
        return compact('data', 'recordsTotal', 'recordsFiltered');
    }

    public function store()
    {
        if (request()->client_id) {
            $this->afterCreate(User::find(request()->client_id));
            return redirect($this->getPath());
        }

        return parent::store();
    }

    protected function afterCreate($model)
    {
    	\Auth()->user()->agency->clients()->syncWithoutDetaching($model);

        if (!$model->password) {
            $password = Str::random(6);
            $model->password = Hash::make($password);
            $model->save();
        }
    	
    	if (request()->sendMail && $model->email) {
    		$email = new \App\Mail\NewUserMail($model,$password);
        	\Mail::send($email);
    	}

		return $model;
    }
}
