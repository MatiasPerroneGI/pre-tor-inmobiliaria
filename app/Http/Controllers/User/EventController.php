<?php

namespace App\Http\Controllers\User;

use App\Event;
use App\Http\Controllers\AppController;

class EventController extends AppController
{
    protected $class = Event::class;
    protected $viewFolder = 'backoffice.events';
    protected $mustValidate = true;

    public function index()
    {
        $events = \Auth()->user()->events()->paginate(10);

        if (request()->ajax()) {
            $response = [
                'info'=>$events,
                'html'=> view('admin.components.iterator', ['view' => request()->input('view'), 'models' => $events, 'model' => 'event'])->render(),
            ];
        } else {
            $response = view("backoffice.chat.index", compact("events"));
        }

        return $response;
    }
}
