<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Property;
use App\Contract;
use App\Http\Controllers\Controller;

class AutocompleteController extends Controller
{
    public function clients()
    {
        return User::selectRaw('id as value, dni as label, name, lastname, email')
            ->where('dni', 'like', request()->input('term') . '%')
            ->get()
            ->toArray()
        ;
    }

    public function clientes()
    {
        if(\Auth()->user()->agency_id){
            return \Auth()->user()->agency->clients()
                ->where('name', 'like', request()->input('term') . '%')
                ->orWhere('lastname', 'like', request()->input('term') . '%')
                ->get()
                ->toArray()
            ;
        }
        return $this->clients();
    }

    public function properties()
    {
        $query = Property::selectRaw('*, id as value, address as label')
            ->where('address', 'like', request()->input('term') . '%')
        ;

        if (\Auth::check()) {
            $query->where('client_id', \Auth::id());
        }

        return $query->get()->toArray();
    }

    public function contracts()
    {
        $query = Contract::selectRaw('*, id as value, name as label')
            ->active()
            ->own()
            ->where('name', 'like', request()->input('term') . '%')
            ->with('owners','guarantors','tenants')
        ;

        return $query->get()->toArray();
    }
}
