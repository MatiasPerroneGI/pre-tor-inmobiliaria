<?php

namespace App\Http\Controllers\User;

use App\Payment;
use App\User;
use App\Http\Controllers\AppController;

class PaymentController extends AppController
{
    protected $class = Payment::class;
    protected $viewFolder = 'backoffice.payments';
    protected $mustValidate = false;

    public function jsonQuery($query)
    {
    	return $query->whereJsonContains('tenants_id', \Auth::id());
    }

    public function update($id)
    {
        $model = $this->class::find($id);

        if (request()->hasFile('payment_proof')) {
            $this->uploadProof($model);
        } else {
            $this->prismaPayment($model);
        }

        return request()->ajax() ? $model->toArray() : redirect($this->getPath());
    }

    public function uploadProof($model)
    {
        $file = request()->file('payment_proof');
        $name = \Str::random(10) . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public/tickets/', $name);

        $model->update([
            'payment_proof' => $name,
            'payment_date' => date('Y-m-d')
        ]);
    }

    public function prismaPayment($model)
    {
        //TODO: lógica de prisma
        $model->update(['payment_date' => date('Y-m-d')]);
    }

    /*

    public function edit($id)
    {
        $model = $this->class::find($id);
        $view = $this->paymentView($model);
        return view ($this->viewFolder.".kame-form-$view", compact('model'));
    }

    private function paymentView($model)
    {
        $view = 'prisma';

        if ($model->service->id == 1) {
            $views = ['', 'efectivo', 'mp', 'transferencia'];
            $view = $views[$model->service->id];
        }

        return $view;
    }
    */
}
