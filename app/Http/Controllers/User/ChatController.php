<?php

namespace App\Http\Controllers\User;

use App\Conversation;
use App\Participant;
use App\User;
use App\Message;
use App\File;
use App\Http\Requests\MessageRequest;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Response;

class ChatController extends AppController
{
    protected $class = Conversation::class;
    protected $viewFolder = 'backoffice.chat';
    protected $mustValidate = true;

    public function index()
    {
        $conversations = $this->getConversations()->paginate(10);

        if (request()->ajax()) {
            $view = request()->input('view', 'backoffice.chat.partials.conversation-single');
            $response = [
                'info'=>$conversations,
                'html'=> view('admin.components.iterator', ['view' => $view, 'models' => $conversations, 'model' => 'conversation'])->render(),
            ];
        } else {
            $response = view("backoffice.chat.index", compact("conversations"));
        }

        return $response;
    }

    public function show($id)
    {
    	$conversation = Conversation::where('id', $id)
            ->with(['messages' => function($query) {
                $query->with('user', 'files')->oldest();
            }])
            ->first()
        ;
        
        // Actualizo mensajes no vistos por el usuario
        $conversation->messages()
            ->where("read",0)
            ->where("user_id",'!=', Auth()->id())
            ->update([
                'read'=>1
            ]);

    	return view($this->viewFolder.'.show',compact('conversation'));
    }

    public function json()
    {
        return $this->getConversations()->limit(5);
    }

    public function answer(Message $message)
    {
        $recipients = $message->conversation->integrantes;
        $conversation = $message->conversation;
        return view($this->viewFolder.'.kame-form',compact('recipients','conversation'));
    }

    public function store()
    {
        $validator = new MessageRequest;
        request()->validate($validator->rules(), $validator->messages());

        $message = new Message(request()->all());
        $message->user_id = \Auth::id();
        $message->conversation_id = $this->getConversationId();
        $message->save();

        $this->saveFiles($message);

        // Queue new message mail
        
        \Mail::queue(new \App\Mail\NewMessageMail($message));

        return request()->ajax() ?
            Response::json([
                    'model'=> $message,
                    'listMessage' => view('backoffice.chat.partials.conversation-single', array('conversation' => $message->conversation))->render(),
            ])
            :
            redirect($this->getPath());
    }

    private function getConversationId()
    {
        $id = request()->conversation_id;
        if (!$id) {
            // nueva conversacion
            $users = User::whereIn('id',[request()->recipient,\Auth()->id()])->get();
            $c = Conversation::create(request()->only("subject"));
            $c->users()->attach($users);
            $id = $c->id;
        }
        return $id;
    }

    private function saveFiles($message)
    {
        if (request()->has("files_ids")) {
            $query = File::whereIn('id',request()->files_ids);
            $message->files()->saveMany($query->get());
            $query->update(['pending'=>0]);
        }
    }

    private function getConversations()
    {
        return \Auth()->user()->conversations()
            ->with(['lastMessage' => function ($query) {
                $query->withCount('files');
            }])
            ->with(['participants' => function ($query) {
                $query->where('user_id','!=',\Auth()->id())->with('user');
            }])
            ->latest()
        ;
    }

    public function searchContacts()
    {
        $term = request()->q;
        $contacts = User::where('id', '!=', \Auth()->id())
            ->where(function ($query) use ($term) {
                $query->where('name', 'like', "%$term%")
                    ->orWhere('email', 'like', "%$term%");
            })
            ->limit(10)
            ->orderBy('name')
            ->get()
        ;

        return ['items' => $contacts];
    }

    public function searchConversations()
    {
        $term = request()->term;
        $conversations = \Auth()->user()->conversations()
            ->where("subject",'like','%'.$term.'%')
            ->limit(5)
            ->get()
        ;
        
        if (request()->ajax()) {
            return Response::json(
                    [
                        'cant'=>$conversations->count(),
                        'html'=> view('backoffice.chat.partials.list-conversation', compact("conversations"))->render(),
                    ]);
        }
        return $conversations;
    }
}
