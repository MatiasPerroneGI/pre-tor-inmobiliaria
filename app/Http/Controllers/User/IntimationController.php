<?php

namespace App\Http\Controllers\User;

use App\Intimation;
use App\Contract;
use App\Conversation;
use App\File;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\AppController;

class IntimationController extends AppController
{
    protected $class = Intimation::class;
    protected $viewFolder = 'backoffice.intimations';
    protected $mustValidate = true;

    public function index(){
        if (request()->ajax()) {
            $intimations = Intimation::orderBy("created_at",'desc')->limit(10)->get();

            $view = 'backoffice.intimations.single';
            return [
                'info'=>$intimations,
                'html'=> view('admin.components.iterator', ['view' => $view, 'models' => $intimations, 'model' => 'intimation'])->render(),
            ];
        } 

        return Parent::index();
    }

    public function show(Intimation $intimation)
    {
        return view($this->viewFolder.'.show',compact("intimation"));
    }

    public function jsonQuery($query)
    {
        // TODO: Intimaciones de los contratos que incluya al usuario. Corroborar que este ok
        return $query->whereHas("contract",function($q){
            $q->own();
        });
    }

    protected function afterSave($intimation)
    {
    	$intimation->update(['user_id'=>\Auth::id()]);
    	$this->saveFiles($intimation);
    	$this->setConversation($intimation);
    	return $intimation;
    }

    private function saveFiles($intimation)
    {
    	if (request()->has("files_ids")) {
            $query = File::whereIn('id',request()->files_ids);
            $intimation->files()->saveMany($query->get());
            $query->update(['pending'=>0]);
        }
    }

    public function getIntimatedUsers(Contract $contract)
    {
        return $contract->getIntimatedUsers();
    }

    /*
    Siempre a los tenants. 
    SI soy dueño -> +owners (Si hay mas de uno incluirlo)
    SI soy tenant
        SI la prop del contrato tiene agencia -> +agencia
        SI la prop del contrato No tiene agencia -> +owners
    */
    private function setConversation($intimation)
    {
        $currentUser = \Auth()->user();

        $contract = Contract::find($intimation->contract_id);
        $property = $contract->property;
    	$conversation = Conversation::create([
    		'subject'=>$intimation->title
    	]);

        $participants = $contract->tenants
            ->merge([$currentUser])
            ->merge($contract->getIntimatedUsers())
        ;

        if ( $contract->userIsOwner() ){
            $participants = $participants->merge($contract->owners);
        } 
        // elseif ( $contract->userIsTenant() ){
        //     if ( $contract->hasAgency() ){
        //         // Por ahora incluimos a todos los usuarios de la agencia
        //         $participants = $participants->merge(\App\User::where("agency_id",$property->agency_id)->get());
        //     }else {
        //         $participants = $participants->merge($contract->owners);
        //     }
        // }

        // Si es inmo y pone en copia a propietarios
        if ($currentUser->hasAgency() && request()->include_owners) {
            $participants = $participants->merge($contract->owners);
        }

    	$conversation->users()->attach($participants->pluck("id")->toArray());

        // Set first message
        $message = Message::create([
            'message'=>request()->description,
            'conversation_id'=>$conversation->id,
            'user_id'=>\Auth::id()
        ]);
        // Set intimations files for message
		$filesMessage = $intimation->files->map(function($f){
			return $f->replicate();
		});
		$message->files()->saveMany($filesMessage);

        $intimation->update(['conversation_id' => $conversation->id]);
    }

    public function resolve(Intimation $intimation)
    {
    	$intimation->update(['is_resolved'=> 1]);
        return redirect($this->getPath());
    }
}
