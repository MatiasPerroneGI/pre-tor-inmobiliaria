<?php

namespace App\Http\Controllers\Test;

use App\User;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Auth;

class UserController extends AppController
{
    protected $class = User::class;
    protected $viewFolder = 'test.users';

    public function loginAs(User $user)
    {
        Auth::login($user);

        return redirect('user.home');
    }
}