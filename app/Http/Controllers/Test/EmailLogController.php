<?php

namespace App\Http\Controllers\Test;

use App\EmailLog;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class EmailLogController extends AppController
{
    protected $class = EmailLog::class;
    protected $viewFolder = 'test.emails-log';

    public function show(EmailLog $emails_log)
    {
    	return $emails_log->body;
    }
}
