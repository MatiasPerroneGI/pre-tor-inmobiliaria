<?php

namespace App\Http\Controllers\Test;

use App\User;
use App\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function subscribe(User $user, Plan $plan)
    {
    	var_dump($user->activeSubscriptions()->count() ? 'Has subscriptions' : 'Doesnt have subscriptions');

    	if (!$user->activeSubscriptions()->count()) {
    		var_dump("Create subscription");
    		$user->newSubscription('main', $plan);
    	}else{
    		var_dump("User already subscribed");
    	}
    	dd($user->activeSubscriptions());
    }

    public function change(User $user, Plan $plan){
    	if ($user->subscribedTo($plan->id)) {
    		return "User already subscribed to plan";
    	}
    	$user->activeSubscriptions()->first()->changePlan($plan);
    	dd($user->activeSubscriptions());
    }

    public function cancel(User $user){
    	if (!$user->activeSubscriptions()->count()) {
    		return "User doesnt have subscription";
    	}
    	// Si se cancela y tiene días de prueba pendientes sigue activo hasta terminar los días.
    	// Si se quiere cancelar inmediatamente la suscripción se envia el atributo TRUE. Si no,
    	// 	queda activa la suscripción hasta el próximo fin de período
    	$user->activeSubscriptions()->first()->cancel(true);
    	dd($user->activeSubscriptions());
    }
}
