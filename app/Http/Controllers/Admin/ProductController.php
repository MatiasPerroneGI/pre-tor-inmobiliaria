<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Controllers\AppController;

class ProductController extends AppController
{
    protected $class = Product::class;
    protected $viewFolder = 'admin.products';

    public function destroy($id)
    {
    	throw new \Exception;
        $model = $this->class::find($id);
        //$model->delete();

        return ['success'=> false, 'error' => 'No se puede borrar un producto que tenga estado visible'];

        return ['success' => true];
    }
}
