<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppController;

class UserController extends AppController{
    protected $class = User::class;
    protected $viewFolder = 'admin.users';
}
