<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AppController;
use \App\Mail\PlanChangedMail;

class PlanController extends AppController{
    protected $class = \App\Plan::class;
    protected $viewFolder = 'admin.plans';
    protected $mustValidate = false;

    protected function afterUpdate($model,$changed){
        if (isset($changed['price'])) {
            $users = \App\User::whereHas("subscriptions",function($query) use ($model){
                return $query->where("plan_id",$model->id);
            })->pluck('email')->toArray();
            
            \Mail::queue(new PlanChangedMail($users,$model,$changed));
        }

    	return $model;
    }
}
