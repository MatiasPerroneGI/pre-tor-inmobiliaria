<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
 //    private $allowedExtensions = [
	// 	'doc',
	// 	'docx',
	// 	'xls',
	// 	'xlsx',
	// 	'jpg',
	// 	'jpeg',
	// 	'png',
	// 	'pdf',
	// 	'ppt',
	// ];

	public function upload(){
        $this->middleWare('progress');

        // TODO: No me valida el tamaño máximo de los archivos, ni con max ni con size
    	request()->validate([
                'files.*'=>'file|mimes:doc,docx,xls,xlsx,jpg,jpeg,png,pdf,ppt|max:3000',
            ],
            [
                'files.*.max'=>'Los archivos no pueden pesar mas de 3 MB',
                'files.*.mimes'=>'El formato de algunos de los archivos es inválido',
            ]
        );
        
        $response = ['success' => true, 'files' => []];
        foreach (request()->file('files') as $file) {
            $file = $this->save($file);
            $response['files'][] = ['src' => $file->src, 'id' => $file->id, 'name'=> $file->original_name];
        }
        return response()->json($response);
    }

    private function save($file){
        $resource = strtolower(request()->input('resource'));
        $name = File::nextId(). '.' . request()->input('resource'). '.' . $file->getClientOriginalExtension();
        $file->storeAs(
            'public/files/'.strtolower(request()->input('resource')), $name
        );
        return File::create(['src' => 'files/'.$resource.'/'.$name, 'original_name' => $file->getClientOriginalName()]);
    }

    public function destroy($file){
        File::find($file)->delete();
        return ['success' => true];
    }

    public function download (File $file){
        return Storage::disk('public')->download($file->src,$file->original_name);
    }

    public function downloadAll(File $file){
        $files = File::where("fileable_type",$file->fileable_type)
            ->where("fileable_id",$file->fileable_id)
            ->get()
        ;
    
        if (count($files) == 1) 
            return $this->download($files->first());

        $zipName = 'files-' . $files->pluck('id')->implode('-') . '.zip';
 
        if (!Storage::disk('public')->exists("files/zips/$zipName")) {
            $zipper = new \Chumper\Zipper\Zipper;
            $zipper->make(base_path('storage/app/public/files/zips/'.$zipName));

            $files->each(function ($file) use ($zipper) {
                $zipper->add('storage/'.$file->src,$file->original_name);
            });
            
            $zipper->close();
        }

        return Storage::disk('public')->download('files/zips/'.$zipName,config('app.name').' Descarga.zip');
    }
}
