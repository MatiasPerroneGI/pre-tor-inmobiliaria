<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Product;
use App\Validators\AddToCartValidator;
use App\Validators\CouponApplyValidator;

class CartController extends Controller
{
    public function show()
    {
    	$cartItems = \KameCart::content();
        
        return view('front.cart.show', compact('cartItems'));
    }

    public function add(Product $product)
    {
    	AddToCartValidator::validate($coupon);

    	$options = [
    		'path' => $product->path,
    		'description' => \Str::limit($product->description, 80),
    		'thumb' => $product->thumb
    	];

    	$cartItem = \KameCart::add($product, request()->input('qty'), $options);

        return \KameCart::toCollection()->put(compact('cartItem'));
    }

    public function update()
    {
        \KameCart::update(request()->input('rowId'), request()->input('qty'));
        
        return \KameCart::toCollection();
    }

    public function remove()
    {
        \KameCart::remove(request()->input('rowId'));

        return \KameCart::toCollection();
    }

    public function clear()
    {
        \KameCart::destroy();

        return redirect('/carrito');
    }

    public function applyCoupon()
    {
        $coupon = Coupon::where('code', request()->input('code'))->first();

        CouponApplyValidator::validate($coupon);

        \KameCart::applyCoupon($coupon);

        return \KameCart::toCollection();
    }
}
