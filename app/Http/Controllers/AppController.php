<?php

namespace App\Http\Controllers;

class AppController extends Controller
{
    protected $class;
    protected $viewFolder;
    protected $path;
    protected $mustValidate = true;

    public function json()
    {
        $data = $this->jsonQuery($this->class::tabulated())->get();
        $recordsTotal = $this->jsonQuery($this->class::query())->count();
        $recordsFiltered = $this->jsonQuery($this->class::filtered())->count();
        return compact('data', 'recordsTotal', 'recordsFiltered');
    }

    public function index()
    {
        return view ($this->viewFolder.".index");
    }

    public function create()
    {
        $model = new $this->class;
        return view ($this->viewFolder.".kame-form", compact('model'));
    }

    public function store()
    {
        $model = new $this->class(request()->all());

        $model = $this->beforeCreate($model);
        $model = $this->beforeSave($model);
        $this->uploadPreviews($model);

        //validación
        $this->validation();

        $model->save();

        $model = $this->afterCreate($model);
        $model = $this->afterSave($model);

        $this->addImages($model);
        $response = request()->ajax() ? $model->toArray() : redirect($this->getPath());
        return $response;
    }

    public function edit($id)
    {
        $model = $this->class::find($id);
        return view ($this->viewFolder.".kame-form", compact('model'));
    }

    public function update($id)
    {
        $model = $this->class::find($id);
        $model->fill(request()->all());

        $model = $this->beforeUpdate($model);
        $model = $this->beforeSave($model);
        $changed = $model->getDirty();

        $this->uploadPreviews($model);

        request()->id = $id;
        //validación
        $this->validation();

        $model->save();

        $model = $this->afterUpdate($model,$changed);
        $model = $this->afterSave($model);

        $this->addImages($model);
        $response = request()->ajax() ? $model->toArray() : redirect($this->getPath());
        return $response;
    }

    public function destroy($id)
    {
        $model = $this->class::find($id);
        $model->delete();
        return ['success' => true];
    }

    protected function getPath()
    {
        if (!$path = $this->path) {
            $path = str_replace('.', '/', $this->viewFolder);
        }
        return $path;
    }

    protected function validation()
    {
        if ($this->mustValidate) {
            $model = str_replace('App\\', '', $this->class);
            $validatorPath = 'App\Http\Requests\\' . $model . 'Request';
            $validator = new $validatorPath;

            request()->validate($validator->rules(), $validator->messages());
        }
    }

    protected function addImages($model)
    {
        //slider
        if (request()->input('imageId')) {
            $images = \App\Image::find(request()->input('imageId'));
            $model->images()->delete();
            $model->images()->save($images);
            $images->update(['pending'=>0]);
        }

        if (request()->has('imagesIds')) {
            $images = \App\Image::whereIn('id', request('imagesIds'));
            $model->images()->saveMany($images->get());
            $images->update(['pending'=>0]);
        }

        $model->save();
    }

    protected function uploadPreviews($model)
    {
        //dd(request()->hasFile('uploadPreviews.files.series.portada:es'));
        if (request()->has('uploadPreviews')) {
            $uploadPreviews = request()->input('uploadPreviews');
            foreach ($uploadPreviews as $uploadPreview) { //itero por cada uploader que puede haber en la vista
                foreach ($uploadPreview as $resouce => $props) {
                    foreach ($props as $prop) {
                        if (request()->hasFile("uploadPreviews.files.$resouce.$prop")) {
                            $options = config("image.$resouce");
                            $file = request()->file("uploadPreviews.files.$resouce.$prop");
                            $imageName = $resouce . '.'. uniqid(). '.' .$file->extension();
                            foreach ($options as $size => $option) {

                                $image = \Image::make($file);
                                foreach ($option as $method => $args) {
                                    if (!is_array($args)) $args = [$args];
                                    call_user_func_array([$image, $method], $args);
                                }
                                $image->save(public_path( "/content/$resouce/$size/$imageName"));
                            }
                            $model->$prop = $imageName;
                        }
                    }
                }
            }
        }

        return $model;
    }

    protected function beforeCreate($model)
    {
        return $model;
    }

    protected function afterCreate($model)
    {
        return $model;
    }

    protected function beforeUpdate($model)
    {
        return $model;
    }

    protected function afterUpdate($model,$changed)
    {
        return $model;
    }

    protected function beforeSave($model)
    {
        return $model;
    }

    protected function afterSave($model)
    {
        return $model;
    }

    protected function jsonQuery($query)
    {
        return $query;
    }
}
