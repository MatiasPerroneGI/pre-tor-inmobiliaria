<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        $guard = $guard ?? 'admin';

        if (Auth::guard($guard)->check()) {
            $prefix = config("auth.guards.$guard.prefix");
            return redirect(route("$prefix.home"));
        }

        return $next($request);
    }
}
