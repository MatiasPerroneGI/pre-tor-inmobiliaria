<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanRequest extends FormRequest
{
    public function authorize()
    {
        return \Auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'trial_period' => 'int',
            'grace_period' => 'int'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'price' => 'Precio',
            'description' => 'Descripción',
            'trial_period' => 'Período de prueba',
            'grace_period' => 'Período de gracia'
        ];
    }
}
