<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    
    public function authorize(){
        return \Auth::check();
    }

    public function rules(){
        // $id = request()->id ?? null;
        return [
            // 'email'=>'required|email|unique:users,email,id,'.$id,
            'email'=>'email',
            'name'=>'required',
            'lastname'=>'required',
            'dni'=>'required',
        ];
    }

    public function messages(){
        return [
            'name.required'=>'El nombre es obligatorio',
            'lastname.required'=>'El apellido es obligatorio',
            'dni.required'=>'El DNI es obligatorio',
            'email.required'=>'El email es obligatorio',
            'email.email'=>'El email ingresado no es válido',
            'email.unique'=>'El email ya se encuentra registrado',
        ];
    }
}
