<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject'=>'required',
            'message'=>'required',
        ];
    }

    public function messages(){
        return [
            'subject.required'=>'Debe ingresar un asunto',
            'message.required'=>'Debe ingresar un mensaje',
        ];
    }
}
