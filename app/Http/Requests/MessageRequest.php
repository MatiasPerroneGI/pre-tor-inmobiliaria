<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient'=>'required',
            'subject'=>'required_without:conversation_id',
            'message'=>'required',
        ];
    }

    public function messages(){
        return [
            'subject.required_without'=>'El asunto es obligatorio',
            'message.required'=>'El mensaje es obligatorio',
            'recipient.required' => 'Debe seleccionar un destinatario',
        ];
    }
}
