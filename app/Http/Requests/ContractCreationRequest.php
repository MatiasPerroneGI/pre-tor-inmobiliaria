<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property.*' => 'required',
            'property.id' => [
                'nullable',
                'numeric',
                // new PropertyRule
            ],
            'contract.commerce' => 'nullable|boolean',
            'contract.deposit_time' => 'numeric',
            'contract.duration' => 'integer',
            'contract.price' => 'numeric',
            'contract.start' => 'date_format:Y-m-d',
            'contract.end' => 'date_format:Y-m-d',
            'contract.dollar_deposit' => 'boolean',
            'contract.deposit_value' => 'numeric',
            'contract.increase' => 'numeric|min:0|max:200',
            'contract.time' => 'integer',
            'contract.total' => 'numeric',
            'contract.anticipated_payment' => 'boolean',
            'contract.penalty' => 'integer',
            'tenant.*.*' => 'required',
            'owner.*.*' => 'required',
            'guarantors.*.*' => 'required',
            'fire_insurance.*' => 'required_if:has_fire_insurance,1',
        ];
    }
}