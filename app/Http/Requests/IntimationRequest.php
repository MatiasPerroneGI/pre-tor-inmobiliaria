<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IntimationRequest extends FormRequest
{
    public function authorize()
    {
         return \Auth::check();
    }

    public function rules()
    {
        return [
            'contract_id'=>'required',
            'title'=>'required',
            'description'=>'required',
        ];
    }

    public function attributes()
    {
        return [
            'contract_id'=>'contrato',
            'title'=>'título',
            'description'=>'descripción',
        ];    
    }
}
