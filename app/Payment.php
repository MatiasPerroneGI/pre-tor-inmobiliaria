<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use Tabulate;

    protected $fillable = [
        'service_id',
        'contract_id',
        'tenants_id',
        'value',
        'expiration',
        'payment_date', //date
        'payment_proof',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function scopeTabulateQuery($query)
    {
        return $query->with('service', 'contract');
    }

    public function getOrderBy()
    {
        return ['expiration', 'asc'];
    }
}
