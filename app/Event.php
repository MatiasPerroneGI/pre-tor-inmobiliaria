<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'subject',
        'message',
        'date',
        'is_read',
        'user_id',
        'schedule_id',
    ];

    protected $dates = ['date'];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function schedule()
    {
        return $this->belongsTo(\App\Schedule::class);
    }
}
