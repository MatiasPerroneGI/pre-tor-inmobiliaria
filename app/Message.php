<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
     protected $fillable = [
        'read',
        'message',
        'user_id',        
        'conversation_id'   
    ];
    

    // Relations
    public function conversation()
    {
        return $this->belongsTo(\App\Conversation::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function files()
    {
        return $this->morphMany(\App\File::class, 'fileable');
    }
    // -----------------------

    // functions
    public function isFromUser()
    {
        return $this->user_id == \Auth()->id();
    }
    
    public function hasFilesAttached()
    {
        return $this->files()->count();
    }

    public function getRecipientAttribute()
    {
        return $this->conversation->participants()->where("user_id",'!=',$this->user_id)->first()->user->name;
    }

    public function recipient()
    {
        return $this->conversation->participants()->where("user_id",'!=',$this->user_id)->first()->user;
    }
}
