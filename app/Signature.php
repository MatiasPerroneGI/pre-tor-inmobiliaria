<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{

    // type 0, garante por bienes propios
    // type 1, garante con propiedad, requiere address y plate
    // type 2, aseguradora, solo requiere nombre ¿cuil? y email

    protected $fillable = [
        'version_id',
        'author',
        'user_id',
        'type',
        'token',
        'img',
    ];

    public function version()
    {
        return $this->belongsTo(Version::class);
    }
}
