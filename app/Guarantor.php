<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{

    // type 0, garante por bienes propios
    // type 1, garante con propiedad, requiere address y plate
    // type 2, aseguradora, solo requiere nombre ¿cuil? y email

    protected $fillable = [
        'name',
        'lastname',
        'address',
        'polize',
        'email',
        'dni',
        'type',
        'plate',
        'contract_id',
        'legal_address',
        'guaranty_address'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}
