<?php

namespace App\Listeners;

use App\Events\ContractCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddClausesToVersion implements ShouldQueue
{
    public function handle(ContractCreated $event)
    {
        $contract = $event->contract->load('versions');

        $clauses = \App\Clause::cloneFromTemplate();

        $contract->versions->last()->clauses()->createMany($clauses->toArray());
    }
}
