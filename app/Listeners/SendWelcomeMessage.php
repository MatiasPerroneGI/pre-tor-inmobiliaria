<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\WelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeMessage implements ShouldQueue
{
    public function __construct()
    {
    }

    public function handle(UserRegistered $event)
    {
        // $mail = (new WelcomeMail($event->user))->to($event->user);
        // Mail::send($mail);
    }
}