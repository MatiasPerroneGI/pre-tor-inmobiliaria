<?php

namespace App\Listeners;

use App\Events\ContractCreated;
use App\Jobs\ContractToVersion as ContractToVersionJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ContractToVersion implements ShouldQueue
{
    public function __construct()
    {
        
    }

    public function handle(ContractCreated $event)
    {
        ContractToVersionJob::dispatch($event->contract);
    }
}
