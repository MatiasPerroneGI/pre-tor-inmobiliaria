<?php

namespace App\Listeners;

use App\Events\ContractConfirmed;
use App\Jobs\AddPaymentsToContracts;
use App\Payment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
* AddPaymentsToContract agrega los payments a un contrato determinado.
* Los payments de un contrato contienen la relativa al pago (o falta de este ) de los services que tiene relacionado.
* Este listener hace uso del job AddPaymentsToContract que encapsula la lógica propia de agregar payments a los contratos.
* El job tiene un uso más genérico y se agrega payments a todos los contracts lo días primero de cada mes.
*/

class AddPaymentsToContract implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(ContractConfirmed $event)
    {
        AddPaymentsToContracts::dispatch($event->contract);
    }
}
