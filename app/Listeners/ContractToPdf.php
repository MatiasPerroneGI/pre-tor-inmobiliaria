<?php

namespace App\Listeners;

use App\Events\ContractCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ContractToPdf implements ShouldQueue
{
    public function __construct()
    {
        
    }

    public function handle(ContractCreated $event)
    {
        $pdf = \PDF::loadView('backoffice.contracts.pdf', ['version' => $event->contract->versions->last()]);
        $versionNumber = $event->contract->versions()->count();
        $pdf->save(public_path('content/contracts/contract.' . $event->contract->id . '.' . $versionNumber . '.pdf'));
    }
}
