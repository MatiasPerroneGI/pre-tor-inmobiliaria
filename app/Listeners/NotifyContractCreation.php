<?php

namespace App\Listeners;

use App\Event;
use App\Events\ContractCreated;
use App\Mail\ContractCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyContractCreation implements ShouldQueue
{
    public function __construct()
    {
    }

    public function handle(ContractCreated $event)
    {
        //Mail para el locatario
        $mail = new ContractCreatedMail($event->contract);
        \Mail::to($event->contract->tenants)->send($mail);

        //Evento para el locador
        $notification = new Event([
            'subject' => 'Contrato creado',
            'message' => 'El contrato "' . $event->contract->name . '" fue creado con éxito. En cuanto el locatario lo confirme te avisaremos.',
            'date' => \Carbon\Carbon::now()
        ]);

        $event->contract->owners->each(function ($owner) use ($notification) {
            $owner->events()->save($notification);
        });
    }
}