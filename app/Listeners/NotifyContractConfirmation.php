<?php

namespace App\Listeners;

use App\Event;
use App\Events\ContractConfirmed;
use App\Mail\ContractConfirmedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyContractConfirmation implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(ContractConfirmed $event)
    {
        $url = url('backoffice/contracts');

        $recipents = $event->contract->tenants->concat($event->contract->owners);

        $events = $recipents->map(function ($recipent) use ($event, $url) {
            return [
                'subject' => 'El contrato ' . $event->contract->name . ' fue confirmado.',
                'message' => "El contrato entrá en vigencia a partir del día ". $event->contract->start->format('d-m-Y') .". Podés acceder a él en la sección de contratos de tu perfil de admistración. <a href=\"$url\">Listado de contratos</a>",
                'date' => date('Y-m-d'),
                'user_id' => $recipent->id
            ];
        });

        Event::insert($events->toArray());

        $mail = new ContractConfirmedMail($event->contract, $recipents);

        \Mail::queue($mail);
    }
}
