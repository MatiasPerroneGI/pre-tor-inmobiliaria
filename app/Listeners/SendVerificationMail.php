<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\VerifyMail;
use App\Mail\WelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendVerificationMail implements ShouldQueue
{
    public function __construct()
    {
    }

    public function handle(UserRegistered $event)
    {
        $mail = new VerifyMail($event->user, $event->token);
        \Mail::send($mail);
    }
}