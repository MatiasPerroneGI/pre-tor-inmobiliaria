<?php

namespace App;

use App\Classes\Tabulate;
use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    use Tabulate;
    
    protected $fillable = [
        'user_id',
        'contract_id',
        'content',
        'pdf',
        'number',
        'confirmed',
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function clauses()
    {
        return $this->hasMany(Clause::class);
    }

    public function isCurrent()
    {
        return $this->contract->version()->count() > $this->number;
    }

    public function isLastOne()
    {
        $lastVersion = $this->contract->versions()->latest()->first('id');

        return $lastVersion->id == $this->id;
    }

    //Tabulate options
    public function getOrderBy()
    {
        return ['created_at', 'desc'];
    }
    //----------------
}
