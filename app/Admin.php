<?php

namespace App;

use App\Notifications\Admin\ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasPermissions;

class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes, HasPermissions;

    protected $guard_name = 'admin';

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    //getters y setters
    public function getThumbAttribute()
    {
        return $this->image ?? 'imagen-no-disponible.jpg';
    }
    //-----------------
}
