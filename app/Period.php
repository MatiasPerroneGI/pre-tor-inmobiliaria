<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = [
        'step',
        'start',
        'end',
        'price',
        'contract_id',
    ];

    protected $dates = ['start', 'end'];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    /*
    //un contrato tiene varios perídos y en cada período puede haber varios pagos.
    //ej.: período Abril-2019 - Agosto 2019 -> pago junio 2019
    public function payments()
    {
        return $this->morphMany(Payment::class, 'payable');
    }
    */
}
