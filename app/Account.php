<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'bank',
        'type',
        'number',
        'cbu',
        'owner',
        'alias',
        'client_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
