<?php

use App\User;
use App\Conversation;
use App\Message;
use Illuminate\Database\Seeder;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker\Factory::create();

    	if (!User::find(2)) {
    		User::create([
    			'name'=>'Corina',
    			'email'=>'corina@mail.com',
    			'password'=>bcrypt('123456'),
			]);

			User::create([
                'name'=>'Angra',
                'email'=>'angra@mail.com',
                'password'=>bcrypt('123456'),
            ]);

            User::create([
                'name'=>'Pipa',
                'email'=>'pipa@mail.com',
                'password'=>bcrypt('123456'),
            ]);
    	}

        factory(App\Conversation::class, 26)->create()->each(function ($conversation) {
            $totalMessages = rand(1,3);
            $recipient = rand(2,4);

            $messages = factory(App\Message::class, $totalMessages)->make()->each(function ($message) use ($recipient) {
                $message->user_id = rand(0,1) ? 1 : $recipient;
                return $message;
            });

            $conversation->messages()->saveMany($messages);
            $conversation->users()->attach([1, $recipient]);
        });
    }
}
