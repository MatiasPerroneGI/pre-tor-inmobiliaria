<?php

use Illuminate\Database\Seeder;

class SuscriptionsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		app('rinvex.subscriptions.plan')->create(
			[
				'name' => 'STARTER',
				'description' => 'Test',
				'price' => 10,
				'currency' => 'USD',

				'signup_fee' => 0,
				'invoice_period' => 1,
				'invoice_interval' => 'month',
				'trial_period' => 0,
				'trial_interval' => 'day',
				'sort_order' => 1,
			]
		);

		app('rinvex.subscriptions.plan')->create(
			[
				'name' => 'PREMIER',
				'description' => 'Test',
				'price' => 30,
				'currency' => 'USD',

				'signup_fee' => 0,
				'invoice_period' => 1,
				'invoice_interval' => 'month',
				'trial_period' => 0,
				'trial_interval' => 'day',
				'sort_order' => 2,
			]
		);

		app('rinvex.subscriptions.plan')->create(
			[
				'name' => 'ENTERPRISE',
				'description' => 'Test',
				'price' => 50,
				'currency' => 'USD',

				'signup_fee' => 0,
				'invoice_period' => 1,
				'invoice_interval' => 'month',
				'trial_period' => 0,
				'trial_interval' => 'day',
				'sort_order' => 3,
			]
		);

		app('rinvex.subscriptions.plan')->all()->each(function ($plan) {
			$features = factory(Rinvex\Subscriptions\Models\PlanFeature::class, $plan->id * 2)->make();
			$plan->features()->saveMany($features);
		});
	}
}