<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(ClauseTemplatesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);

        if (in_array(env('APP_ENV'), ['local', 'test'])) {
            // factory(\App\Property::class, 10)->create();
            // $this->call(ChatSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(SuscriptionsSeeder::class);
        }
    }
}
