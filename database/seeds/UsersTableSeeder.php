<?php

use App\Property;
use App\User;
use App\Agency;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    public function run()
    {

        Agency::create([
            'name' => 'Inmobiliaria',
            'legal_address' => 'Calle 123 ',
            'cuit' => '20-22333444-8',
            'img' => null,
        ]);
        
        $usuario = User::create([
            'name' => 'Usuario',
            'lastname' => 'de Prueba',
            'cuit' => '20-22333444-8',
            'dni' => '22333444',
            'email' => 'user@mail.com',
            'password' => bcrypt('123456'),
            'agency_id'=>1,
        ]);

        $permissions = [
            ['name' => 'viewAny contracts'],
            ['name' => 'view contracts'],
            ['name' => 'create contracts'],
            ['name' => 'update contracts'],
            ['name' => 'delete contracts'],
            ['name' => 'restore contracts'],
            ['name' => 'forceDelete contracts'],
            ['name' => 'viewAny users'],
            ['name' => 'view users'],
            ['name' => 'create users'],
            ['name' => 'update users'],
            ['name' => 'delete users'],
            ['name' => 'restore users'],
            ['name' => 'forceDelete users'],
        ];

        collect($permissions)->each(function ($data) use ($usuario) {
            $permission = Permission::create($data);
            $usuario->givePermissionTo($permission);
        });

        factory(User::class, 10)
            ->create()
            ->each(function ($user) use ($usuario){
                if ($isClient = rand(0,1) && !$user->id != 1) {
                    $usuario->agency->clients()->attach($user);
                }

                if ($amount = rand(0,3)) {
                    $props = factory(Property::class, $amount)->create()->each(function($p) use ($isClient,$user,$usuario){
                        if ($isClient) {
                            $propFromAgency = rand(0,1) ? $usuario->agency_id : null;
                            $p->update(['agency_id'=>$propFromAgency]);
                            $user->properties()->attach($p->id,['agencia'=>$propFromAgency]);
                        }
                    });
                }
            })
        ;
    }
}
