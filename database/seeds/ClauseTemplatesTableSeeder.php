<?php

use Illuminate\Database\Seeder;

class ClauseTemplatesTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('clause_templates')->insert([
            [
                'title' => 'PARTES',
                'type' => 'template',
                'content' => 'clauses.partes',
            ], 
            [
                'title' => 'OBJETO',
                'type' => 'template',
                'content' => 'clauses.objeto',
            ],
            [
                'title' => '-    PRIMERA: LA COSA',
                'type' => 'template',
                'content' => 'clauses.la-cosa',
            ],
            [
                'title' => '-   SEGUNDA: OBJETO, ACEPTACIÓN Y DESTINO:',
                'type' => 'template',
                'content' => 'clauses.destino',
            ],
            [
                'title' => '-   TERCERA: PLAZO, DEVOLUCIÓN Y MULTA:',
                'type' => 'template',
                'content' => 'clauses.plazo',
            ],
            [
                'title' => '-   CUARTA: PRECIO Y MORA:',
                'type' => 'template',
                'content' => 'clauses.precio',

             ],
             [
                'title' => '-   QUINTA: ADMINISTRACIÓN Y REGISTRO DE LA CONTRATACIÓN:',
                'type' => 'editable',
                'content' => '
                    En este acto las PARTES acuerdan centralizar la administración y registro del total desarrollo del CONTRATO bajo la plataforma online www.pre-tor.com (en adelante la “Plataforma de Administración Contractual”). En virtud de ello las PARTES se comprometen a dejar constancia dentro de la indicada plataforma online, y únicamente a través de los medios provistos por la misma, del cumplimiento o incumplimiento de todas las obligaciones establecidas por el presente, así como también de cualquier otra circunstancia que consideren de relevancia durante el desarrollo de la relación locativa.

                    
                    Las PARTES, en virtud del principio de buena fe contractual y por diligencia en cuanto al cumplimiento de las obligaciones asumidas en el CONTRATO, se comprometen a mantener accesos regulares a la referida Plataforma de Administración Contractual, durante el desarrollo de la totalidad de la relación locativa.
                ',
            ],
            [
                'title' => ' -   SEXTA: SERVICIOS E IMPUESTOS.',
                'type' => 'template',
                'content' => 'clauses.servicios'
            ],
            [
                'title' => '-   SEPTIMA:  CONSERVACIÓN, Y MEJORAS.',
                'type' => 'editable',
                'content' => '
                    El/la LOCADOR/A se obliga a conservar el INMUEBLE en el estado que permita al/ a la LOCATARIO/A usar y gozar del mismo conforme al destino previsto en la Cláusula Segunda del CONTRATO.
                    
                    El/la LOCATARIO/A asume la obligación de mantener la cosa en el estado que la recibió; de dar inmediata cuenta a el/la LOCADOR/A −a través de la Plataforma de Administración Contractual− de cualquier desperfecto que afecten la seguridad o el estado de conservación del INMUEBLE; y de permitir el libre acceso de el/la LOCADOR/A y/o a sus representantes a las dependencias del INMUEBLE, en la fecha a ser acordada a través de la referida plataforma para su inspección.
                    
                    MEJORAS. El/la LOCATARIO/A no podrá realizar mejora alguna en el INMUEBLE, ni obra que afecte o modifique la estructura construida, sin expresa autorización escrita de el/la LOCADOR/A, otorgada por medio de la Plataforma de Administración Contractual.
                    
                    Cualquier mejora que el/la LOCATARIO/A realice con la aprobación de el/la LOCADOR/A, podrá ser retirada por el/la LOCATARIO/A a la finalización de la relación locativa, siempre y cuando no afecte la estructura del INMUEBLE.
                    
                    El incumplimiento de cualquiera de las obligaciones asumidas por el/la LOCATARIO/A en la presente cláusula dará derecho a el/la LOCADOR/A a la resolución inmediata del presente CONTRATO y al reclamo de los daños y perjuicios emergentes de dichos incumplimientos.
                '
            ],
            [
                'title' => '-   OCTAVA: INCUMPLIMIENTOS Y RESOLUCIÓN ANTICIPADA:',
                'type' => 'editable',
                'content' => '
                    Cualquier incumplimiento del/de la LOCATARIO/A, sin perjuicio de las penalidades que se establecen en las demás cláusulas, habilitará al/a la LOCADOR/A a reclamar el cumplimiento de este CONTRATO y a la ejecución de las garantías establecidas en la Cláusula Décima.
                    
                    El/La LOCADOR/A podrá resolver el CONTRATO, por exclusiva culpa del LOCATARIO, cuando este último (i) afecte el INMUEBLE a un destino distinto al previsto en la cláusula Segunda, (ii) falte a su deber de conservar el INMUEBLE o haga abandono del mismo, o (iii) cuando el/la LOCATARIO/A incumpla con el pago de la prestación dineraria prevista en las clausulas Cuarta y Sexta, durante dos períodos consecutivos.
                    
                    En cualquiera de dichos casos el/la LOCADOR/A podrá accionar por el pago de lo adeudado y/o por el desalojo del INMUEBLE (art. 1086 CC y C), sin perjuicio de los daños y perjuicios cuyo reclamo correspondiera en cada caso. Previo a la resolución del contrato por la falta de pago del locatario en los términos indicados en el punto (iii) del párrafo anterior, el/la LOCADOR/A deberá intimar al/a la LOCATARIO/A, a través de la Plataforma de Administración Contractual, al pago de las sumas adeudadas en el plazo de 15 días corridos desde la recepción de dicha notificación.
                    
                    El/La LOCATARIO/A podrá reclamar el cumplimiento de este CONTRATO o resolverlo por exclusiva culpa del/de la LOCADOR/A, con el correspondiente reclamo de daños y perjuicios, cuando este último (i) incumpla con su obligación de conservar la cosa con aptitud para el use y goce del/de la LOCATARIO/A conforme al destino previsto en la Cláusula Segunda; o (ii) cuando el/la LOCATARIO/A se vea privado de la tenencia de la cosa por un tercero que resulte tener un mejor derecho sobre el INMUEBLE que el/la LOCADOR/A.
                    
                    El/la LOCATARIO/A podrá, trascurridos los seis (6) primeros meses de vigencia de la relación locativa, resolver el CONTRATO sin expresión de causa, debiendo notificar a través de la Plataforma de Administración Contractual su decisión a el/la LOCADOR/A con una antelación no menor a los sesenta días (60) anteriores a la fecha de restitución del INMUEBLE.
                    
                    En caso de que el/la LOCATARIO/A haga uso de la opción resolutoria citada dentro del primer año del CONTRATO, deberá abonar −en concepto de indemnización a favor de el/la LOCADOR/A− la suma equivalente a un mes y medio de locación al valor del canon que esté abonando al momento de hacer uso de dicha facultad, y la de un solo mes si la opción se ejercita con posterioridad al transcurso de dicho lapso. Para hacer uso de esta opción el/la LOCATARIO/A deberá estar al día en el cumplimiento de todas sus obligaciones. Además, deberá permitir al/a la LOCADOR/A a partir de la fecha de tal notificación, el ingreso al INMUEBLE para su exhibición a terceros interesados en un eventual alquiler.
                    
                    ABANDONO: Para el evento de que el/la LOCATARIO/A abandonare el INMUEBLE o depositare judicialmente las llaves, deberá abonar al/a la LOCADOR/A una multa igual al alquiler pactado desde la iniciación del juicio hasta el día en que el/la LOCADOR/A tome la libre y efectiva tenencia definitiva de la propiedad, debiendo indemnizarlo también por daños y perjuicios sufridos, siempre que dicha consignación de llaves no fuera imputable a conductas desplegadas por este último. Sin perjuicio de lo expuesto en caso de abandono, y para evitar los posibles deterioros que pudieran producirse y/o la ocupación ilegal de terceros, el/la LOCADOR/A queda facultado a retomar posesión del INMUEBLE por cualquier medio licito que resulte necesario a tal fin.
                '
            ],
            [
                'title' => '-   NOVENA:    RESPONSABILIDAD POR DAÑOS:',
                'type' => 'editable',
                'content' => '
                    Ni el/la LOCADOR/A ni el/la LOCATARIO/A responderán frente a cualquier hecho ajeno a su voluntad que ocurriere con motivo de la locación, incluso en el supuesto de caso fortuito o fuerza mayor.
                ',
            ],
            [
                'title' => '-   DECIMA:    DEPÓSITO Y DEMAS GARANTIAS:',
                'type' => 'template',
                'content' => 'clauses.deposito',
            ],
            [
                'title' => '-   DECIMA PRIMERA: NOTIFICACIONES Y DOMICILIOS:',
                'type' => 'template',
                'content' => 'clauses.notificaciones',
            ],
            [
                'title' => '-   DECIMO SEGUNDA: SELLADO:',
                'type' => 'editable',
                'content' => ' 
                    El impuesto de sellos (aforo) del presente CONTRATO será a cargo de las PARTES, en proporciones iguales. Cualquier otro gasto necesario para la ejecución del presente CONTRATO será asumido por la parte que lo haya generado.
                '
            ],
            [
                'title' => '-   DECIMO TERCERA: COMPETENCIA Y PROCEDIMIENTOS DE DESALOJO:',
                'type' => 'template',
                'content' => 'clauses.competencia',
            ],
            [
                'title' => '-',
                'type' => 'template',
                'content' => 'clauses.firma',
            ]
        ]);
    }
}
