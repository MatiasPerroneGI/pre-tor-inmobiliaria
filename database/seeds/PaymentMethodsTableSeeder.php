<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('payment_methods')->insert([
            ['value' => 'Efectivo'],
            ['value' => 'Transferencia'],
        ]);
    }
}
