<?php

use App\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        $admin = Admin::create([
        	'name' => 'Admin',
        	'email' => env('ADMIN_EMAIL'),
        	'password' => bcrypt(env('ADMIN_PASSWORD')),
        ]);

        $permissions = [
            //products
            ['name' => 'viewAny contracts', 'guard_name' => 'admin'],
            ['name' => 'view contracts', 'guard_name' => 'admin'],
            ['name' => 'create contracts', 'guard_name' => 'admin'],
            ['name' => 'update contracts', 'guard_name' => 'admin'],
            ['name' => 'delete contracts', 'guard_name' => 'admin'],
            ['name' => 'restore contracts', 'guard_name' => 'admin'],
            ['name' => 'forceDelete contracts', 'guard_name' => 'admin'],
        ];

        collect($permissions)->each(function ($data) use ($admin) {
            $permission = Permission::create($data);
            $admin->givePermissionTo($permission);
        });
    }
}
