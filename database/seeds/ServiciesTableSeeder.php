<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('services')->insert([
            ['value' => 'Canon Mensual'],
            ['value' => 'Agua'],
            ['value' => 'Energía eléctrica'],
            ['value' => 'Gas'],
            ['value' => 'Expensas'],
            ['value' => 'ABL'],
        ]);
    }
}
