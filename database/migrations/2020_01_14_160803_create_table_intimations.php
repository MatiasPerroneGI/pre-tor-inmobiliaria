<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIntimations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intimations', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->boolean("is_resolved")->default(0);
			$table->boolean("include_guarantors")->default(0);
            $table->integer("contract_id")->index();
            $table->integer("conversation_id");
            $table->integer("user_id")->index()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intimations');
    }
}
