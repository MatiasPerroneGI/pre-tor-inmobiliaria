<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Database extends Migration
{
    public function up()
    {
        //IMAGES
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('src');
            $table->string('url')->nullable();
            $table->integer('imageable_id')->nullable()->unsigned();
            $table->string('imageable_type', 80)->nullable();
            $table->tinyInteger('is_video')->default(0)->unsigned();
            $table->boolean('pending')->default(1);
            $table->smallInteger('order')->default(0)->unsigned();
            $table->timestamps();
            //indices
            $table->index(['imageable_id', 'imageable_type']);
            $table->index('order');
        });

        //IMAGES INFO
        Schema::create('images_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('value');
            $table->integer('image_id')->unsigned()->index();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('area', 30);
            $table->text('description');
            $table->text('accessories');
            // $table->integer('user_id')->unsigned()->index();
            $table->integer('agency_id')->nullable()->index();

            $table->timestamps();
            $table->softdeletes();
        });

        Schema::create('property_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('agencia')->nullable()->index();

            // $table->unique(['user_id', 'property_id']);
            $table->unique(['user_id', 'property_id','agencia']);
        });

        //ADDRESSES
        // Schema::create('addresses', function (Blueprint $table) {
        //     $table->smallIncrements('id');
        //     $table->string('guarantor_address');
        //     $table->string('owner_address');
        //     $table->string('client_address');
        //     $table->softDeletes();
        //     $table->timestamps();
        //
        //     $table->integer('contract_id')->unsigned()->index();
        // });

        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned()->index();
            $table->tinyInteger('type')->unsigned(); //0: banco / 1: mercado pago
            //campos con información de la cuenta
            $table->string('bank');
            $table->string('number');
            $table->string('cbu');
            $table->string('owner');
            $table->string('alias');
        });

        Schema::create('contract_types', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->timestamps();
            $table->string('title');
            $table->string('view');
            $table->text('content');
            //campos del modelo, texto, tags para reemplazar, etc
        });

        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('place');
            $table->string('jurisdiction');
            $table->string('activity'); //TODO: actividad que se desarrollara en el comercio.
            $table->smallInteger('duration')->unsigned(); //cuanto dura el contrato
            $table->date('start');
            $table->date('end');
            $table->float('price', 8, 2);
            $table->float('increase', 8, 2);
            $table->tinyInteger('time')->unsigned();
            $table->float('penalty', 8, 2);
            $table->tinyInteger('deposit_time')->unsigned();
            $table->float('deposit_value', 8, 2);
            $table->float('total', 10, 2); //99.999.999,99
            $table->tinyInteger('payment_method_id')->unsigned(); //Forma de pago (MP, depósito, etc)
            $table->boolean('anticipated_payment'); //Pago a mes vencido o mes actual
            $table->boolean('dollar_deposit'); //Pago a mes vencido o mes actual
            $table->boolean('commerce');
            $table->text('extra_data');
            $table->boolean('confirmed')->default(0);
            $table->tinyInteger('model_id')->unsigned()->nullable()->index(); //TODO: resolver
            $table->integer('property_id')->unsigned()->index();
            $table->timestamps();
        });

        //Seguro contra incendios
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('policy', 100);
            $table->string('company', 100)->nullable();
            $table->integer('contract_id')->unsigned()->index();

            $table->timestamps();
        });

        Schema::create('guarantors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('lastname', 100)->nullable();
            $table->string('guaranty_address')->nullable();
            $table->string('legal_address');
            $table->string('polize')->nullable();
            $table->string('email');
            $table->string('dni', 20);
            $table->tinyInteger('type')->unsigned(); // type 0, garante por bienes propios - type 1, garante con propiedad, requiere address y plate - type 2, aseguradora, solo requiere nombre ¿cuil? y email
            $table->string('plate')->nullable();

            $table->integer('contract_id')->unsigned()->index();

            $table->timestamps();
        });

        Schema::create('versions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned()->index(); //quien creó la versión
            $table->boolean('confirmed')->default(0);
            $table->integer('contract_id')->unsigned()->index();
            $table->text('content');
            $table->string('pdf');
            $table->smallInteger('number')->unsigned();
        });

        Schema::create('clauses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->text('content'); //En la creación del contrato el contenido viene preseteado de un template
            $table->enum('type', ['editable', 'appendable', 'template']);
            $table->boolean('approved')->default(0);
            $table->boolean('edited')->default(0);
            $table->integer('version_id')->unsigned()->index();
        });

        Schema::create('clause_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->text('content');
            $table->enum('type', ['editable', 'appendable', 'template']);
            $table->tinyInteger('order')->default(0);
            $table->boolean('is_visible')->default(1);
        });

        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('step')->unsigned();
            $table->date('start');
            $table->date('end');
            $table->float('price', 8, 2)->unsigned();
            $table->integer('contract_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('payment_methods', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value', 50);
        });

        //tabla de relación
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legal_address')->nullable();
            $table->integer('contract_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
        });

        //tabla de relación
        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legal_address')->nullable();
            $table->integer('contract_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
        });

        //tabla de relación
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('legal_address');
            $table->string('cuit');
            $table->string('img')->nullable();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();

        });

        Schema::create('signatures', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('version_id')->unsigned()->index();
            $table->smallInteger('author')->unsigned(); //locador / locatario
            $table->integer('user_id')->unsigned()->index();
            $table->smallInteger('type')->unsigned(); //física / digital
            $table->binary('token')->nullable(); //token firma digital
            $table->string('img')->nullable(); //foto firma física
        });

        Schema::create('services', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('value', 120);
            $table->timestamps();

            //nombre / categoría, información general del servicio
        });

        Schema::create('contract_service', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('contract_id')->unsigned()->index();
            $table->integer('service_id')->unsigned()->index();
            $table->decimal('value', 8, 2)->nullable();
        });

        //Payment
        //Pago de servicios - pago del mes
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //morph
            $table->smallInteger('service_id')->unsigned();
            $table->integer('contract_id')->unsigned();
            $table->json('tenants_id');
            $table->date('expiration');
            $table->date('payment_date')->nullable();
            $table->decimal('value', 8, 2);
            $table->string('payment_proof')->nullable(); //archivo comprobante - foto
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->boolean("read")->default(0)->unsigned();
            $table->text("message");
            // $table->bigInteger("message_id");
            $table->integer("user_id");
            $table->integer("conversation_id");
            $table->index(['conversation_id','user_id']);
        });

        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->string("subject");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('conversation_id');
            $table->integer('user_id');

            $table->timestamps();
            $table->index(['conversation_id', 'user_id']);
        });

        //FILES morpheable
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('src');
            $table->string('original_name');
            $table->integer('fileable_id')->nullable()->unsigned();
            $table->string('fileable_type', 80)->nullable();
            $table->boolean('pending')->default(1);
            $table->timestamps();

            //indices
            $table->index(['fileable_id', 'fileable_type']);
        });

        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('message');
            $table->date('date')->nullable();
            $table->string('frequency')->nullable();
            $table->string('param', 30)->nullable();

            $table->integer("user_id")->unsigned()->index();
            $table->timestamps();
        });

        // Eventos concretos creados a través de la programación o fijos relizados por el usuario
        Schema::create("events",function(Blueprint $table){
            $table->increments('id');
            $table->string('subject');
            $table->text('message');
            $table->date('date');

            $table->integer("user_id")->unsigned()->index();
            $table->integer("schedule_id")->unsigned()->index();
            $table->boolean('is_read')->default(0);
            $table->timestamps();
        });



        // // PLANES Y SUSCRIPCIONES
        Schema::create(config('rinvex.subscriptions.tables.plans'), function (Blueprint $table) {
            // Columns
            $table->increments('id');
            $table->string('slug');
            $table->text('name');
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->decimal('price')->default('0.00');
            $table->decimal('signup_fee')->default('0.00');
            $table->string('currency', 3);
            $table->smallInteger('trial_period')->unsigned()->default(0);
            $table->string('trial_interval')->default('day');
            $table->smallInteger('invoice_period')->unsigned()->default(0);
            $table->string('invoice_interval')->default('month');
            $table->smallInteger('grace_period')->unsigned()->default(0);
            $table->string('grace_interval')->default('day');
            $table->tinyInteger('prorate_day')->unsigned()->nullable();
            $table->tinyInteger('prorate_period')->unsigned()->nullable();
            $table->tinyInteger('prorate_extend_due')->unsigned()->nullable();
            $table->smallInteger('active_subscribers_limit')->unsigned()->nullable();
            $table->mediumInteger('sort_order')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->unique('slug');
        });

        Schema::create(config('rinvex.subscriptions.tables.plan_features'), function (Blueprint $table) {
            // Columns
            $table->increments('id');
            $table->integer('plan_id')->unsigned();
            $table->string('slug');
            $table->text('name');
            $table->text('description')->nullable();
            $table->string('value');
            $table->smallInteger('resettable_period')->unsigned()->default(0);
            $table->string('resettable_interval')->default('month');
            $table->mediumInteger('sort_order')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->unique(['plan_id', 'slug']);
            $table->foreign('plan_id')->references('id')->on(config('rinvex.subscriptions.tables.plans'))
                  ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create(config('rinvex.subscriptions.tables.plan_subscriptions'), function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('user');
            $table->integer('plan_id')->unsigned();
            $table->string('slug');
            $table->text('name');
            $table->text('description')->nullable();
            $table->dateTime('trial_ends_at')->nullable();
            $table->dateTime('starts_at')->nullable();
            $table->dateTime('ends_at')->nullable();
            $table->dateTime('cancels_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->string('timezone')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->unique('slug');
            $table->foreign('plan_id')->references('id')->on(config('rinvex.subscriptions.tables.plans'))
                  ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create(config('rinvex.subscriptions.tables.plan_subscription_usage'), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_id')->unsigned();
            $table->integer('feature_id')->unsigned();
            $table->smallInteger('used')->unsigned();
            $table->dateTime('valid_until')->nullable();
            $table->string('timezone')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['subscription_id', 'feature_id']);
            $table->foreign('subscription_id')->references('id')->on(config('rinvex.subscriptions.tables.plan_subscriptions'))
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('feature_id')->references('id')->on(config('rinvex.subscriptions.tables.plan_features'))
                  ->onDelete('cascade')->onUpdate('cascade');
        });

        // Clientes. Relación entre inmobiliarias y sus clientes
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->index();
            $table->integer('client_id')->index();
        });

        //Logs de emails
        Schema::create('emails_log', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->text('body');
            $table->string('to');
            $table->string('subject');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('payment_service');
        Schema::dropIfExists('contract_service');
        Schema::dropIfExists('services');
        Schema::dropIfExists('reminders');
        Schema::dropIfExists('signatures');
        Schema::dropIfExists('tenants');
        Schema::dropIfExists('owners');
        Schema::dropIfExists('periods');
        Schema::dropIfExists('versions');
        Schema::dropIfExists('guarantors');
        Schema::dropIfExists('contracts');
        Schema::dropIfExists('payment_methods');
        Schema::dropIfExists('models');
        Schema::dropIfExists('accounts');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('images');
        Schema::dropIfExists('images_info');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('conversations');
        Schema::dropIfExists('participants');
        Schema::dropIfExists('events');
        Schema::dropIfExists('schedules');
        Schema::dropIfExists('files');
        Schema::dropIfExists('emails_log');

        Schema::dropIfExists(config('rinvex.subscriptions.tables.plan_subscription_usage'));
        Schema::dropIfExists(config('rinvex.subscriptions.tables.plan_subscriptions'));
        Schema::dropIfExists(config('rinvex.subscriptions.tables.plan_features'));
        Schema::dropIfExists(config('rinvex.subscriptions.tables.plans'));
    }

     /**
     * Get jsonable column data type.
     *
     * @return string
     */
    protected function jsonable(): string
    {
        $driverName = DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME);
        $dbVersion = DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION);
        $isOldVersion = version_compare($dbVersion, '5.7.8', 'lt');

        return $driverName === 'mysql' && $isOldVersion ? 'text' : 'text';
    }
}
