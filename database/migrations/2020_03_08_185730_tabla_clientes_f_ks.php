<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaClientesFKs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->change();
            $table->unsignedInteger('agency_id')->change();
            $table->foreign('client_id', 'FK_clients_users')->references('id')->on('users');
            $table->foreign('agency_id', 'FK_clients_agencies')->references('id')->on('agencies');
            $table->dropIndex('clients_agency_id_index');
            $table->dropIndex('clients_client_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('FK_clients_users');
            $table->dropForeign('FK_clients_agencies');
            $table->integer('client_id')->change();
            $table->integer('agency_id')->change();
            $table->index('agency_id', 'clients_agency_id_index');
            $table->index('client_id', 'clients_client_id_index');
        });
    }
}