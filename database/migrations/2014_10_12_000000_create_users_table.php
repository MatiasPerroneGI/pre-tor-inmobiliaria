<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 80);
            $table->string('lastname', 80);
            $table->string('dni', 15);
            $table->string('cuit', 15);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('image', 60)->nullable();
            // TODO: Check nullable
            $table->integer('agency_id')->nullable()->index();
            $table->string('api_token', 60)->nullable()->index();
            $table->rememberToken();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
