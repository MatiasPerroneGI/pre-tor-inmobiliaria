<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->firstName;
    return [
        'name' => $name,
        'lastname' => $faker->lastName,
        'cuit' => $faker->numerify('##-########-#'),
        'email' => str_slug($name) . '@mail.com',
        'password' => bcrypt('123456'),
        'dni' => $faker->numberBetween(20000000, 40000000)
    ];
});
