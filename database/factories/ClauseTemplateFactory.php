<?php

use App\ClauseTemplate;
use Faker\Generator as Faker;

$factory->define(ClauseTemplate::class, function (Faker $faker) {
	static $order = 0;

    return [
        'title' => $faker->sentence(2),
        'content' => $faker->paragraph,
        'is_visible' => 1,
        'order' => $order++,
    ];
});
