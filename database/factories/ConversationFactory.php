<?php

use App\Conversation;
use Faker\Generator as Faker;

$factory->define(Conversation::class, function (Faker $faker) {
    return [
        'subject' => $faker->sentence(6, true),
    ];
});
