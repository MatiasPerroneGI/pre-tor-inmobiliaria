<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rinvex\Subscriptions\Models\PlanFeature;

$factory->define(Rinvex\Subscriptions\Models\PlanFeature::class, function (Faker $faker) {
	
    return [
        'name'=>$faker->sentence(3,true),
        'description'=>$faker->sentence(4,true),
        'value'=>"1",
        'sort_order'=>2,
    ];
});
