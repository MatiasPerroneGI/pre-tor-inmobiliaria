<?php

use App\Property;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'area' => rand(10, 1000),
        'description' => $faker->sentence,
        'accessories' => implode(', ', $faker->words(rand(1,5))),
        'agency_id'=>null,
    ];
});
