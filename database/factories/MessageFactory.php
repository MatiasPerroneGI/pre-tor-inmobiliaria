<?php

use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    return [
        'message'=> $faker->sentences(3, true),
        'created_at'=> $faker->dateTimeBetween('-1 hour'),
        'updated_at'=> \Carbon\Carbon::now(),
    ];
});
