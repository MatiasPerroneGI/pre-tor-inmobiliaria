@extends('backoffice.layouts.app')

@section('section', 'Propiedades')

@section('icon', 'building')

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 

        {!! KameForm::text('address')->col('md-8 sm-12') !!}
        
        {!! KameForm::text('area')->col('md-4 sm-12') !!}
        
        {!! KameForm::editable('description') !!}
        
        {!! KameForm::textarea('accessories') !!}

        @include('admin.components.images-uploader', ['resource' => 'properties'])
  
        {!! KameForm::submit('Enviar') !!}

    {!! KameForm::close() !!}
@endsection

@section('scripts')
    <script src="/backend/js/app/Product.js"></script>
@endsection
