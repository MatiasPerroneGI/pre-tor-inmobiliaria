@extends('backoffice.layouts.app')

@section('section', 'Log de emails')

@section('action', 'Listar')

@section('icon', 'envelope')

@section('widget-body')
    <!-- datatable start -->
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Asunto</th>
                <th>Destinatarios</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Asunto</th>
                <th>Destinatarios</th>
                <th>Acciones</th>
            </tr>
        </tfoot>
    </table>
    <!-- datatable end -->
@endsection

@section('scripts')
    <script src="/backend/js/test/EmailsLogs.js"></script>
@endsection
