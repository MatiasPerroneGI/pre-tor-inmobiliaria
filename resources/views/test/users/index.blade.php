@extends('backoffice.layouts.app')

@section('section', 'Usuarios')

@section('action', 'Listar')

@section('icon', 'users')

@section('widget-body')
    <!-- datatable start -->
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </tfoot>
    </table>
    <!-- datatable end -->
@endsection

@section('scripts')
    <script src="/backend/js/test/Users.js"></script>
@endsection
