<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
    @include('front.partials.head')
    @yield('head')
</head>
<body>
    @include('front.partials.navbar')

	<div class="offcanvas-wrapper" style="margin-top:130px;">

        @include('front.partials.alert')

		@yield('content')

        @include('front.partials.footer')

    </div>
    <div class="site-backdrop"></div>

    @include('front.partials.scripts')

    @yield('scripts')
</body>
</html>
