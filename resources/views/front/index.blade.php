@extends('front.app')

@section('content')
    @include('front.partials.home.slider')

    <section class="section_all" id="about">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <div class="section_icons">
                              <i class="mbri-target"></i>
                          </div>
                          <h3 class="mt-3">¿ Cómo podemos  <span class="text_custom">ayudarte </span>?</h3>
                          <p class="section_subtitle mx-auto text-muted">Valoramos el tiempo, hacemos simple la elaboracion de contratos mediante una herramienta segura, aficiente e inteligente creada para todas y cada una de las partes.</p>
                      </div>
                  </div>
              </div>

              <div class="row mt-5">
                  <div class="col-lg-4">
                      <div class="about_content_box_all text-center mt-3">
                          <div class="about_detail">
                              <div class="about_icon">
                                  <i class="text_custom mdi mdi-android-studio text-center"></i>
                              </div>
                              <h5 class="text-dark text-capitalize mt-3">INDIVIDUOS</h5>
                              <p class="edu_desc mt-3 mb-0 text-muted">Contratos inteligentes y seguros, a un sólo click.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="about_content_box_all text-center mt-3">
                          <div class="about_detail">
                              <div class="about_icon">
                                  <i class="text_custom mdi mdi-code-tags text-center"></i>
                              </div>
                              <h5 class="text-dark text-capitalize mt-3">EMPRESAS</h5>
                              <p class="edu_desc mb-0 mt-3 text-muted">Eficiencia, confianza y control corpotativo.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="about_content_box_all text-center mt-3">
                          <div class="about_detail">
                              <div class="about_icon">
                                  <i class="text_custom mdi mdi-tablet-android text-center"></i>
                              </div>
                              <h5 class="text-dark text-capitalize mt-3">ABOGADOS</h5>
                              <p class="edu_desc mb-0 mt-3 text-muted">Tu tiempo vale, tareas simples y expeditivas.</p>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row mt-5 vertical_content_manage">
                  <div class="col-lg-12">
                      <div class="app_about_box_content mx-auto text-center mt-3">
                          <h4 class="text-capitalize"> Participá de la comunidad Pre-tor</h4>
                          <div class="feauters-content mt-3">
                              <p class="feauters-subtitle text-muted mt-3">
                                  Individuos, empresas y abogados en una misma plataforma online.
                                  <br>
                                  Celebrar un contrato nunca antes fue tan fácil.
                              </p>
                          </div>
                          <div class="mt-5">
                              <a href="#" class="btn btn_custom">Test Gratuito</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- About End -->

      <!-- Start Cta -->
      <section class="section_all bg_cta_business_img">
          <div class="bg_overlay_cover_on"></div>
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="text-center text-white">
                          <h2 class="">Lorem ipsum dolor sit amet.</h2>
                          <p class="mx-auto cta_details mt-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea .</p>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- End Cta -->
      <!-- Why Choose Us Start -->
      <section class="section_all">
          <div class="container">
              <div class="row vertical_content_manage">
                  <div class="col-lg-7">
                      <div class="app_description_header mt-3">
                          <h4 class="text-capitalize font-weight-normal">Modelos de contratos</h4>
                          <p class="text-muted mt-3">Pre-tor es una herramienta inteligente que asistirá a las partes involucradas para toma de desiciones en la celebracion de diferentes tipos de contratos.</p>

                          <div class="row">
                              <div class="col-lg-6">
                                  <div class="business_choose_box p-3 mt-3">
                                      <div class="business_choose_icon">
                                          <span class=""></span>
                                      </div>
                                      <div class="business_choose_details mt-2">
                                          <h6 class="text-capitalize mb-0">Locador</h6>
                                          <p>Especifique sus términos comerciales y determine cláusular mas relevantes</p>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-lg-6">
                                  <div class="business_choose_box p-3 mt-3">
                                      <div class="business_choose_icon">
                                          <span class=""></span>
                                      </div>
                                      <div class="business_choose_details mt-2">
                                          <h6 class="text-capitalize mb-0">Locatario</h6>
                                          <p>Garantice sus derechos y estavlezca sus conficiones de garantía.</p>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>
                  <div class="col-lg-5">
                      <div class="mt-3">
                          <img src="/img/m1.png" alt="" class="img-fluid mx-auto d-block">
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- Descrption end -->


      <!-- Portfolio Start -->
      <section class="section_all bg-light" id="portfolio">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <div class="section_icons">
                              <i class="mbri-image-gallery"></i>
                          </div>
                          <h3 class="mt-3">Conocé nuestra herramienta</h3>
                          <p class="section_subtitle mx-auto text-muted">Diseño moderno, ágil y dinámico que otorga flexibilidad en cada una de las instancias del proceso contractual . Adminitración centralizada.</p>

                      </div>
                  </div>
              </div>
              <div class="row mt-5">
                  <ul class="col list-unstyled list-inline text-center mb-0 text-uppercase work_menu" id="menu-filter">
                      <li class="list-inline-item"><a class="active" data-filter="*">All</a></li>
                      <li class="list-inline-item"><a class="" data-filter=".seo">Individuos</a></li>
                      <li class="list-inline-item"><a class="" data-filter=".webdesign">Empresas</a></li>
                      <li class="list-inline-item"><a class="" data-filter=".WORK">Notificaciones</a></li>
                      <li class="list-inline-item"><a class="" data-filter=".wordpress">Contratos</a></li>
                  </ul>
              </div>
          </div>
          <div class="container">
              <div class="row mt-5 work-filter">
                  <div class="col-lg-4 seo wordpress">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/1.jpg">
                              <img class="img-fluid " src="/img/portfolio/1.jpg" alt="1" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="col-lg-4 php webdesign WORK">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/2.jpg">
                              <img class="img-fluid" src="/img/portfolio/2.jpg" alt="2" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="col-lg-4 seo php">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/3.jpg">
                              <img class="img-fluid" src="/img/portfolio/3.jpg" alt="4" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="col-lg-4 wordpress webdesign html">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/4.jpg">
                              <img class="img-fluid" src="/img/portfolio/4.jpg" alt="4" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="col-lg-4 html php">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/5.jpg">
                              <img class="img-fluid" src="/img/portfolio/5.jpg" alt="5" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="col-lg-4 WORK webdesign seo wordpress">
                      <div class="">
                          <a class="img-zoom" href="/img/portfolio/6.jpg">
                              <img class="img-fluid" src="/img/portfolio/6.jpg" alt="6" />
                              <div class="">
                                  <div class="">
                                      <p class="text-light"></p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
          <!-- End Work Gallary -->
      </section>
      <!-- End Work Section -->

      <!-- Start Video -->
      <section class="section_all bg_business_video">
          <div class="bg_overlay_cover_on"></div>
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <h3 class="text-white">Video<span class="text-white"> Presentation</span></h3>
                          <p class="section_subtitle mx-auto text-white">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                      </div>
                  </div>
              </div>
              <div class="row mt-5">
                  <div class="col-lg-12">
                      <div class="video-btn-white text-center">
                          <a href="http://vimeo.com/99025203" class="business_play_btn features_video mx-auto">
                              <i class="mdi mdi-play"></i>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- End Video -->

      <!-- Features Start -->
      <section class="section_all" id="features">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <div class="section_icons">
                              <i class="mbri-features"></i>
                          </div>
                          <h3 class="mt-3">Los Beneficios de  <span class="text_custom"> Pre-tor  </span></h3>
                          <p class="section_subtitle mx-auto text-muted">Tus contratos, en las mejores manos: las tuyas.</p>

                      </div>
                  </div>
              </div>

              <div class="row mt-5">
                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_1 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-touch font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              <h5 class="text-white"></h5>
                              <p class="mt-3 mb-0">Contratos inteligentes que están predefinidos bajo las normativas vigentes.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_2 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-login font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              {{-- <h5 class="text-white">Login App</h5> --}}
                              <p class="mt-3 mb-0">El contrato se crea automaticamente en base a la información brindada por las partes.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_3 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-setting font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              {{-- <h5 class="text-white">Additional App</h5> --}}
                              <p class="mt-3 mb-0">Suscripcion mensual, sin costos adicionales. Menos del 8% de un canon locativo.</p>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row mt-5">
                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_2 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-rocket font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              {{-- <h5 class="text-white">High Performance</h5> --}}
                              <p class="mt-3 mb-0">Monitoreo constante; ante cualquier incumplimiento de alguna de las partes la plataforma indicara el proceder legal adecuado y lo ejecutará mediante un click.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_3 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-users font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              {{-- <h5 class="text-white">Multiple User</h5> --}}
                              <p class="mt-3 mb-0">Modificaciones del contrato, como así también todo tipo de notificaciones quedan.</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="business_services_boxes service_color_1 mt-3 p-5">
                          <div class="business_services_icon">
                              <i class="mbri-preview font-weight-bold text-center"></i>
                          </div>
                          <div class="business_services_content mt-3">
                              {{-- <h5 class="text-white">24/7 Spport</h5> --}}
                              <p class="mt-3 mb-0">Generación de informes mensuales y elaboración de reporte final al término de cadacontrato.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- Features End  -->



      <!-- Testimonial Start  -->
      <section class="section_all bg_testimonial_business" id="client">
          <div class="bg_overlay_cover_on"></div>
          <div class="container">
              <div class="row">
                  <div class="col-lg-4">
                      <div class="bg-white p-4 app_testi_box mx-auto text-center">
                          <div class="testi_icon">
                              <i class="mbri-user text_custom"></i>
                          </div>
                          <p class="review_box">"Luctus lobortis dui lobortis aptent tempus nec pellentesque, dapibus commodo mollis cum fermentum nec. Lorem Ipsum is simply dummy text of the printing. "</p>

                          <p class="client_name text-center mb-0 mt-4 font-weight-bold">- Steven Joneson, Envato</p>
                      </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="bg-white p-4 app_testi_box mx-auto text-center">
                          <div class="testi_icon">
                              <i class="mbri-user2 text_custom"></i>
                          </div>
                          <p class="review_box">"Luctus lobortis dui lobortis aptent tempus nec pellentesque, dapibus commodo mollis cum fermentum nec. Lorem Ipsum is simply dummy text of the printing. "</p>

                          <p class="client_name text-center mb-0 mt-4 font-weight-bold">- Anna Arceneaux, Envato</p>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="bg-white p-4 app_testi_box mx-auto text-center">
                          <div class="testi_icon">
                              <i class="mbri-github text_custom"></i>
                          </div>
                          <p class="review_box">"Luctus lobortis dui lobortis aptent tempus nec pellentesque, dapibus commodo mollis cum fermentum nec. Lorem Ipsum is simply dummy text of the printing. "</p>

                          <p class="client_name text-center mb-0 mt-4 font-weight-bold">- Jeffrey Snyder, Envato</p>
                      </div>

                  </div>

              </div>
          </div>
      </section>
      <!--  Testimonial End  -->



      <!-- Start Pricing -->
      <section class="section_all bg-light" id="pricing">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <div class="section_icons">
                              <i class="mbri-like"></i>
                          </div>
                          <h3 class="mt-3">Suscripción <span class="text_custom"> mensual  </span></h3>
                          <p class="section_subtitle mx-auto text-muted">El mejor plan, es legalmente cubierto.</p>
                      </div>
                  </div>
              </div>
              <div class="row mt-5 vertical_content_manage">
                  <div class="col-lg-4">
                      <div class="app_price_box bg-white text-center bg-white mt-3">
                          <div class="app_plan_name">
                              <h3 class="text_custom">Basic</h3>
                              <p>Perfact For you</p>
                          </div>
                          <div class="pricing_header mt-4">
                              <div class="app_price_tag_heading mt-4">
                                  <h2 class="mb-0"><sub>$</sub>10/<span>month</span></h2>
                              </div>
                          </div>
                          <div class="app_list_price_features text_muted mb-4 mt-4">
                              <p> Additional Features</p>
                              <p>Maximum Support</p>
                              <p>24/7 Support</p>
                              <p class="mb-0">Free Email Support</p>
                          </div>

                          <div class="">
                              <a href="#" class="btn btn_custom btn_rounded">Choose a Plan</a>
                          </div>
                      </div>

                  </div>

                  <div class="col-lg-4">
                      <div class="app_price_box bg-white text-center mt-3">
                          <div class="app_plan_name">
                              <h3 class="text_custom">Standard</h3>
                              <p>Perfact For you</p>
                          </div>
                          <div class="pricing_header mt-4">
                              <div class="app_price_tag_heading mt-4">
                                  <h2 class="mb-0"><sub>$</sub>20/<span>month</span></h2>
                              </div>
                          </div>
                          <div class="app_list_price_features text_muted mb-4 mt-4">
                              <p> Additional Features</p>
                              <p>Maximum Support</p>
                              <p>24/7 Support</p>
                              <p class="mb-0">Free Email Support</p>
                          </div>

                          <div class="">
                              <a href="#" class="btn btn_custom btn_rounded">Choose a Plan</a>
                          </div>
                      </div>

                  </div>

                  <div class="col-lg-4">
                      <div class="app_price_box bg-white text-center bg-white mt-3">
                          <div class="app_plan_name">
                              <h3 class="text_custom">Premium</h3>
                              <p>Perfact For you</p>
                          </div>
                          <div class="pricing_header mt-4">
                              <div class="app_price_tag_heading mt-4">
                                  <h2 class="mb-0"><sub>$</sub>30/<span>month</span></h2>
                              </div>
                          </div>
                          <div class="app_list_price_features text_muted mb-4 mt-4">
                              <p> Additional Features</p>
                              <p>Maximum Support</p>
                              <p>24/7 Support</p>
                              <p class="mb-0">Free Email Support</p>
                          </div>

                          <div class="">
                              <a href="#" class="btn btn_custom btn_rounded">Choose a Plan</a>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
      </section>
      <!-- End Pricing -->

      <!-- Counter Start -->
      <section class="section_all bg_business_counter_cover">
          <div class="bg_overlay_cover_on"></div>
          <div class="container">
              <div class="row" id="counter">
                  <div class="col-lg-3">
                      <div class="text-center counter_box p-4 mt-3 bg-white rounded">
                          <div class="counter_icon mb-3">
                              <i class="text_custom mdi mdi-alarm"></i>
                          </div>
                          <h1 class="counter_value mb-1" data-count="654">4 </h1>
                          <p class="info_name mb-0">Working Hours</p>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="text-center counter_box p-4 mt-3 bg-white rounded">
                          <div class="counter_icon mb-3">
                              <i class="text_custom mdi mdi-box-shadow"></i>
                          </div>
                          <h1 class="counter_value mb-1" data-count="6400">10</h1>
                          <p class="info_name mb-0">Completed Projects</p>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="text-center counter_box p-4 mt-3 bg-white rounded">
                          <div class="counter_icon mb-3">
                              <i class="text_custom mdi mdi-account-box-outline"></i>
                          </div>
                          <h1 class="counter_value mb-1" data-count="2389">201</h1>
                          <p class="info_name mb-0">No. of Clients</p>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="text-center counter_box p-4 mt-3 bg-white rounded">
                          <div class="counter_icon mb-3">
                              <i class="text_custom mdi mdi-account-multiple-outline"></i>
                          </div>
                          <h1 class="counter_value mb-1" data-count="653">2</h1>
                          <p class="info_name mb-0">Team Member</p>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- Counter End -->

      <!-- Contact Us Start -->
      <section class="section_all" id="contact">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="section_title_all text-center">
                          <div class="section_icons">
                              <i class="mbri-download"></i>
                          </div>
                          <h3 class="mt-3">Comience a uttilizar<span class="text_custom"> Pre-tor </span> de ma era gratuita</h3>
                          <p class="section_subtitle mx-auto text-muted">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                      </div>
                  </div>
              </div>
              <div class="row mt-5">
                  <div class="col-lg-12">
                      <div class="business_form_custom landing_form_custom soft_form_custom mx-auto mt-3">
                          <form>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="form-group mt-3">
                                          <input name="name" id="name" type="text" class="form-control" placeholder="Your name..." required="">
                                      </div>
                                  </div>
                                  <div class="col-lg-12">
                                      <div class="form-group mt-3">
                                          <input name="email" id="email" type="email" class="form-control" placeholder="Your email..." required="">
                                      </div>
                                  </div>
                                  <div class="col-lg-12">
                                      <div class="form-group mt-3">
                                          <input type="text" class="form-control" id="subject" placeholder="Your Subject.." required="">
                                      </div>
                                  </div>
                                  <div class="col-lg-12">
                                      <div class="form-group mt-3">
                                          <textarea name="comments" id="comments" rows="4" class="form-control" placeholder="Your message..." required=""></textarea>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <input type="submit" class="btn btn_custom w-100" value="Enviar">
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </section>
@endsection
