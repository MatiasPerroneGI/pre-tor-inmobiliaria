@extends('front.app')

@section('content')
      <!--  Testimonial End  -->


      <!-- Start Pricing -->
<section class="section_all bg-light" id="pricing">
  <div class="container">
      <div class="row">
          <div class="col-lg-12">
              <div class="section_title_all text-center">
                  <div class="section_icons">
                      <i class="mbri-like"></i>
                  </div>
                  <h3 class="mt-3">Selecciona tu <span class="text_custom"> plan  </span></h3>
                  <p class="section_subtitle mx-auto text-muted">El mejor plan, es legalmente cubierto.</p>
              </div>
          </div>
      </div>
      <div class="row mt-5 vertical_content_manage">
        @foreach ($plans as $plan)
          <div class="col-lg-4">
              <div class="app_price_box bg-white text-center bg-white mt-3">
                  <div class="app_plan_name">
                      <h3 class="text_custom">{{ $plan->name }}</h3>
                      <p>{{ $plan->name }}</p>
                  </div>
                  <div class="pricing_header mt-4">
                      <div class="app_price_tag_heading mt-4">
                      <h2 class="mb-0"><sub>$</sub>{{ $plan->price }}<span>/ mes</span></h2>
                      </div>
                  </div>

                  <div class="">
                      <a href="/backoffice/register?plan={{ $plan->id }}" class="btn btn_custom btn_rounded">Empezar</a>
                  </div>
              </div>

          </div>
        @endforeach
      </div>
  </div>
</section>
@endsection
