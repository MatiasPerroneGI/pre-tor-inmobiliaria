<div class="grid-item">
    <div class="product-card">
        @if ($product->discount)
            <div class="product-badge text-danger">{{$product->discount}}% Off</div>
        @endif
        <a class="product-thumb" href="{{$product->path}}">
            <img src="/content/products/450x290/{{$product->getThumbAttribute()}}" alt="{{$product->name}}">
        </a>
        <h3 class="product-title">
            <a href="{{$product->path}}">{{ str_limit($product->name, 80) }}</a>
        </h3>
        <h4 class="product-price">
            @if ($product->final_price != $product->discount_price)
                <del>${{$product->final_price}}</del>${{$product->discount_price}}
            @else
                ${{$product->final_price}}
            @endif
        </h4>
        <div class="product-buttons">
            <button data-id="{{$product->id}}" class="btn btn-outline-primary btn-sm addToCart" data-toast data-toast-type="success" data-toast-position="bottomRight" data-toast-icon="icon-circle-check" data-toast-title="Agregado" data-toast-message="El producto fue agregado al carrito.">
                Agregar al carrito
                <span class="fa fa-spin fa-spinner" style="display: none"></span>
            </button>
        </div>
    </div>
</div>