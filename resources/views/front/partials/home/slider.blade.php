<section class="bg_custom full_height_100vh_home" id="home">
            <div class="bg_overlay_cover_on"></div>
            <div class="home_table_cell">
                <div class="home_table_cell_center">
                    <div class="container">
                        <div class="row vertical_content_manage">
                            <div class="col-lg-6">
                                <div class="mt-3">
                                    <h1 class="home_title text-white  mb-0 pt-3">Todos tus contratos de manera inteligente,simple, segura y confiable.</h1>

                                    <div class="home_text_details">
                                        <p class="home_subtitle mt-4 mb-0">Pre-tor es una herramienta para facilitar la administracion, el registro y el desenvolvimiento de la vida de un contrato.</p>
                                    </div>

                                    <div class="home_btn_manage app_btn_icon mt-4 pt-3">
                                        <a href="#" class="btn btn_custom btn_rounded mr-3">Test Gratuito</a>
                                        {{-- <a href="#" class="pl-2 text-white"><i class="mdi mdi-android"></i></a>
                                        <a href="#" class="pl-2 text-white"><i class="mdi mdi-apple"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="app_home_landing mt-3">
                                    <img src="images/iPhone_1.png" alt="" class="img-fluid mx-auto d-block">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
