<nav class="navbar navbar-expand-lg fixed-top custom_nav_menu sticky">
    <div class="container">
        <!-- LOGO -->
        <a class="navbar-brand logo" href="index.html">
            <img src="/img/logo-white.png" alt="" class="img-fluid logo-light">
            <img src="/img/logo.png" alt="" class="img-fluid logo-dark">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="mdi mdi-menu"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                    <a href="#home" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="#about" class="nav-link">About</a>
                </li>
                <li class="nav-item">
                    <a href="#features" class="nav-link">Features</a>
                </li>
                <li class="nav-item">
                    <a href="#portfolio" class="nav-link">Screenshot</a>
                </li>
                <li class="nav-item">
                    <a href="#client" class="nav-link">Client's</a>
                </li>
                <li class="nav-item">
                    <a href="#pricing" class="nav-link">Pricing</a>
                </li>
                <li class="nav-item">
                    <a href="#contact" class="nav-link">Contact</a>
                </li>
            </ul>
            <a style="color:white" href="{{ route('backoffice.login') }}" class="btn_custom btn btn_small text-capitalize  navbar-btn mr-3">Entrar</a>
            <a style="color:white" href="/registro" class="btn_custom btn btn_small text-capitalize  navbar-btn mr-3">Registrarse</a>
        </div>
    </div>
</nav>
