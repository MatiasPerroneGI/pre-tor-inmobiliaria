<section class="section_all bg_footer ">
    <div class="container ">
        <div class="row ">
            <div class="col-lg-3">
                <div class="business_footer_logo mt-3">
                    <img src="/img/logo-white.png " alt="" class="img-fluid">
                    <p class="text-muted mt-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="col-lg-3 ">
                <div class="footer_heading_tag mt-3 ">
                    <p class="text-uppercase font-weight-bold text-white ">Services</p>
                </div>
                <div class="buss_footer_menu_list mt-3 ">
                    <ul class="list-unstyled mb-0 ">
                        <li><a href="# ">About Us </a></li>
                        <li><a href="# ">Social Media</a></li>
                        <li><a href="# ">General Question</a></li>
                        <li><a href="# ">Services</a></li>
                        <li><a href="# ">Privacy</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 ">
                <div class="footer_heading_tag mt-3 ">
                    <p class="text-uppercase font-weight-bold text-white ">Resource</p>
                </div>
                <div class="buss_footer_menu_list mt-3 ">
                    <ul class="list-unstyled mb-0 ">
                        <li><a href="# ">Blog </a></li>
                        <li><a href="# ">Reviewes</a></li>
                        <li><a href="# ">Tutorials</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 ">
                <div class="contact_details_content mt-3 ">
                    <div class="contact_icon float-left pull-left ">
                        <i class="mbri-pin text_custom mr-3 "></i>
                    </div>
                    <div class="contact_detail ">
                        <p class="text-muted mb-0 ">9592 Proctor St.
                            <br>Front Royal, VA 22630.</p>
                    </div>
                </div>

                <div class="contact_details_content mt-3 ">
                    <div class="contact_icon float-left pull-left ">
                        <i class="mbri-letter text_custom mr-3 "></i>
                    </div>
                    <div class="contact_detail ">
                        <p class="text-muted mb-0 ">Support@mail.com</p>
                    </div>
                </div>

                <div class="contact_details_content mt-3 ">
                    <div class="contact_icon float-left pull-left ">
                        <i class="mbri-mobile text_custom mr-3 "></i>
                    </div>
                    <div class="contact_detail ">
                        <p class="text-muted mb-0 ">+773-804-9578,
                            <br>+610-978-2889.</p>
                    </div>
                </div>

            </div>

        </div>
        <div class="row mt-3 ">
            <div class="col-lg-12 ">
                <div class="text-center mt-3 ">
                    <p class="footer_alt_cpy mb-0 ">2018 &copy; Gentacz. Design by Saptavarana.</p>
                </div>
                <div class="text-center buss_icon_social_footer mt-3 ">
                    <ul class="list-inline mb-0 ">
                        <li class="list-inline-item "><a href=" " class=" "><i class="mdi mdi-facebook "></i></a></li>
                        <li class="list-inline-item "><a href=" " class=" "><i class="mdi mdi-twitter "></i></a></li>
                        <li class="list-inline-item "><a href=" " class=" "><i class="mdi mdi-linkedin "></i></a></li>
                        <li class="list-inline-item "><a href=" " class=" "><i class="mdi mdi-google-plus "></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</section>
