@extends('front.app')

@section('title', 'Nosotros')

@section('content')
    <!-- Page Title-->
     <div class="page-title">
       <div class="container">
         <div class="column">
           <h1>About Us</h1>
         </div>
         <div class="column">
           <ul class="breadcrumbs">
             <li><a href="/">Home</a>
             </li>
             <li class="separator">&nbsp;</li>
             <li>About Us</li>
           </ul>
         </div>
       </div>
     </div>
     <!-- Page Content-->
     <div class="container padding-bottom-2x mb-2">
       <div class="row align-items-center padding-top-2x padding-bottom-2x">
         <div class="col-md-5 order-md-2"><img class="d-block w-270 m-auto" src="/img/template/features/04.jpg" alt="Delivery"></div>
         <div class="col-md-7 order-md-1 text-md-left text-center">
           <div class="mt-30 hidden-md-up"></div>
           <h2>Rituales de belleza, en casa</h2>
           <p>MANTRA, su nombre describe el espíritu de la marca. Se asocia con la protección, el cuidado, lo divino, las vibraciones beneficiosas, la paz, la curación, el ritual y la repetición y constancia que hay que tener para lograr los mejores resultados cuando se realiza un tratamiento.

            MANTRA es una marca internacional con negocios registrados en Hong Kong, Argentina, Chile y China especializada en el diseño, búsqueda, desarrollo y fabricación de dispositivos de belleza.

            Nuestra MISION es realizar dispositivos inteligentes para resaltar la belleza y el cuidado personal.

            Nuestra VISION es innovar constantemente y ser el líder de la industria de los dispositivos de la belleza y del cuidado personal.

            Compromiso MANTRA con los ODS (Objetivos de Desarrollo Sostenible)</p>
         </div>
       </div>
     </div>
@endsection
