<!-- BEGIN Left Aside -->
<aside class="page-sidebar">
    <div class="page-logo">
        <a href="#" class="page-logo-link press-scale-down d-flex align-items-center" data-toggle="modal" data-target="#modal-shortcut">
            <span class="page-logo-text mr-1  text-center">Kamecode Admin</span>
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">
        <div class="nav-filter">
            <div class="position-relative">
                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                <a href="/admin" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                    <i class="fal fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="info-card">
            <img src="/content/admins/thumb/{{ \Auth::guard('user')->user()->thumb }}" class="profile-image rounded-circle" alt="{{ \Auth::guard('user')->user()->name }}">
            <div class="info-card-text">
                <a href="#" class="d-flex align-items-center text-white">
                    <span class="text-truncate text-truncate-sm d-inline-block">
                        {{ \Auth::guard('user')->user()->name }}
                    </span>
                </a>
                <span class="d-inline-block text-truncate text-truncate-sm">{{ config('app.name') }}</span>
            </div>
            <img src="/backend/img/card-backgrounds/cover-2-lg.png" class="cover" alt="cover">
            <a href="#" onclick="return false;" class="pull-trigger-btn" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar" data-focus="nav_filter_input">
                <i class="fal fa-angle-down"></i>
            </a>
        </div>
        <ul id="js-nav-menu" class="nav-menu">
            <li>
                <a href="/admin/plans" title="Planes" data-filter-tags="plan planes">
                    <i class="fal fa-pencil"></i>
                    <span class="nav-link-text" data-i18n="nav.application_intel">Planes</span>
                </a>
            </li>
            <li>
                <a href="/admin/users" title="Usuarios" data-filter-tags="usuario usuarios users">
                    <i class="fal fa-users"></i>
                    <span class="nav-link-text" data-i18n="nav.application_intel">Usuarios</span>
                </a>
            </li>
            @if (in_array(config('app.env'), ['test', 'local']))
                <li>
                    <a href="/test/emails-log" title="Mails" data-filter-tags="mails">
                        {{-- <i class="fal fa-envelope"></i> --}}
                        <span class="nav-link-text" data-i18n="nav.application_intel">Log de emails</span>
                    </a>
                </li>
                <li>
                    <a href="/test/users" title="Usuarios" data-filter-tags="usuarios login">
                        {{-- <i class="fal fa-sign-in-alt"></i> --}}
                        <span class="nav-link-text" data-i18n="nav.application_intel">Login as</span>
                    </a>
                </li>
            @endif
            {{--
            <li class="nav-title">Tools & Components</li>
            <li>
                <a href="#" title="UI Components" data-filter-tags="ui components">
                    <i class="fal fa-window"></i>
                    <span class="nav-link-text" data-i18n="nav.ui_components">UI Components</span>
                </a>
                <ul>
                    <li>
                        <a href="ui_alerts.html" title="Alerts" data-filter-tags="ui components alerts">
                            <span class="nav-link-text" data-i18n="nav.ui_components_alerts">Alerts</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui_accordion.html" title="Accordions" data-filter-tags="ui components accordions">
                            <span class="nav-link-text" data-i18n="nav.ui_components_accordions">Accordions</span>
                        </a>
                    </li>
                </ul>
            </li>
            --}}
        </ul>
        <div class="filter-message js-filter-message bg-success-600"></div>
    </nav>
    <!-- END PRIMARY NAVIGATION -->
</aside>
<!-- END Left Aside -->
