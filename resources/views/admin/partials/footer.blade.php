<footer class="page-footer" role="contentinfo">
    <div class="d-flex align-items-center flex-1 text-muted">
        <span class="hidden-md-down fw-700">2019 © Kamecode Admin by&nbsp;<a href='https://kame-code.com/' class='text-primary fw-500' title='Kamecode' target='_blank'>kame-code.com</a></span>
    </div>
</footer>