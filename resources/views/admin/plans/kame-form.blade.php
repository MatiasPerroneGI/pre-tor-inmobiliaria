@extends('admin.layouts.app')

@section('section', 'Planes')

@section('icon', 'pencil')

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 

        {!! KameForm::text('name')->col('md-4 sm-12') !!}
        
        {!! KameForm::text('price')->col('md-4 sm-12') !!}

        {!! KameForm::select('currency', ['USD' => 'USD', 'ARS' => 'ARS'])->col('md-4 sm-12') !!}
        
        {!! KameForm::editable('description') !!}

        {!! KameForm::select('singup_free', [0=>'No', 1=>'Si'])->col('md-4 sm-12') !!}

        {!! KameForm::text('trial_period')->col('md-4 sm-12') !!}

        {!! KameForm::select('trial_interval', ['day' => 'Días', 'month' => 'Meses'])->col('md-4 sm-12') !!}

        {!! KameForm::text('grace_period')->col('md-6 sm-12') !!}

        {!! KameForm::select('grace_interval', ['day' => 'Días', 'month' => 'Meses'])->col('md-6 sm-12') !!}
        
        {!! KameForm::submit('Enviar') !!}

    {!! KameForm::close() !!}
@endsection

@section('scripts')
    
@endsection
