@extends('admin.layouts.app')

@section('section', 'Planes')

@section('action', 'Listar')

@section('icon', 'building')

@section('widget-body')
    <!-- datatable start -->
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
        </tfoot>
    </table>
    <!-- datatable end -->
@endsection

@section('scripts')
    <script src="/backend/js/app/Plans.js"></script>
@endsection
