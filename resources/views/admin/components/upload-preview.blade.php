<div
    class="image-preview"
    id="{{$prop}}-preview"
    @if ($model->$prop)
        style="background-image: url('/content/{{$resource}}/thumb/{{$model->$prop}}');"
    @endif
>
    <label for="{{$prop}}-upload" class="{{$prop}}-label" id="{{$prop}}-label">{{$label}}</label>
    <input type="file" name="uploadPreviews[files][{{$resource}}][{{$prop}}]" class="{{$prop}}-upload" id="{{$prop}}-upload" />
    <input type="hidden" name="uploadPreviews[properties][{{$resource}}][{{$prop}}]" value="{{$prop}}">
</div>

@push('scripts')
    <script>
        $.uploadPreview({
            input_field: "#{{str_replace(':', '\\\:', $prop)}}-upload",
            preview_box: "#{{str_replace(':', '\\\:', $prop)}}-preview",
            label_field: "#{{str_replace(':', '\\\:', $prop)}}-label",
            label_default: "{{ $label }}",
            label_selected: "{{ $label }}",
            no_label: false
        });
    </script>
@endpush