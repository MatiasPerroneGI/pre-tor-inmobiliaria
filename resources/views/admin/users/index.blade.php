@extends('admin.layouts.app')

@section('section', 'Usuarios')

@section('action', 'Listar')

@section('icon', 'user')

@section('header-buttons')
    <a href="/admin/users/create" data-toggle="tooltip" data-placement="top" title="Cargar Usuario" class="btn btn-primary btn-lg btn-icon rounded-circle waves-effect waves-themed">
        <i class="fal fa-plus"></i>
    </a>
@endsection

@section('widget-body')
    <!-- datatable start -->
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>DNI</th>
                <th>Email</th>
                <th>Plan</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
        
    </table>
    <!-- datatable end -->

    
@endsection

@section('scripts')
    <script src="/backend/js/app/Users.js"></script>
@endsection
