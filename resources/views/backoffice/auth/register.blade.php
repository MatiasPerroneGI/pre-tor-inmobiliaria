@extends('front.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
              <h2 class="text-center font-weight-bold font-italic">Quiero ser parte</h2>
              <h6 class="text-center">* Datos Obligatorios</h6>

                <div class="card-body">
                    <form method="POST" action="{{ route('backoffice.register') }}">
                        @csrf
                        <input type="hidden" name="plan" value="{{ $plan->id }}" />

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group row d-flex justify-content-center d-flex justify-content-center">
                                    <label for="name" class="col-12 col-form-label text-center">Nombre*</label>

                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row d-flex justify-content-center">
                                    <label for="lastname" class="col-12 col-form-label text-center">Apellido*</label>

                                    <div class="col-md-12">
                                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required >

                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row d-flex justify-content-center">
                                    <label for="dni" class="col-12 col-form-label text-center">DNI*</label>

                                    <div class="col-md-12">
                                        <input id="dni" type="text" class="form-control{{ $errors->has('dni') ? ' is-invalid' : '' }}" name="dni" value="{{ old('dni') }}" required >

                                        @if ($errors->has('dni'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('dni') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex justify-content-center">
                                    <label for="email" class="col-12 col-form-label text-center">Email *</label>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row d-flex justify-content-center">
                                    <label for="password" class="col-12 col-form-label text-center">Contraseña *</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row d-flex justify-content-center">
                                    <label for="password-confirm" class="col-12 col-form-label text-center">Confirmar Contraseña *</label>

                                    <div class="col-md-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="tyc" id="tyc" value="yes">

                                    <label class="form-check-label" for="tyc">
                                        Acepto los <a href="#">términos y condiciones</a> del sitio.
                                    </label>
                                </div>
                                @if ($errors->has('tyc'))
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $errors->first('tyc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-5 offset-md-2">
                              <button type="submit" class="btn btn-danger btn-pink">
                                  Registrarme
                              </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
