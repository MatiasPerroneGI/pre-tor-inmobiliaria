<li {{!$event->is_read ? "class=unread" : ''}}>
    <a href="/backoffice/chat/{{$event->id}}" class="d-flex align-items-center">
        <span class="mr-2">
            <span class="profile-image rounded-circle d-inline-block" style="background-image:url('/content/admins/thumb/imagen-no-disponible.jpg')"></span>
        </span>
        <span class="d-flex flex-column flex-1 ml-1">
            <span class="msg-a fs-sm">{{ \Str::limit($event->subject, 15) }}</span>
            <span class="msg-b fs-xs">{{\Str::limit($event->message, 50) }}</span>
            <span class="fs-nano text-muted mt-1">{{$event->date->format('d/m g:i A')}}</span>
        </span>
    </a>
</li>