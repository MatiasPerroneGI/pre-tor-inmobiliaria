@extends('backoffice.layouts.app')

@push("styles")
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-solid.css">
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-brands.css">
@endpush

@section('section', 'Chat')

{{-- @section('action', 'Listar') --}}

@section('icon', 'envelope')

@section('extra-content')
    <div class="d-flex flex-grow-1 p-0">
        <!-- inbox container -->
        <!-- left slider ->
        <div id="js-inbox-menu" class="flex-wrap position-relative bg-white slide-on-mobile slide-on-mobile-left height-mobile-auto">
            <div class="position-absolute pos-top pos-bottom w-100">
                <div class="d-flex h-100 flex-column">
                    <div class="px-3 px-sm-4 px-lg-5 py-4 align-items-center">
                        <div class="btn-group btn-block" role="group" aria-label="Button group with nested dropdown ">
                            <a href="/backoffice/chat/create" class="btn btn-danger btn-block fs-md">Nuevo <i class="fa fa-plus fa-md" style="margin-left: 5px;"></i></a>
                        </div>
                    </div>
                    <div class="flex-1 pr-3">
                        <button  data-action="toggle" data-class="d-flex" data-target="#panel-compose" data-focus="select2-ajax" class="dropdown-item px-3 px-sm-4 pr-lg-3 pl-lg-5 py-2 fs-md font-weight-bold d-flex justify-content-between rounded-pill border-top-left-radius-0 border-bottom-left-radius-0 ">
                            <div>
                                <i class="fas fa-folder-open width-1"></i>Nuevo mensaje - Modal
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-backdrop" data-action="toggle" data-class="slide-on-mobile-left-show" data-target="#js-inbox-menu"></div> 
        <!-- end left slider -->

        <div class="d-flex flex-column flex-grow-1 bg-white">
            <div class="flex-grow-0  position-relative" style="z-index: 4">
                <div class="d-flex align-items-center pl-2 pr-3 py-3 pl-sm-3 pr-sm-4 py-sm-4 px-lg-5 py-lg-4  border-faded border-top-0 border-left-0 border-right-0 flex-shrink-0" >
                    <a href="javascript:void(0);" class="pl-2 pr-2 py-2 pl-sm-2 d-flex d-lg-none align-items-center justify-content-center mr-2 btn" data-action="toggle" data-class="slide-on-mobile-left-show" data-target="#js-inbox-menu">
                        <i class="fal fa-ellipsis-v h1 mb-0 "></i>
                    </a>
                    <h1 class="subheader-title ml-1 ml-lg-0">
                        <i class="fas fa-folder-open mr-2 hidden-lg-down"></i>
                        Mensajes
                        <a href="/backoffice/chat/create" data-toggle="tooltip" data-placement="top" title="Nuevo Mensaje" class="ml-2 btn btn-outline-primary btn-md text-primary btn-icon rounded-circle waves-effect waves-themed">
                            <i class="fal fa-plus"></i>
                        </a>
                    </h1>
                    <div class="d-flex position-relative ml-auto"  style="max-width: 23rem;position: relative;">
                        <i class="fas fa-search position-absolute pos-left fs-lg px-3 py-2 mt-1"></i>
                        <input type="text" class="form-control bg-subtlelight pl-6 search-conversations" placeholder="Buscar mails">
                        <i class="fal fa-times fa-sm clear-search position-absolute cursor-pointer pos-right fs-lg px-3 py-2 mt-1"></i>
                    </div>
                </div>
                
                <!-- Resultados del buscador -->
                <span class="conversations-searcher flex-column flex-grow-1 w-100 bg-white" style="display: none;
                box-shadow: rgba(0, 0, 0, 0.25) 3px 1px 6px 0px;z-index: 3; position: absolute;"> 
                    <div><h4 style="background-color: #eee" class="text pl-2 pr-3 py-3 pl-sm-3 pr-sm-4 py-sm-4 px-lg-5 py-lg-4 mb-0 border-faded border-top-0 border-left-0 border-right-0 ">Resultados</h4></div>
                    <div class="custom-scroll position-relative ">
                        <div class="d-flex h-100 flex-column">          
                            <div class="d-flex h-100 flex-column">
                                <ul id="emails-searcher" class="notification notification-layout-2 h-100" style="max-height: 228px;">
                              
                                </ul>
                            </div>
                        </div>          
                    </div>
                </span>
                <!-- END resultados del buscador -->

            </div>
            <span class="conversations-container d-flex flex-column flex-grow-1 bg-white">

                <div class="d-flex justify-content-end">
                    <div class="d-flex flex-wrap align-items-center pl-3 pr-1 py-2 px-sm-4 px-lg-5 border-faded border-top-0 border-left-0 border-right-0">
                        {{$conversations->appends(Request::except(['page']))->links('backoffice.chat.partials.paginator')}}
                    </div>
                </div>

                <div class="flex-wrap align-items-center flex-grow-1 position-relative bg-gray-50">
                    <div class="position-absolute pos-top pos-bottom w-100 custom-scroll">
                        <div class="d-flex h-100 flex-column">
                            <ul id="js-emails" class="notification notification-layout-2">
                                @each('backoffice.chat.partials.conversation-single', $conversations, 'conversation')
                            </ul>
                        </div>
                    </div>
                </div>
            </span>
        </div>
        <!-- end inbox container -->
    </div>

    @include("backoffice.chat.partials.modal-compose")

@endsection

@section('scripts')
    <script type="text/javascript">
            // the codes below are just for example use, you may need to change the scripts according to your requirement
            // select all checkbox function
            var title = document.title,

                newEmailDisplayTab = function()
                {
                    var count = $('#js-emails .unread').length
                    var newTitle = title + ' (' + count + ')';
                    document.title = newTitle;
                    $(".js-unread-emails-count").text(' (' + count + ')');
                },

                deleteEmail = function(threadID)
                {

                    // delete after animation is complete
                    threadID.animate(
                    {
                        height: 'toggle',
                        opacity: 'toggle'
                    }, '200', 'easeOutExpo', function()
                    {
                        //remove email after animation is complete
                        $(this).remove();
                        //update unread email count
                        newEmailDisplayTab();
                    });

                    //we remove any tooltips (this is a bug with bootstrap where the tooltip stays on screen after removing parent)
                    $('.tooltip').tooltip('dispose');

                    //uncheck master select all
                    if ($("#js-msg-select-all").is(":checked"))
                    {
                        $("#js-msg-select-all").prop('checked', false);
                    }

                    return this;
                }

            // select all component demo
            $("#js-msg-select-all").on("change", function(e)
            {
                if (this.checked)
                {
                    $('#js-emails :checkbox').prop("checked", $(this).is(":checked")).closest("li").addClass("state-selected");
                }
                else
                {
                    $('#js-emails :checkbox').prop("checked", $(this).is(":checked")).closest("li").removeClass("state-selected");
                }
            });

            // add or remove state-selected class to emails when they are checked
            $('#js-emails :checkbox').on("change", function()
            {

                if ($("#js-msg-select-all").is(":checked"))
                {
                    $('#js-msg-select-all').prop('indeterminate', true);
                }

                if (this.checked)
                {
                    $(this).closest("li").addClass("state-selected");
                }
                else
                {
                    $(this).closest("li").removeClass("state-selected");
                }

            });

            // email delete button triggers
            $(".js-delete-email").on('click', function()
            {
                deleteEmail($(this).closest("li"));
            })
            $("#js-delete-selected").on('click', function()
            {
                deleteEmail($("#js-emails input:checked").closest("li"))
            });

			//initApp.pushSettings("layout-composed", false);
            // show unread email count (once)
            // newEmailDisplayTab()
    </script>
@endsection
