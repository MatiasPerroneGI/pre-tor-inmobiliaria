@extends('backoffice.layouts.app')

@push("styles")
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-solid.css">
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-brands.css">
@endpush

@section('section', 'Mensaje')

@section('action', 'Nuevo')

@section('icon', 'paper-plane')

@section('extra-content')
	<div class="d-flex flex-grow-1 p-0">
	    <!-- inbox container -->
	    <div class="d-flex flex-column flex-grow-1 bg-white">
	        <!-- inbox header -->
	        <div class="flex-grow-0 mb-3">
	            <!-- inbox title -->
	            <div class="d-flex align-items-center pl-2 pr-3 py-3 pl-sm-3 pr-sm-4 py-sm-4 px-lg-5 py-lg-4  border-faded border-top-0 border-left-0 border-right-0 flex-shrink-0">
	                <!-- button for mobile -->
	                <a href="/backoffice/chat" class="pl-3 pr-3 py-2 d-flex align-items-center justify-content-center mr-2 btn rounded-circle" 
	                title="Volver">
	                    <i class="fal fa-arrow-left h1 mb-0 "></i>
	                </a>
	                <!-- end button for mobile -->
	                <h1 class="subheader-title">
	                    Nuevo mensaje
	                </h1>
	            </div>
	            <!-- end inbox title -->
	        </div>
	        <!-- end inbox header -->
	        <!-- inbox message -->
	        <div class="flex-wrap align-items-center flex-grow-1 position-relative bg-white mx-lg-3">
	            <div class=" position-absolute pos-top pos-bottom w-100">
	                <div class="d-flex h-100 flex-column">
	                   @include("backoffice.chat.partials.form")
	                </div>
	            </div>
	        </div>
	        <!-- end inbox message -->
	    </div>
	    <!-- end inbox container -->
	</form>
@endsection

@section('scripts')
	<script type="text/javascript">
	    // push settings with "false" save to local
	    initApp.pushSettings("layout-composed", false);
	</script>
@endsection
