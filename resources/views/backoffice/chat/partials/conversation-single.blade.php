<li data-id="{{$conversation->id}}" {{!$conversation->lastMessage->read && !$conversation->lastMessage->isFromUser() ? "class=unread" : ''}}>
    <div class="d-flex align-items-center px-3 px-sm-4 px-lg-5 py-1 py-lg-0 height-4 height-mobile-auto">
        <div class="ml-0 mr-3 mx-lg-3">
            <img src="/content/admins/thumb/imagen-no-disponible.jpg" class="profile-image profile-image-md rounded-circle" alt="Melissa Ayre">
        </div>
        <div class="d-flex flex-row flex-wrap flex-1 align-items-stretch align-self-stretch order-2 order-lg-3">
            <div class="row w-100">
                <a href="/backoffice/chat/{{$conversation->id}}" class="name d-flex width-sm align-items-center pt-1 pb-0 py-lg-1 col-12 col-lg-auto">{{\Str::limit($conversation->integrantesNames,20)}}</a>
                {{-- <a href="/backoffice/chat/{{$conversation->id}}" class="name d-flex width-sm align-items-center pt-1 pb-0 py-lg-1 col-12 col-lg-auto">{{$conversation->participants->first()->user->name}}</a> --}}
                <a href="/backoffice/chat/{{$conversation->id}}" class="name d-flex align-items-center pt-0 pb-1 py-lg-1 flex-1 col-12 col-lg-auto subject">
                    {{ \Str::limit($conversation->subject, 15) }} <span class="text-muted hidden-sm-down"> - {{\Str::limit($conversation->lastMessage->message,50) }}</span>
                </a>
            </div>
        </div>
        <div class="fs-sm text-muted ml-auto hide-on-hover-parent order-4 position-on-mobile-absolute pos-top pos-right mt-2 mr-3 mr-sm-4 mt-lg-0 mr-lg-0">
            @if($conversation->lastMessage->files_count)
                <i class="fas fa-paperclip fa-md mr-1"></i>
            @endif
            <a href="/backoffice/chat/answer/{{$conversation->lastMessage->id}}" data-toggle="tooltip" data-original-title="Responder" class="btn btn-icon ml-1 fa-sm">
                <i class="fal fa-reply"></i>
            </a>
            <span class="hidden-sm-down">{{$conversation->lastMessage->created_at->format('d/m g:i A')}}</span>
        </div>
    </div>
</li>