<form id="fileUploader" 
	action="/files/upload" 
	method="post" 
	box_for_ids=".adjuntos-ids" 
	box_for_template='.adjuntos' 
	text_file_template='.adjuntos-text'
	button_trigger_upload='.upload-file'
	enctype="multipart/form-data">

	<input hidden type="text" name="resource" value="{{$resource}}">
	<input hidden type="file" name="files[]" multiple class="uploadFile">
</form>

@push("scripts")	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
	<script>
		var FileUploader = (function ($, w, undefined) {
			var form;
			var forIds;
			var fileTemplate;
			var buttonTrigger;
			var fileInput;
			var forFiles;

			var defaultTplAdjuntoText = `
				<div data-id={id} class="d-flex mt-1 mb-1" style="background-color: #f5f5f5; padding: 4px 4px 4px 8px;max-width: 448px;">
		            <b>{name}</b>
		            <a class="ml-auto delete-file"><i class="fal fa-times"></i></a>
		        </div>
			`
			
			var html, files;
			function init(){
				form = $("#fileUploader");
				forIds = $(form.attr("box_for_ids"));
				fileTemplate = $(form.attr("text_file_template"));
				buttonTrigger = $(form.attr("button_trigger_upload"));
				fileInput = form.find("input.uploadFile");
				forFiles = $(form.attr("box_for_template"));
				
				attachFiles();
				ajaxFileUpload();
			}

			function attachFiles(){
		        buttonTrigger.click(function(){
		            fileInput.trigger("click");
		        })

		        fileInput.change(function(){
		            form.submit();
		        })

		        deleteFileListener();
		    }

		    function deleteFileListener(){
		    	$(document).on('click',".delete-file",function(){
		    		elem = $(this);
		    		id = $(this).closest("[data-id]").attr("data-id");
		    		$.ajax({
	                    type:'post',
	                    url: '/files/'+id+'/delete',
	                    success: function () {
	                        
	                        $("input[name='files_ids[]'][value=" + id + "]").remove();

	                        elem.parent().fadeOut(
	                            500,
	                            function () { elem.parent().remove() }
	                        );
	                    }
	                });
		    	})
		    }
	
			function ajaxFileUpload () {
	           
	            form.ajaxForm({
	            	beforeSend: function() {
	            		// Poner clase file-upload al progress bar
	                    $(".progress.file-upload").show(200);
	                },
	                uploadProgress: function(event, position, total, percentComplete) {
	                    var percentVal = percentComplete + '%';
	                    if (percentVal == '100%') percentVal = '99%';
	                    $(".progress.file-upload").children().width(percentVal);
	                },

	                success: function (data) {
	                    files = data.files;
	                   	
	                   	$.each(files,function(i,file){
	                   		let input = `<input name="files_ids[]" hidden type="text" value="`+file.id+`">`;

	                   		if (forIds.length) {
	                   			forIds.append($(input));
	                   		}else{
	                   			buttonTrigger.closest("form").append($(input));
	                   		}

							let template = fileTemplate.text() || defaultTplAdjuntoText;
							template = template.replace('{id}',file.id);
							template = template.replace('{name}',file.name);
							template = template.replace('{src}',file.src);

							forFiles.append($(template))
	                   	})

	                   	$(".progress.file-upload").fadeOut(200).children().width("0%");
	                },

	                error: function (a, b, c) {
	                	$(".progress.file-upload").fadeOut(200).children().width("0%");
	                    toastr["error"](a.responseJSON.errors[Object.keys(a.responseJSON.errors)[0]][0], "Error")
	                }
	            });
	        }	    


	    	return {
	            init: function (files) {
	                init(files);
	            }
	        }
	
		})(jQuery, window);

	    FileUploader.init();

	</script>
@endpush