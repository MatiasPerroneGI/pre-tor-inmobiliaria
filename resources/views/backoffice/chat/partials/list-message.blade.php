<!-- message -->
<div id="msg-{{$message->id}}" class="single-message d-flex flex-column border-faded border-top-0 border-left-0 border-right-0 py-3 px-3 px-sm-4 px-lg-0 mr-0 mr-lg-5 flex-shrink-0">
    <div class="d-flex align-items-center flex-row message">
        <div class="ml-0 mr-3 mx-lg-3 width-4">
            <img src="/content/{{$message->user->thumb}}" class="border-faded profile-image profile-image-lg rounded-circle" alt="">
        </div>
        <div class="fw-500 flex-1 d-flex flex-column cursor-pointer" aria-expanded="false" data-toggle="collapse" data-target="#msg-{{$message->id}} > .js-collapse">
            <div class="fs-lg">
                {{$message->user->name}} <span class="fs-nano text-muted">   ({{$message->user->email}})</span>
            </div>
            <div class="fs-nano">
                Para {{$message->user_id != \Auth::id() ? ' mí' : $message->user->email}}
            </div>
        </div>
        <div class="color-fusion-200 fs-sm">
            {{$message->created_at->format('d/m g:i A')}}<span class="hidden-sm-down"> ({{$message->created_at->diffForHumans()}})</span>
        </div>
        <div class="collapsed-reveal">
            <a href="/backoffice/chat/answer/{{$message->id}}" data-toggle="tooltip" data-original-title="Responder" class="btn btn-icon ml-1 fs-lg">
                <i class="fal fa-reply"></i>
            </a>
            
        </div>
        @if($message->files->count())
            <i class="fas fa-paperclip fa-md ml-1 mr-1"></i>
        @endif
        
    </div>
    <div id="js-msg-{{$message->id}}" class="js-collapse collapse">
        <div class="pl-lg-5 pt-3 pb-4 ">
            <h3><strong>{{$message->subject}}</strong></h3>
            {{$message->message}}

            @if($message->files->count())
                <div class="d-flex mt-3 ">
                    <div class="d-flex flex-column files-container flex-1">
                        @foreach($message->files as $file)
                            <div class="mb-2" style="background-color: #f5f5f5;padding: 4px 4px 4px 8px;max-width:50%;">
                                <a href="/files/download/{{$file->id}}" class="d-flex align-items-center file-download">
                                    <span class="mt-1"><strong>{{$file->filename}}</strong></span>
                                     <i class="ml-2 mr-1 fal fa-download fa-md ml-auto position-relative"></i>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    @if($message->files->count() > 1)
                        <div class="fs-lg mb-3 d-flex">
                            <a href="/files/downloadAll/{{$file->id}}" class="download-all-files"><span class="badge badge-primary ml-2 hidden-sm-down">Descargar todos</span></a>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>
<!-- end message -->
