<div class="text-muted mr-1 mr-lg-3 ml-auto">
    <span class="paginator-info">{{$paginator->firstItem()}} - {{$paginator->lastItem()}}</span> <span class="hidden-lg-down"> de {{$paginator->total()}} </span>
</div>
<div class="d-flex flex-wrap pagination">
    <a href="{{ $paginator->url($paginator->currentPage()-1) }}" class="btn btn-icon rounded-circle {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}"><i class="fal fa-chevron-left fs-md "></i></a>
    <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="btn btn-icon rounded-circle {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}"><i class="fal fa-chevron-right fs-md"></i></a>
</div>