{{--
<div class="d-flex justify-content-end">
    <div class="d-flex flex-wrap align-items-center pl-3 pr-1 py-2 px-sm-4 px-lg-5 border-faded border-top-0 border-left-0 border-right-0">
        {{$conversations->appends(Request::except(['page']))->links('backoffice.chat.partials.paginator')}}
    </div>
</div>

<div class="flex-wrap align-items-center flex-grow-1 position-relative bg-gray-50">
    <div class="position-absolute pos-top pos-bottom w-100 custom-scroll">
        <div class="d-flex h-100 flex-column">
            <ul id="js-emails" class="notification notification-layout-2">
                @include("backoffice.chat.partials.list-conversation", ['view' => 'backoffice.chat.partials.conversation-single'])
			</ul>
        </div>
    </div>
</div>
--}}

@include("backoffice.chat.partials.list-conversation", ['view' => 'backoffice.chat.partials.conversation-single'])