
@push("styles")
<!--     <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/dropzone/dropzone.css"> -->
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush

<form action="/backoffice/chat" method="{{$method ?? 'post'}}" class="flex-1 d-flex flex-column message-form">
    @csrf    
    <input type="text" hidden name="user_id">
    {{-- <input type="text" hidden name="recipient_id" value="{{$recipient->id ?? ''}}"> --}}
    <input type="text" hidden name="conversation_id" value="{{$conversation->id ?? ''}}">

    <div class="px-3">
        <select class="js-data-example-ajax form-control " id="select2-ajax" name="recipient" {{isset($recipients) ? 'readonly="true"' :''}} {{isset($recipients) && count($recipients)>1?'multiple':''}}>
            @isset($recipients)
                @foreach($recipients as $rec)
                    <option value="{{$rec->id}}" selected="selected">{{$rec->name}}</option>
                @endforeach
            @endisset
        </select>
        
        <input type="text" placeholder="Asunto" class="form-control border-top-0 border-left-0 border-right-0 px-0 rounded-0 fs-md mt-2" name="subject" tabindex="4" {{ isset($conversation) ? 'disabled readonly' : '' }}  value="{{$conversation->subject ?? ''}}">
    </div>
    <div class="flex-1" style="overflow-y: auto;">
        <div id='fake_textarea' class="form-control rounded-0 w-100 h-100 border-0 py-3" contenteditable tabindex="5">
            <br>
            <br>
        </div>
    </div>

    <div class="px-3 d-flex flex-column adjuntos"></div>
    <div class="d-none adjuntos-ids"></div>

    <div class="px-3 py-4 d-flex flex-row align-items-center flex-wrap flex-shrink-0">  
        <div>
        <button type="submit" class="btn btn-success mr-3">Enviar <i class="fas fa-paper-plane"></i></a>
        </div>
        <a href="javascript:void(0);" class="btn btn-icon fs-xl mr-1 upload-file" data-toggle="tooltip" data-original-title="Adjuntar archivos" data-placement="top">
            <i class="fas fa-paperclip color-fusion-300"></i>
        </a>
        <div class="progress w-50 file-upload ml-2" style="display: none">
            <div class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>

    <textarea hidden name="message" style="display: none;"></textarea>
    <textarea hidden class="adjuntos-text">
        <div data-id={id} class="d-flex mt-1 mb-1" style="background-color: #f5f5f5; padding: 4px 4px 4px 8px;max-width: 448px;">
            <b>{name}</b>
            <a class="ml-auto delete-file"><i class=" fa fa-times"></i></a>
        </div>
    </textarea>
</form>

@include('backoffice.chat.partials.file-uploader',['resource'=>'Message'])

@push("scripts")
    <script src="/backend/js/formplugins/select2/select2.bundle.js"></script>
    <!-- <script src="/backend/js/formplugins/dropzone/dropzone.js"></script> -->
    <script src="/backend/js/backoffice/Chat.js"></script>
@endpush

