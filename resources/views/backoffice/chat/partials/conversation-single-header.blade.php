<li {{!$conversation->lastMessage->read && !$conversation->lastMessage->isFromUser() ? "class=unread" : ''}}>
    <a href="/backoffice/chat/{{$conversation->id}}" class="d-flex align-items-center">
        <span class="mr-2">
            <span class="profile-image rounded-circle d-inline-block" style="background-image:url('/content/admins/thumb/imagen-no-disponible.jpg')"></span>
        </span>
        <span class="d-flex flex-column flex-1 ml-1">
            <span class="name">{{$conversation->participants->first()->user->name}}</span>
            <span class="msg-a fs-sm">{{ \Str::limit($conversation->subject, 15) }}</span>
            <span class="msg-b fs-xs">{{\Str::limit($conversation->lastMessage->message,50) }}</span>
            <span class="fs-nano text-muted mt-1">{{$conversation->lastMessage->created_at->format('d/m g:i A')}}</span>
        </span>
    </a>
</li>