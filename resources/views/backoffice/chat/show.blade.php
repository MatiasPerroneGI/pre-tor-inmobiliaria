@extends('backoffice.layouts.app')

@push("styles")
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-solid.css">
	<link rel="stylesheet" media="screen, print" href="/backend/css/fa-brands.css">
@endpush

@section('section', 'Mensaje')

@section('action', 'Mostrar')

@section('icon', '')

@section('extra-content')
	<div class="d-flex flex-grow-1 p-0">
        
        <div class="d-flex flex-column flex-grow-1 bg-white">
            <!-- inbox header -->
            <div class="flex-grow-0">
                <!-- inbox button shortcut -->
                <div class="d-flex flex-wrap align-items-center pl-2 pr-3 py-2 px-sm-4 pr-lg-5 pl-lg-0 border-faded border-top-0 border-left-0 border-right-0">
                    <div class="flex-1 d-flex align-items-center">
                        <a href="/backoffice/chat" class="btn mr-2 mr-lg-3">
                            <i class="fas fa-arrow-left fs-lg"></i><span class="ml-3">Volver atrás</span>
                        </a>
                    </div>
                </div>
                <!-- end inbox button shortcut -->
            </div>
            <!-- end inbox header -->
            <!-- inbox message -->
            <div class="flex-wrap align-items-center flex-grow-1 position-relative bg-white">
                <div class=" position-absolute pos-top pos-bottom w-100 custom-scroll">
                    <div class="d-flex h-100 flex-column">
                        <!-- inbox title -->
                        <div class="d-flex align-items-center pl-2 pr-3 py-3 pl-sm-3 pr-sm-4 py-sm-4 px-lg-5 py-lg-3 flex-shrink-0">
                            <h1 class="subheader-title mb-0 ml-2 ml-lg-5">
                                {{$conversation->subject}}
                            </h1>
                        </div>
                        <!-- end inbox title -->
                        @each("backoffice.chat.partials.list-message",$conversation->messages,'message')
                    </div>
                </div>
            </div>
            <!-- end inbox message -->
        </div>
        <!-- end inbox container -->
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        // push settings with "false" save to local
        initApp.pushSettings("layout-composed", false);

        // Despliego el último mensaje
        $(".single-message").last().find("[data-toggle=collapse]").trigger("click");
    </script>
@endsection
