@extends('backoffice.layouts.app')

@section('section', 'Agenda')

@section('action', 'Listar')

@section('icon', 'calendar')

@section('header-buttons')
    <a href="/backoffice/schedules/create" data-toggle="tooltip" data-placement="top" title="Cargar programación" class="btn btn-primary btn-lg btn-icon rounded-circle waves-effect waves-themed">
        <i class="fal fa-plus"></i>
    </a>
@endsection

@section('widget-body')
    <div class="row">
        <div class="col-md-12">
            <table id="datatable" class="table " width="100%">
                <thead>
                    <tr>
                        <th>Asunto</th>
                        <th>Mensaje</th>
                        <th>Frecuencia</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody><!--Datatable--></tbody>

            </table>
        </div>
    </div>
    
@endsection

@section('scripts')
    <script>
        KT.create('#datatable', {
            resource: 'schedules',
            ajax: '/backoffice/schedules/json',
            columns: [
                'subject',
                'message|limit',
                'frequency',
                'id|actions'
            ]
        });
    </script>
@endsection
