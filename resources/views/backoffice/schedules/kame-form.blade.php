@extends('backoffice.layouts.app')

@section('section', 'Agenda')

@section('icon', 'calendar')

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 
		<legend class="col-md-12">Contenido</legend>	
		{!! KameForm::text('subject')->col('md-12') !!}
		{!! KameForm::textarea('message')->col('md-12') !!}

        <legend class="col-md-12 mt-2">Programación</legend>
		
		<div class="col-md-12 mb-3">
			<div class="custom-control custom-switch">
	            <input name="toggle_type" type="checkbox" class="custom-control-input" id="customSwitch1" {{$model->id ? ($model->date ? "checked='checked'" : '') : "checked='checked'"}}>
	            <label data-swchon-text="Día fijo" data-swchoff-text="Programar" class="custom-control-label" for="customSwitch1"></label>
	        </div>
	    </div>
	
		<div class="freq-container w-100 col-md-12">
			<div class="row">
				{!! KameForm::select('frequency', $model->getFrequenciesSelect())->col('md-6 sm-6 xs-6') !!}
				{!! KameForm::select('param', $model->getPeriodSelect())->col('md-6 sm-6 xs-6') !!}
			</div>
		</div>
		<div class="fixed-container w-100"> 
			{!! KameForm::datepicker('date',['default'=>$model->date ?? \Carbon\Carbon::today()])->col('md-6') !!}
        </div>
    
    	{!! KameForm::submit('')!!}
    {!! KameForm::close() !!}
@endsection

@section('scripts')
    <script>var frequenciesOptions = {!! \App\Schedule::getFrequenciesOptions() !!}</script>
    <script src="/backend/js/backoffice/Schedule.js"></script>
@endsection
