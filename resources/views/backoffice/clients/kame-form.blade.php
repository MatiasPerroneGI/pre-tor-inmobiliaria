@extends('backoffice.layouts.app')

@section('section', 'Cliente')

@section('icon', 'user')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 

        {!! KameForm::text('email')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('name')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('lastname')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('dni')->col('md-3 sm-12') !!}
        
        <div class="col-md-12" style="margin-bottom: 20px;">
            <div class="custom-control custom-checkbox">
                {!! \Form::checkbox('sendMail',null,null,['class'=>'custom-control-input','id'=>'sendMail','disabled'=>!$model->email]) !!}
                <label class="custom-control-label" for="sendMail">Enviar correo</label>
            </div>
        </div>

        <input type="text" name="client_id" hidden="true">

        {!! KameForm::submit('Enviar') !!}
    {!! KameForm::close() !!}
@endsection


@section("scripts")
    <script src="/backend/js/backoffice/Client.js"></script>
@endsection
