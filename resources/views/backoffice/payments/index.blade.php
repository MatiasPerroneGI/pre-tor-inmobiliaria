@extends('backoffice.layouts.app')

@section('section', 'Pagos')

@section('action', 'Listar')

@section('icon', 'credit-card')

@section('widget-body')
    <div class="row">
        <div class="col-md-12">
            <!-- datatable start -->
            <table id="datatable" class="table  w-100">
                <thead>
                    <tr>
                        <th>Servicio</th>
                        <th>Contrato</th>
                        <th>Vencimiento</th>
                        <th>Estado</th>
                        <th>Pagar</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            <!-- datatable end -->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/backend/js/backoffice/Payments.js"></script>
@endsection
