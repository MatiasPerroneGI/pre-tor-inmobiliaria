@extends('backoffice.layouts.app')

@section('section', 'Pagos')

@section('icon', 'credit-card')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush


@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 
        
        <div class="col-md-6">
            <div class="form-container active">
                {!! KameForm::text('number', ['placeholder' => 'Número'])->label('Número') !!}

                {!! KameForm::text('name', ['placeholder' => 'Nombre']) !!}

                {!! KameForm::text('expiry', ['placeholder' => 'MM/AA'])->label('Vencimiento') !!}

                {!! KameForm::text('cvc', ['placeholder' => 'CVC'])->label('CVC') !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="card-wrapper"></div>
        </div>

        <input type="hidden" name="payment_date" value="{{ date('Y-m-d') }}">
  
        {!! KameForm::submit('Pagar') !!}

    {!! KameForm::close() !!}
@endsection

@section('scripts')
    <script src="/backend/js/card.min.js"></script>
    <script>
        new Card({
            form: document.querySelector('form'),
            container: '.card-wrapper',
            formSelectors: {
                numberInput: 'input#number', // optional — default input[name="number"]
                expiryInput: 'input#expiry', // optional — default input[name="expiry"]
                cvcInput: 'input#cvc', // optional — default input[name="cvc"]
                nameInput: 'input#name' // optional - defaults input[name="name"]
            },
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Nombre Apellido',
                expiry: '••/••',
                cvc: '•••'
            },
        });
    </script>
@endsection
