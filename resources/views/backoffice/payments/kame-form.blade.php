@extends('backoffice.layouts.app')
@section('section', 'Pagos')

@section('icon', 'credit-card')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush


@section('widget-body')

    <div class="col-md-12">
        <h3>Subí tu comprobante de pago</h3>
        <p>Si ya pagaste el servicio subí el comprobante</p>
        {!! KameForm::model($model, $formOptions) !!} 

            {!! KameForm::file('payment_proof')->label('Comprobante') !!}

            {!! KameForm::submit('Cargar comprobante') !!}
            
        {!! KameForm::close() !!}
    </div>

    <div class="col-md-12">
        <hr>
        <h3>O pagá a través de Pre-tor</h3>

         @if ($model->service->id != \App\Service::CANON)
            {!! KameForm::model($model, $formOptions) !!} 
            
                <div class="col-md-6">
                    <div class="form-container active">
                        {!! KameForm::text('number', ['placeholder' => 'Número'])->label('Número') !!}

                        {!! KameForm::text('name', ['placeholder' => 'Nombre']) !!}

                        {!! KameForm::text('expiry', ['placeholder' => 'MM/AA'])->label('Vencimiento') !!}

                        {!! KameForm::text('cvc', ['placeholder' => 'CVC'])->label('CVC') !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card-wrapper"></div>
                </div>
                
                {!! KameForm::submit('Pagar') !!}

            {!! KameForm::close() !!}
        @endif
    </div>

       

    
@endsection

@section('scripts')
    <script src="/backend/js/card.min.js"></script>
    <script>
        new Card({
            form: $('form:last')[0],
            container: '.card-wrapper',
            formSelectors: {
                numberInput: 'input#number', // optional — default input[name="number"]
                expiryInput: 'input#expiry', // optional — default input[name="expiry"]
                cvcInput: 'input#cvc', // optional — default input[name="cvc"]
                nameInput: 'input#name' // optional - defaults input[name="name"]
            },
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Nombre Apellido',
                expiry: '••/••',
                cvc: '•••'
            },
        });
    </script>
@endsection
