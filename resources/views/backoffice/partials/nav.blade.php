@inject('menues', 'App\Services\MenuService')

<!-- BEGIN Left Aside -->
<aside class="page-sidebar">
    <div class="page-logo">
        <a href="/backoffice" class="page-logo-link press-scale-down d-flex align-items-center" data-toggle="modal" data-target="#modal-shortcut">
            <span class="page-logo-text mr-1  text-center text-primary">Pre-tor</span>
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">
        <div class="nav-filter">
            <div class="position-relative">
                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                <a href="/admin" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                    <i class="fal fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <ul id="js-nav-menu" class="nav-menu">
            <li class="mb-3">
                <a href="#" data-action="toggle" data-class="nav-function-hidden" title="Hide Navigation">
                    <i class="fal fa-times"></i>
                    <span class="nav-link-text pretor-menu-text" data-i18n="nav.application_intel"></span>
                </a>
            </li>
            @each('backoffice.partials.nav-item', $menues->get('user'), 'menu')
            <li><br/></li>
            @each('backoffice.partials.nav-item', $menues->get('admin'), 'menu')
        </ul>
        <div class="filter-message js-filter-message bg-success-600"></div>
    </nav>
    <!-- END PRIMARY NAVIGATION -->
</aside>
<!-- END Left Aside -->
