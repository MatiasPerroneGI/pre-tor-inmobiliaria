@if ($menu['produccion'] Or !in_array(config('app.env'), ['production', 'produccion']))
  @if ($menu['link'] == '-')
  <li><br></li>
  @else
  <li class="{{ request()->is("{$menu['prefijo']}/{$menu['link']}") ? 'active' : '' }}" >
      <a href="{{ "/{$menu['prefijo']}/{$menu['link']}" }}" title="{{ $menu['titulo'] }}" data-filter-tags="{{ $menu['tags'] }}">
          {{-- <i class="fal fa-home"></i> --}}
          <span class="nav-link-text" data-i18n="nav.application_intel">{{ $menu['titulo'] }}</span>
      </a>
  </li>
  @endif
@endif
