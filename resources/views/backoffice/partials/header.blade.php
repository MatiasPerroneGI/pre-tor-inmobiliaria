@inject('user', 'App\Services\UserService')

<header class="page-header" role="banner">
    <!-- we need this logo when user switches to nav-function-top -->
    <div class="page-logo">
        <a href="#" class="page-logo-link press-scale-down d-flex align-items-center" data-toggle="modal" data-target="#modal-shortcut">
            <img src="/backend/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
            <span class="page-logo-text mr-1">Kamecode Admin</span>
            <i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
        </a>
    </div>
    <!-- DOC: mobile button appears during mobile width -->
    <div class="hidden-lg-up">
        <a href="#" class="header-btn btn press-scale-down" data-action="toggle" data-class="mobile-nav-on">
            <i class="ni ni-menu"></i>
        </a>
    </div>

    <div class="d-flex position-relative flex-1 h-100 align-items-center justify-content-between">
        <ol class="breadcrumb page-breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
            @if (isset($model))
                <li class="breadcrumb-item"><a href="/admin/{{ $model->getTable() }}">@yield('section')</a></li>
            @else
                <li class="breadcrumb-item">@yield('section')</li>
            @endif

            @if (isset($viewConfig['accion']))
                <li class="breadcrumb-item active">{{ $viewConfig['accion'] }}</li>
            @else
                @hasSection("action")
                    <li class="breadcrumb-item active">@yield('action')</li>
                @endif
            @endif
        </ol>

        <li class="d-none d-sm-block">
            <span>{{\Carbon\Carbon::today()->isoFormat("LL")}}</span>
            <span class="ml-3"><img style="width: 36px;" src="{{ $user->thumb() }}" class=" rounded-circle rounded-sm" alt="{{ $user->name() }}"></span>
            <span>
                <strong>{{ $user->name() }}</strong>
            </span>
        </li>
    </div>

    <div class="ml-auto d-flex">

        <!-- app notification -->
        <div id="header-notifications">
            <a href="#" data-open class="header-icon cursor-pointer" data-toggle="dropdown" title="Tienes 11 notificaciones">
                <i class="fal fa-bell"></i>
                <span class="badge badge-icon badge-primary">11</span>
            </a>
            <div class="dropdown-menu dropdown-menu-animated dropdown-xl">
                <div class="dropdown-header bg-trans-gradient d-flex justify-content-center align-items-center rounded-top mb-2">
                    <h4 class="m-0 text-center color-white">
                        11 nuevos
                        <small class="mb-0 opacity-80">Avisos</small>
                    </h4>
                </div>
                <ul class="nav nav-tabs nav-tabs-clean" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link px-4 fs-md js-waves-on fw-500" data-toggle="tab" href="#tab-conversations" data-i18n="drpdwn.messages">Mensajes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-4 fs-md js-waves-on fw-500" data-toggle="tab" href="#tab-feeds" data-i18n="drpdwn.feeds">Avisos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-4 fs-md js-waves-on fw-500" data-toggle="tab" href="#tab-events" data-i18n="drpdwn.events">Intimaciones</a>
                    </li>
                </ul>
                <div class="tab-content tab-notification">
                    <div class="tab-pane p-3 text-center" data-loader="false">
                        <h5 class="mt-4 pt-4 fw-500">
                            <div class="spinner-grow text-primary" role="status">
                                <span class="sr-only">Cargando...</span>
                            </div>
                            <small class="mt-3 fs-b fw-400 text-muted">
                                Cargando...
                            </small>
                        </h5>
                    </div>
                    <div class="tab-pane" id="tab-conversations" role="tabpanel">
                        <div class="custom-scroll h-100">
                            <ul class="notification" data-conversations>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-feeds" role="tabpanel">
                        <div class="custom-scroll h-100">
                            <ul class="notification" data-events>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-events" role="tabpanel">
                        <div class="custom-scroll h-100">
                            <ul class="notification" data-intimations>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="py-2 px-3 bg-faded d-block rounded-bottom text-right border-faded border-bottom-0 border-right-0 border-left-0">
                    <a href="#" class="fs-xs fw-500 ml-auto">Ver todas las notificaciones</a>
                </div>
            </div>
        </div>
        <div>
        <a href="{{ $user->logoutRoute() }}" class="header-icon cursor-pointer text-primary"
            title="Cerrar sesión de {{  $user->name(false) }}">
                <i class="fal fa-sign-out"></i>
            </a>
        </div>

    </div>



</header>
