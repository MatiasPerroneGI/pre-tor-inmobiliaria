<main id="js-page-content" role="main" class="page-content container-fluid">
{{-- 
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-@yield('icon')'></i> @yield('section')
        </h1>
        <div class="subheader-block">
            @yield('header-buttons')
        </div>
    </div>
 --}}    
    @include('admin.partials.errors')
    
    @hasSection("widget-body")
        <div class="row">
            <div class="col-xl-10 offset-xl-1 col-sm-12">
                @hasSection("section")
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title">@yield('section')</h1>
                        </div>
                    </div>
                @endif
                @yield('widget-body')
                {{-- <div id="panel-1" class="panel main"> --}}
                    {{-- <div class="panel-hdr">
                        <h2> @yield('section') </h2>
                        <div class="panel-toolbar">
                            <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
                            <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
                            <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
                        </div>
                    </div> --}}
                    {{-- <div class="panel-container show"> --}}
                        {{-- <div class="panel-content"> --}}
                            {{-- @yield('widget-body') --}}
                        {{-- </div> --}}
                    {{-- </div> --}}
                {{-- </div> --}}
            </div>
        </div>
    @endif

    @yield('extra-content')
</main>