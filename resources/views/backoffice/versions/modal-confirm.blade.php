<!-- Modal -->
<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="/backoffice/contracts/{{ $version->contract->id }}/confirm">
            @csrf
            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modalConfirmLabel">
                        <span style="float: none" class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        <span class="text">Aprobar contrato</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 content">
                            <p>¿Está seguro que desea aprobar este contrato?</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Confirmar</button>
                </div>
            </div>
        </form>
    </div>
</div>
