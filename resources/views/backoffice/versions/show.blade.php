@extends('backoffice.layouts.app')

@section('section', $version->contract->name)

@section('action', 'Listar')

@section('icon', 'file')

@push('styles')
    <style>
        #watermark {
            display: none
        }
    </style>
@endpush

@section('header-buttons')
    @if (\Auth::id() != $version->user_id && $version->isLastOne())
        <a href="confirmar" class="btn btn-success pull-right" style="position: fixed; top: 100px; right: 35px; z-index: 2;">
            <span class="fa fa-check"></span> Confirmar contrato
        </a>
    @endif
@endsection

@section('widget-body')
    @include('backoffice.contracts._pdf', ['version' => $version])

    @if (\Auth::id() != $version->user_id && $version->isLastOne())
        <a class="btn btn-success" href="/backoffice/versions/{{ $version->id }}/edit">Editar</a>
    @endif

    @include('backoffice.versions.modal-confirm')
@endsection

@section('scripts')
    <script type="text/javascript">
    $('a[href="confirmar"]').on('click', function (e) {
        e.preventDefault();
        $('#modalConfirm').modal('show');
    });
</script>
@endsection