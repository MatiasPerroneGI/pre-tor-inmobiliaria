@extends('backoffice.layouts.app')

@section('section', 'Versiones')

@section('action', 'Listar')

@section('icon', 'file')

@section('widget-body')
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Creada</th>
                <th>Modificada</th>
                <th>Aprobada</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <input type="hidden" name="contract_id" value="{{ request()->input('contract_id') }}">
@endsection

@section('scripts')
    <script src="/backend/js/backoffice/Versions.js"></script>
@endsection