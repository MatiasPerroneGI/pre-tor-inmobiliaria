@extends('backoffice.layouts.app')

@section('head')
    <style type="text/css">
        .edited .form-control:disabled, .edited .form-control[readonly] {
            background-color: #fffde1 !important
        }
    </style>
@endsection

@section('section', 'Versionado')

@section('icon', 'calendar')

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!}
		<legend class="col-md-12">Contrato</legend>

        @foreach ($model->clauses()->with('version.contract')->get() as $clause)
            {{-- if $clause->type == ''editable', 'appendable'' --}}

            <div class="mb-3 col-md-12">
                <h3>{{$clause->title}}</h3>
            </div>

            <div class="mb-3 col-md-12 {{ $clause->edited ? 'edited' : '' }}">
                {!! Form::textarea('contents[]', $clause->render() ,['class' => 'form-control', 'data-autosize' => 'true', 'rows' => '','value' => 'asda', 'readonly']) !!}
            </div>

            @if ($clause->type == 'editable')
                <button type="button" style="margin:auto;"  class="enable-edit btn btn-primary w-75 mb-3" name="button">Editar</button>
            @endif

        @endforeach

    	{!! KameForm::submit('Guardar')!!}
    {!! KameForm::close() !!}
@endsection

@section('scripts')
    <script type="text/javascript">
        $('.enable-edit').on('click',function(){
            prevTextArea = $(this).prev().children()
            if ( prevTextArea.prop('readonly') ) {
                prevTextArea.removeAttr("readonly")
                $(this).html('Guardar');
            }
            else{
                prevTextArea.prop("readonly", true)
                $(this).html('Editar');
            }
        })
    </script>
@endsection
