<!DOCTYPE html>
<html lang="{{ config('app.lang') }}">
<head>
    @include('admin.partials.head')
    @yield('head')
    @stack('styles')
</head>
<body class="mod-bg-1 ">
    <script src="/backend/js/settings.js"></script>
    <div class="page-wrapper">
        <div class="page-inner">
            @include('backoffice.partials.nav')
            <div class="page-content-wrapper">
                @include('admin.partials.header')

               <main id="js-page-content" role="main" class="page-content">
                    @yield('breadcrumbs')

                    @section('subheader')
                        <div class="subheader">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-@yield('icon')'></i> @yield('section')
                            </h1>
                            <div class="subheader-block">
                                @yield('header-buttons')
                            </div>
                        </div>
                    @show
                    
                    @include('admin.partials.errors')
                    
                    @yield('content')
                </main>

                <div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div>

                @include('admin.partials.footer')
            </div>
        </div>
    </div>

    @stack('components')

    @include('admin.partials.scripts')
    
    @yield('scripts')
    
    @stack('scripts')
</body>
</html>
