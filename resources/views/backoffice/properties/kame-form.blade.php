@extends('backoffice.layouts.app')

@section('section', 'Propiedades')

@section('icon', 'building')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush


@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 
        {!! KameForm::text('address')->col('md-8 sm-12') !!}
        
        {!! KameForm::text('area')->col('md-4 sm-12') !!}
        
        {!! KameForm::select('owners[]',$model->owners->pluck("name","id"), ['class'=>'js-data-example-ajax','multiple'=>'multiple'])->col('md-12 sm-12') !!}
        
        {!! KameForm::editable('description') !!}
        
        {!! KameForm::textarea('accessories') !!}

        @include('admin.components.images-uploader', ['resource' => 'properties'])
  
        {!! KameForm::submit('Enviar') !!}

    {!! KameForm::close() !!}
@endsection

@section('scripts')
    {{-- <script src="/backend/js/app/Product.js"></script> --}}
    <script src="/backend/js/formplugins/select2/select2.bundle.js"></script>
    <script>
        $("select[name='owners[]'").select2({
            ajax:{
                url: "/backoffice/autocomplete/clientes",
                // dataType: 'json',
                delay: 250,
                data: function(params){
                    return {
                        term: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params){
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination:{
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "Propietarios",
            escapeMarkup: function(markup){
                return markup;
            }, 
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        }); 
        $("select[name='owners[]'").trigger("change");
    
     //    checkRecipient();

	    // function checkRecipient(){
	    //     if($("input[name=recipient_id]").val()){
	    //         destinatario.select2().select2("val", null);
	    //         destinatario.select2().select2("val", $("input[name=recipient_id]").val());
	    //         $("input[name=recipient]").val($("input[name=recipient_id]").val());
	    //     }
	    // }

	    function formatRepo(repo){
	        if (repo.loading) return repo.text;

	        var markup = "<div class='select2-result-repository clearfix d-flex'>" +
	            "<div class='select2-result-repository__meta'>"+
	            "<div class='select2-result-repository__title fs-lg fw-500'>" + repo.name + ' ' + repo.lastname + "<br></div>"+
	            "<div class='select2-result-repository__forks mr-2'><small>" + repo.email+ "</small></div>"+
	            "</div></div>";

	        return markup;
	    }

	    function formatRepoSelection(repo){
	        return repo.name || repo.text;
	    }
    </script>
@endsection
