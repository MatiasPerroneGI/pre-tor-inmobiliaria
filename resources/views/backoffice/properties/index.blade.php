@extends('backoffice.layouts.app')

@section('section', 'Propiedades')

@section('action', 'Listar')

@section('icon', 'building')

@section('header-buttons')
    <a href="/backoffice/properties/create" class="btn btn-outline-primary waves-effect waves-themed">
        Nueva propiedad
    </a>
@endsection

@section('widget-body')
    <div class="row">
        <div class="col-md-12">
            <!-- datatable start -->
            <table id="datatable" class="table  w-100">
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Dirección</th>
                        <th>Superficie</th>
                        <th>Dueño/s</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            <!-- datatable end -->

            <input type="hidden" name="user_id" value="{{ request()->input('user_id') }}">
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/backend/js/backoffice/Properties.js"></script>
@endsection
