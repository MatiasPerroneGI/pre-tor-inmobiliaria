@extends('backoffice.layouts.app')

@section('section', 'Intimaciones')

@section('action', 'Listar')

@section('icon', 'exclamation-circle')

@section('header-buttons')
    <a href="/backoffice/intimations/create" class="btn btn-outline-primary waves-effect waves-themed">
        Nueva intimación
    </a>
@endsection

@section('widget-body')
    <div class="row">
        <div class="col-md-12">
            <!-- datatable start -->
            <table id="datatable" class="table  w-100">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Contrato</th>
                        <th>Asunto</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            <!-- datatable end -->

        </div>
    </div>
@endsection

@section('scripts')
    <script>var userId={!!\Auth::id()!!}; </script>
    <script src="/backend/js/backoffice/Intimations.js"></script>
@endsection
