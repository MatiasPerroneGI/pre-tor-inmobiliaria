@extends('backoffice.layouts.app')

@section('section', 'intimación')

@section('action', 'Listar')

@section('icon', 'exclamation-circle')

@section('widget-body')
    <div class="row form">
    	<div class="col-md-12 mt-3 mb-3">
    		@if($intimation->is_resolved)
    			<span class="badge badge-warning text-uppercase p-2">Intimación resuelta</span>
    		@else
    			<span class="badge badge-danger text-uppercase p-2">Intimación no resuelta</span>
    		@endif	
    	</div>

        <div class="col-md-12 mb-3">
            <h1 class="title">{{$intimation->title}}</h1>
        </div>

        <div class="col-md-12 mt-2">
        	<label for="">Intimación de</label>
            <p class="form-control">{{$intimation->user->fullName}}</p>
        </div>

        <div class="col-md-12 mt-2">
        	<label for="">Fecha</label>
            <p class="form-control">{{$intimation->created_at->isoFormat('LL')}}</p>
        </div>

        <div class="col-md-12 mt-2">
        	<label for="">Descripción</label>
            <p class="form-control">{{$intimation->description}}</p>
        </div>

        @if(count($intimation->files))
        	<div class="col-md-12  d-flex justify-content-between mt-4">
        		<small><strong>Intimación con archivos adjuntos</strong></small>
        		<a href="/files/downloadAll/{{$intimation->files->first()->id}}"><strong>DESCARGAR ADJUNTOS</strong></a>
        	</div>
        @endif

        @if($intimation->user_id == \Auth::id() && !$intimation->is_resolved)
        	<div class="col-md-12 mt-4">
        		<a href="/backoffice/intimations/{{$intimation->id}}/resolve" class="btn btn-success">Resolver intimación</a>
        	</div>
        @endif
    </div>
@endsection

@section('scripts')
    <script>var userId = {!!\Auth::id()!!};</script>
@endsection
