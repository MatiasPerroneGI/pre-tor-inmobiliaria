@extends('backoffice.layouts.app')

@section('section', 'Intimaciones')

@section('icon', 'exclamation-circle')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush

@section('widget-body')
    @php
        $contract = request()->has("contract_id") ? \App\Contract::find(request()->contract_id)->with('owners','guarantors','tenants')->first() : null;
    @endphp

    {!! KameForm::model($model, $formOptions) !!} 

        {!! KameForm::select('contract_id',$contract ? [$contract->id => $contract->name] : [])->label('Contrato')->col('md-8 sm-12') !!}

        {!! KameForm::text('',['disabled','class'=>'parte form-control'])->label('Intimar a')->col('md-8 sm-12')->id("parte") !!}
        
        {!! KameForm::text('title') !!}

        {!! KameForm::textarea('description',['rows'=>3]) !!}

        <div class="col-md-3 mb-3">
        	<a href="javascript:void(0);" class="btn mb-3 btn-success upload-file" data-toggle="tooltip" data-original-title="Adjuntar archivos" data-placement="top">
	            Agregar adjuntos
	        </a>
        </div>
        <div class="col-md-9 px-3 d-flex flex-column adjuntos">
        	<div class="progress w-50 file-upload ml-2" style="display: none">
	            <div class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
	        </div>
        </div>

        {!! KameForm::checkbox("include_guarantors")->label("Poner en copia a garante/s") !!}
        
        @if(\Auth()->user()->agency_id)
            {!! KameForm::checkbox("include_owners")->label("Poner en copia a propietario/s") !!}
        @endif

        {!! KameForm::text("",['disabled','class'=>'form-control owners'])->label("Propietario/s")->id("owners")->col(4) !!}
        {!! KameForm::text("",['disabled','class'=>'form-control tenants'])->label("Inquilino/s")->id("tenants")->col(4) !!}
        {!! KameForm::text("",['disabled','class'=>'form-control guarantors'])->label("Garante/s")->id("guarantors")->col(4) !!}        
  
        {!! KameForm::submit('Enviar') !!}

    {!! KameForm::close() !!}

    @include('backoffice.chat.partials.file-uploader', ['resource'=>'Intimation'])
@endsection


@section('scripts')
    {{-- <script src="/backend/js/app/Product.js"></script> --}}
    <script src="/backend/js/formplugins/select2/select2.bundle.js"></script>
    <script>
        var contract = {!! $contract ??  'null'!!};
    	var currentUser = {!! \Auth()->user()!!};
        var intimados = {!! $contract ? $contract->getIntimatedUsers() : 'null' !!};

        if (contract) fillContractInfo();

        $("select[name=contract_id").select2({
            ajax:{
                url: "/backoffice/autocomplete/contracts",
                // dataType: 'json',
                delay: 250,
                data: function(params){
                    return {
                        term: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params){
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination:{
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "Nombre del contrato",
            escapeMarkup: function(markup){
                return markup;
            }, 
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        }); 

        $("select[name=contract_id").trigger("change");
        $("select[name=contract_id]").change(function(){
            $.ajax({
                url:'/backoffice/intimations/getIntimatedUsers/' + contract.id,
                success:function(users){
                    intimados = users;
                    fillContractInfo();
                }
            })
        })

        function fillContractInfo(){
            intimados = $.map( intimados, function( user ) {
                text = user.name + ' ' + user.lastname;
                text += user.agency?' - '+user.agency.name : ''; 
                return text
            }).join(", ");

            let owners = contract.owners.map(owner => owner.name + ` ` + owner.lastname).join(", ")
            let guarantors = contract.guarantors.map(guarantor => guarantor.name + ` ` + guarantor.lastname).join(", ")
            let tenants = contract.tenants.map(tenant => tenant.name + ` ` + tenant.lastname).join(", ")
    
            $("input.owners").val(owners);
            $("input.guarantors").val(guarantors);
            $("input.tenants").val(tenants);
            $("input.parte").val(intimados)
        }

	    function formatRepo(repo){
	        if (repo.loading) return repo.text;

			contract = repo;
	        var markup = "<div class='select2-result-repository clearfix d-flex'>" +
	            "<div class='select2-result-repository__meta'>"+
	            "<div class='select2-result-repository__title fs-lg fw-500'>" + repo.name + "<br></div>"+
	            "</div></div>";

	        return markup;
	    }

	    function formatRepoSelection(repo){
	        return repo.name || repo.text;
	    }
    </script>
@endsection
