<li>
    <div class="d-flex align-items-center show-child-on-hover">
        <a href="/backoffice/intimations/{{$intimation->id}}" class="d-flex flex-column flex-1">
        	<div>
	        	@if($intimation->is_resolved)
	    			<span class="badge badge-warning text-uppercase">Resuelta</span>
	    		@else
	    			<span class="badge badge-danger text-uppercase">No resuelta</span>
	    		@endif	
    		</div>
            <span class="name">{{$intimation->name}}</span>
            <span class="msg-a fs-sm mt-2">{{$intimation->title}}</span>
            <span class="fs-nano text-muted mt-1">{{$intimation->created_at->format("d/m/Y")}}</span>
        </a>
    </div>
</li>