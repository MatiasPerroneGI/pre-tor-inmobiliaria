@extends('backoffice.layouts.app')

@section('section', 'Usuarios')

@section('action', 'Listar')

@section('icon', 'user')

@section('header-buttons')
    <a href="/backoffice/users/create" class="btn btn-outline-primary waves-effect waves-themed">
        Nuevo usuario
    </a>
@endsection

@section('widget-body')
    <!-- datatable start -->
    <div class="row">
        <div class="col-md-12">
            <table id="datatable" class="table w-100">
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>DNI</th>
                        <th>Email</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>

            </table>
            <!-- datatable end -->
        </div>
    </div>
    
@endsection

@section('scripts')
    <script src="/backend/js/backoffice/Users.js"></script>
@endsection
