@extends('backoffice.layouts.app')

@section('section', 'Usuario')

@section('icon', 'user')

@push("styles")
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/select2/select2.bundle.css">
@endpush

@section('widget-body')
    {!! KameForm::model($model, $formOptions) !!} 

        {!! KameForm::text('name')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('lastname')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('dni')->col('md-3 sm-12') !!}
        
        {!! KameForm::text('email')->col('md-3 sm-12') !!}

        {{-- TODO --}}
        {!! KameForm::select('permissions[]',config('permission.models.permission')::whereIn('id',[10,17,18,19])->pluck('name', 'id'),['multiple'=>'multiple'])->col('md-12 sm-12') !!}
        
        {!! KameForm::submit('Enviar') !!}
    
        <input type="text" name="type" hidden="true" value="{{request()->is("*clients*") ? 'clients' : 'users'}}">

    {!! KameForm::close() !!}
@endsection


@section("scripts")
    <script src="/backend/js/formplugins/select2/select2.bundle.js"></script>
    <script>
        $(document).ready(function(){
            $('.custom-select').select2();
        })
    </script>
@endsection
