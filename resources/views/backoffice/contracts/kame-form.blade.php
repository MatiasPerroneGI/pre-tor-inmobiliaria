@extends('backoffice.layouts.app')

@section('section', 'Contratos')

@section('icon', 'file')

@push('styles')
    <link rel="stylesheet" media="screen, print" href="/backend/css/formplugins/smartwizard/smartwizard.css">
@endpush

@section('widget-body')
    <form id="form-contract" method="post" action="/backoffice/contracts">
        @csrf
        <div id="wizard-contract" style="display: none;">
            <ul>
                <li><a href="#step-1">Paso 1<br><small>Contrato</small></a></li>
                <li><a href="#step-2">Paso 2<br><small>Locador/es</small></a></li>
                <li><a href="#step-3">Paso 3<br><small>Locatario/s</small></a></li>
                <li><a href="#step-4">Paso 4<br><small>Inmueble</small></a></li>
                <li><a href="#step-5">Paso 5<br><small>Plazo, devolución y multa</small></a></li>
                <li><a href="#step-6">Paso 6<br><small>Garante</small></a></li>
                <li><a href="#step-7">Paso 7<br><small>Cláusulas adicionales</small></a></li>
            </ul>
            <div class="p-3">

                @include('backoffice.contracts.steps._step1')

                @include('backoffice.contracts.steps._step2')

                @include('backoffice.contracts.steps._step3')

                @include('backoffice.contracts.steps._step4')

                @include('backoffice.contracts.steps._step5')

                @include('backoffice.contracts.steps._step6')

                @include('backoffice.contracts.steps._step7')

                <div class="form-actions">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="pager wizard no-margin no-steps-container">
                                <li class="previous">
                                    <a class="btn btn-lg tab-prev">Anterior</a>
                                </li>
                                <li class="next">
                                    <a class="btn btn-lg tab-next">Siguiente</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <textarea id="tpl-guarantor" style="display: none;">@include('backoffice.contracts.steps.tpl-guarantor')</textarea>
    <textarea id="tpl-owner" style="display: none;">@include('backoffice.contracts.steps.tpl-owner')</textarea>
    <textarea id="tpl-tenant" style="display: none;">@include('backoffice.contracts.steps.tpl-tenant')</textarea>
@endsection

@section('scripts')
    <script src="/backend/js/formplugins/smartwizard/smartwizard.js"></script>
    <script src="/backend/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
    <script src="/backend/js/formplugins/jquery-validate/bootstrapValidator.min.js"></script>
    <script src="/backend/js/app/Form.js"></script>
    <script src="/backend/js/app/Autocomplete.js"></script>
    <script src="/backend/js/app/RestricType.js"></script>
    <script src="/backend/js/backoffice/Contract.js"></script>
@endsection
