@extends('backoffice.layouts.app')

@section('section', 'Contratos')

@section('action', 'Listar')

@section('icon', 'file')

@section('header-buttons')
    {{-- <a href="/backoffice/contracts/create" data-toggle="tooltip" data-placement="top" title="Nuevo Contrato" class="btn btn-primary btn-lg btn-icon rounded-circle waves-effect waves-themed">
        <i class="fal fa-plus"></i>
    </a> --}}
    <a href="/backoffice/contracts/create" class="btn btn-outline-primary waves-effect waves-themed">
        {{-- <i class="fal fa-plus"></i>  --}}
        Nuevo contrato
    </a>
@endsection

@section('widget-body')
    <table id="datatable" class="table table-bordered table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Creado</th>
                <th>Nombre</th>
                <th>Locador/es</th>
                <th>Locatario/s</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
@endsection

@section('scripts')
    <script src="/backend/js/backoffice/Contracts.js"></script>
@endsection