<div id="step-5">
     <h3><strong>Paso 5</strong> - Plazo, devolución y multa</h3>

     <div class="row">
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_duration">Duración del contrato (en meses)</label>
               <input class="@error('contract.duration') is-invalid @enderror form-control input-lg" type="text" name="contract[duration]" id="contract_duration" data-restrict-type="int" required>
          </div>
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_start">Incio del contrato</label>
               <input class="@error('contract.start') is-invalid @enderror form-control input-lg" type="text" name="contract[start]" id="contract_start" data-datepicker required>
          </div>
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_end">Fin del contrato</label>
               <input class="@error('contract.end') is-invalid @enderror form-control input-lg" type="text" name="contract[end]" id="contract_end" readonly>
          </div>
     </div>

     <div class="row">
          <div class="mb-3 col-sm-3">
               <label class="form-label" for="contract_price">Canon inicial</label>
               <input class="@error('contract.price') is-invalid @enderror form-control input-lg" type="text" name="contract[price]" id="contract_price" data-restrict-type="int" required>
          </div>
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_deposit_time">Meses depósito</label>
               <select class="form-control input-lg" id="contract_deposit_time" name="contract[deposit_time]">
                    <option value="0.5">Medio mes</option>
                    <option value="1">Un mes</option>
                    <option value="1.5">Un mes y medio</option>
                    <option value="2">Dos meses</option>
                    <option value="2.5">Dos meses y medio</option>
                    <option value="3">Tres meses</option>
               </select>
          </div>
          <div class="mb-3 col-sm-3">
               <label class="form-label" for="contract_dollar_deposit">Depósito en USD</label>
               <select id="contract_dollar_deposit" name="contract[dollar_deposit]" class="form-control input-lg">
                    <option value="0">No</option>
                    <option value="1">Si</option>
               </select>

          </div>
          <div class="mb-3 col-sm-3">
               <label class="form-label" for="contract_deposit_value">Valor del depósito</label>
               <input class="@error('contract.deposit_value') is-invalid @enderror form-control input-lg" type="text" id="contract_deposit_value" name="contract[deposit_value]" readonly>
          </div>
     </div>

     <div class="row">
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_increase">Porcentaje de aumento</label>
               <input class="@error('contract.increase') is-invalid @enderror form-control input-lg" type="number" name="contract[increase]" id="contract_increase" data-restrict-type="float" min="0" max="200" required>
          </div>
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_time">Perído del aumento (en meses)</label>
               <input class="@error('contract.time') is-invalid @enderror form-control input-lg" type="number" step="1" name="contract[time]" id="contract_time" data-restrict-type="int" required>
          </div>
          <div class="mb-3 col-sm-4">
               <label class="form-label" for="contract_total">Monto total del contrato</label>
               <input class="@error('contract.total') is-invalid @enderror form-control input-lg" name="contract[total]" id="contract_total" readonly>
          </div>
     </div>

     <div class="row">
          <div class="mb-3 col-sm-6">
               <label class="form-label" for="contract_anticipated_payment">Fecha de pago</label>
               <select class="@error('contract.anticipated_payment') is-invalid @enderror form-control input-lg" name="contract[anticipated_payment]" id="contract_anticipated_payment">
                    <option value="0">Mes en curso</option>
                    <option value="1">Mes vencido</option>
               </select>
          </div>
          <div class="mb-3 col-sm-6">
               <label class="form-label" for="contract_penalty">Intereses moratorios</label>
               <input class="@error('contract.penalty') is-invalid @enderror form-control input-lg" type="text" name="contract[penalty]" id="contract_penalty" data-restrict-type="int">
          </div>
     </div>
</div>
