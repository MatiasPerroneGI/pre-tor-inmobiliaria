<div style="padding: 10px; border: solid 1px #b1b1b1; margin-top: 10px" data-person-box="guarantor">
     <div class="row">
          <div class="mb-3 col-sm-12">
               <div class="form-group">
                    <label class="form-label" for="guarantor_type">Tipo</label>
                    <select id="guarantor_type" name="guarantors[{{$i ?? '${i}' }}][type]" class="form-control input-lg" data-shower-triger="guarantors[{{$i ?? '${i}' }}][type]">
                         <option value="1">Bienes del locatario</option>
                         <option value="2">Seguro de caución</option>
                         <option value="3">Fianza simple</option>
                         <option value="4">Fianza con afectación de inmueble particular</option>
                    </select>
               </div>
          </div>
     </div>

     <div class="row">
          <div class="mb-3 col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="2,3,4">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_name" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="3,4">Nombre</label>
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_name" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="2">Razón social</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][name]" id="guarantor_{{$i ?? '${i}' }}_name">
          </div>
          <div class="col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="3,4">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_lastname">Apellido</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][lastname]" id="guarantor_{{$i ?? '${i}' }}_lastname">
          </div>
          <div class="col-sm-4">
               <label class="form-label" for="legal_address{{$i ?? '${i}' }}">Domicilio para notificcaciones</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][legal_address]" id="legal_address{{$i ?? '${i}' }}">
          </div>
          <div class="col-sm-4">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_email">Email</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][email]" id="guarantor_{{$i ?? '${i}' }}_email">
          </div>
          <div class="col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="2,3,4">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_dni" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="3,4">DNI</label>
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_dni" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="2">CUIT</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][dni]" id="guarantor_{{$i ?? '${i}' }}_dni">
          </div>
          <div class="col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="2">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_polize">Poliza</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][polize]" id="guarantor_{{$i ?? '${i}' }}_polize">
          </div>
          <div class="col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="4">
               <label class="form-label" for="guaranty_address{{$i ?? '${i}' }}">Domicilio de la garantía</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][guaranty_address]" id="guaranty_address{{$i ?? '${i}' }}">
          </div>
          <div class="col-sm-4" data-shower-target="guarantors[{{$i ?? '${i}' }}][type]" data-shower-value="1,4">
               <label class="form-label" for="guarantor_{{$i ?? '${i}' }}_plate">Matrícula</label>
               <input class="form-control input-lg" type="text" name="guarantors[{{$i ?? '${i}' }}][plate]" id="guarantor_{{$i ?? '${i}' }}_plate">
          </div>
     </div>

     <div class="row">
          <div class="col-sm-12">
               <a href="#" data-remove-person="guarantor" class="text-danger">
                    <span class="fal fa-times"></span>
                    Remover Garante
               </a>
          </div>
     </div>
</div>
