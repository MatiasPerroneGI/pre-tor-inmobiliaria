<div class="tab-pane" id="tab5">
     <h3><strong>Paso 5</strong> - Notificación y domicilios</h3>

     <h4>Locador</h4> <br>

     <div class="row">
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Email</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="@error('address.owner') is-invalid @enderror form-control input-lg" value="" type="text" name="address[owner]" id="area">
                    </div>
               </div>
          </div>
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Domicilio</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="form-control input-lg" value="" type="text" name="" id="area">
                    </div>
               </div>
          </div>
     </div>

     <h4>Locatario</h4> <br>

     <div class="row">
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Email</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="@error('area') is-invalid @enderror form-control input-lg" type="text" name="area" id="area">
                    </div>
               </div>
          </div>
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Domicilio</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="@error('address.client_address') is-invalid @enderror form-control input-lg" type="text" name="address[client_address]" id="area">
                    </div>
               </div>
          </div>
     </div>

     <h4>Garante</h4> <br>

     <div class="row">
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Email</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="@error('area') is-invalid @enderror form-control input-lg" type="text" name="area" id="area">
                    </div>
               </div>
          </div>
          <div class="col-sm-6">
               <div class="form-group">
                    <label for="area">Domicilio</label>
                    <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                         <input class="@error('address.guarantor_address') is-invalid @enderror form-control input-lg" type="text" name="address[guarantor_address]" id="area">
                    </div>
               </div>
          </div>
     </div>
 </div>
