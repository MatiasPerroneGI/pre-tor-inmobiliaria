<div id="step-7">
     <h3><strong>Paso 7</strong> - Cláusulas adicionales</h3>

     <div class="row">
          <div class="mb-3 col-sm-12">
               <label for="has_fire_insurance">Seguro de incendios</label>
               <select id="has_fire_insurance" name="has_fire_insurance" class="form-control input-lg" data-shower-triger="fire_insurance">
                    <option value="1">Si</option>
                    <option value="0">No</option>
               </select>
          </div>
          <div class="mb-3 col-sm-6" data-shower-target="fire_insurance" data-shower-value="1">
               <label for="fire_insurance_policy" data-shower-target="fire_insurance" data-shower-value="1">N° de Póliza</label>
               <input class="@error('fire_insurance.policy') is-invalid @enderror form-control input-lg" type="text" name="fire_insurance[policy]" id="fire_insurance_policy">
          </div>
          <div class="mb-3 col-sm-6" data-shower-target="fire_insurance" data-shower-value="1">
               <label for="fire_insurance_company" data-shower-target="fire_insurance" data-shower-value="1">Compañía</label>
               <input class="@error('fire_insurance.company') is-invalid @enderror form-control input-lg" type="text" name="fire_insurance[company]" id="fire_insurance_company">
          </div>
     </div>

     <div class="row">
          <div class="mb-3 col-sm-12">
               <label for="extra_data">Cláusulas adicionales</label>
               <textarea class="form-control input-lg" name="extra_data" id="extra_data" rows="6"></textarea>
          </div>
     </div>
 </div>
