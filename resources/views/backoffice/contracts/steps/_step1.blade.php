<div id="step-1">
    <h3><strong>Paso 1</strong> - Datos del contrato</h3>

    <div class="row">
          {{--
          <div class="col-sm-6">
               <div class="form-group">
                    <label class="form-label" for="contract_type">Tipo de contrato</label>
                    <select class="@error('contract.type') is-invalid @enderror form-control input-lg" id="contract_type" name="contract[type]">
                         <option value="location">Locacion</option>
                         <option value="addendum">Adenda</option>
                    </select>
               </div>
          </div>
          --}}

        <div class="mb-3 col-md-6">
            <label class="form-label" for="contract_name">¿Cómo llamaremos a este contrato?</label>
            <input class="@error('contract.name') is-invalid @enderror form-control input-lg" type="text" name="contract[name]" id="contract_name" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="contract_payment_method_id">Método de pago</label>
            <select class="@error('contract.payment_method_id') is-invalid @enderror form-control input-lg" id="contract_payment_method_id" name="contract[payment_method_id]" required>
                @foreach (\App\PaymentMethod::all() as $paymentMethod)
                  <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->value }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row">
      <div class="mb-3 col-sm-6">
            <label class="form-label" for="contract_place">¿Dónde se celebrará este contrato?</label>
            <input class="@error('contract.place') is-invalid @enderror form-control input-lg" type="text" name="contract[place]" id="contract_place" autocomplete="address-level2" required>
      </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="contract_jurisdiction">Provincia donde se seran resueltos los posibles conflictos</label>
            <input class="@error('contract.jurisdiction') is-invalid @enderror form-control input-lg" type="text"  name="contract[jurisdiction]" autocomplete="address-level1" id="contract_jurisdiction" required>
        </div>
    </div>
</div>


{{--
    <div class="mb-3 col-sm-6">
        <label class="form-label" for="name">Tú domicilio legal</label>
        <input class="@error('owner_address') is-invalid @enderror form-control input-lg" type="text" name="owner_address" id="owner_address" required>
    </div>
--}}
