<div id="step-6">
     <h3>
          <strong>Paso 6</strong> - Garante/s
     </h3>
     <div id="guarantors">
          @forelse($guarantors as $i=> $guarantor)
          @include("backoffice.contracts.steps.tpl-guarantor",['i' => $i + 1, 'guarantor' => $guarantor])
          @empty
          @include("backoffice.contracts.steps.tpl-guarantor",['i' => 0, 'guarantor' => new \App\User])
          @endforelse
     </div>
     <div class="row" style="margin-top: 15px">
          <div class="col-sm-12">
               <a href="#" data-add-person="guarantor">
                    <span class="fal fa-plus"></span>
                    Agregar Garante
               </a>
          </div>
     </div>
 </div>
