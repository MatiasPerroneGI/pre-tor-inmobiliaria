<div style="padding: 10px; border: solid 1px #b1b1b1; margin-top: 10px" data-person-box="tenant">
     <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="dni">DNI</label>
            <input class="form-control input-lg" data-restrict-type="int" type="number" name="tenant[${i}][dni]" id="tenats__{{$i ?? '${i}'}}_dni" data-autocomplete-trigger="tenant${i}" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="email">Email</label>
            <input class="form-control input-lg" type="email" name="tenant[${i}][email]" id="tenats__{{$i ?? '${i}'}}_email" data-autocomplete-target="tenant${i}" data-autocomplete-prop="email" required>
        </div>
    </div>

    <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="name">Nombre</label>
            <input class="form-control input-lg" type="text" name="tenant[${i}][name]" id="tenats__{{$i ?? '${i}'}}_name" data-autocomplete-target="tenant${i}" data-autocomplete-prop="name" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="lastname">Apellido</label>
            <input class="form-control input-lg" type="text" name="tenant[${i}][lastname]" id="tenats__{{$i ?? '${i}'}}_lastname" data-autocomplete-target="tenant${i}" data-autocomplete-prop="lastname" required>
        </div>
    </div>

    <div class="row">
        <div class="mb-3 col-sm-12">
            <label class="form-label" for="cuit">CUIT</label>
            <input class="form-control input-lg" type="text" name="tenant[${i}][cuit]" id="tenats__{{$i ?? '${i}'}}_cuit" data-autocomplete-target="tenant${i}" data-autocomplete-prop="cuit" required>
        </div>
    </div>

     <div class="row">
          <div class="col-sm-12">
               <a href="#" data-remove-person="tenant" class="text-danger">
                    <span class="fal fa-times"></span>
                    Remover Locador
               </a>
          </div>
     </div>
</div>
