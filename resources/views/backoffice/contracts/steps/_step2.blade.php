<div id="step-2">
    <h3>
        <strong>Paso 2</strong> - Datos del/los locador/es
    </h3>
    <div id="owners">
        @foreach($owners as $i=> $owner)
        @include("backoffice.contracts.steps.tpl-owner",['i' => $i, 'owner' => $owner])
        @endforeach
    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-sm-12">
            <a href="#" data-add-person="owner">
                <span class="fal fa-plus"></span>
                Agregar Locador
            </a>
        </div>
    </div>
</div>
