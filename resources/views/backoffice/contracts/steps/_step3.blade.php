<div id="step-3">
     <h3>
          <strong>Paso 3</strong> - Datos del/los locatario/s
     </h3>
     <div id="tenants">
          @foreach($tenants as $i=> $tenant)
          @include("backoffice.contracts.steps.tpl-tenant",['i' => $i, 'tenant' => $tenant])
          @endforeach
     </div>
     <div class="row" style="margin-top: 15px">
          <div class="col-sm-12">
               <a href="#" data-add-person="tenant">
                    <span class="fal fa-plus"></span>
                    Agregar Locatario
               </a>
          </div>
     </div>
</div>
