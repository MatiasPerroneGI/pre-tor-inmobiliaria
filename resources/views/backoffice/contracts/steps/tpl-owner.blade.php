<div style="padding: 10px; border: solid 1px #b1b1b1; margin-top: 10px" data-person-box="owner">
     <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_dni">DNI</label>
            <input value="{{$owner->dni ?? ''}}" class="form-control input-lg" data-restrict-type="int" type="number" name="owners[{{$i ?? '${i}' }}][dni]" id="owners_{{$i ?? '${i}'}}_dni" data-autocomplete-trigger="owners{{$i ?? '${i}' }}" autocomplete="dni" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_email">Email</label>
            <input class="form-control input-lg" value="{{$owner->email ?? ''}}" type="email" name="owners[{{$i ?? '${i}' }}][email]" id="owners_{{$i ?? '${i}'}}_email" data-autocomplete-target="owners{{$i ?? '${i}' }}" data-autocomplete-prop="email" autocomplete="email" required>
        </div>
    </div>

    <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_name">Nombre</label>
            <input class="form-control input-lg" value="{{$owner->name ?? ''}}" type="text" name="owners[{{$i ?? '${i}' }}][name]" id="owners_{{$i ?? '${i}'}}_name" data-autocomplete-target="owners{{$i ?? '${i}' }}" data-autocomplete-prop="name" autocomplete="name" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_lastname">Apellido</label>
            <input class="form-control input-lg" value="{{$owner->lastname ?? ''}}" type="text" name="owners[{{$i ?? '${i}' }}][lastname]" id="owners_{{$i ?? '${i}'}}_lastname" data-autocomplete-target="owners{{$i ?? '${i}' }}" data-autocomplete-prop="lastname" autocomplete="lastname" required>
        </div>
    </div>

    <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_cuit">CUIT</label>
            <input class="form-control input-lg" value="{{$owner->cuit ?? ''}}" type="text" name="owners[{{$i ?? '${i}' }}][cuit]" id="owners_{{$i ?? '${i}'}}_cuit" data-autocomplete-target="owners{{$i ?? '${i}' }}" data-autocomplete-prop="cuit" autocomplete="cuit" required>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="owners_{{$i ?? '${i}'}}_legal_address${i}">Domicilio para notificaciones</label>
            <input class="form-control input-lg" type="text" name="owners[${i}][legal_address]" id="owners_{{$i ?? '${i}'}}_legal_address${i}" autocomplete="street-address">
        </div>
    </div>

     <div class="row">
          <div class="col-sm-12">
               <a href="#" data-remove-person="owner" class="text-danger">
                    <span class="fal fa-times"></span>
                    Remover Locador
               </a>
          </div>
     </div>
</div>
