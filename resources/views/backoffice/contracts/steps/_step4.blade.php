<div id="step-4">
    <h3><strong>Paso 4</strong> - Datos del Inmueble</h3>

    @if(count($properties) > 1)
        <div class="row">
            <div class="col-md-12 mb-3">
                <label class="form-label" for="">Inmuebles disponibles</label>
                {!! \Form::select("",$properties->pluck("address","id")->toArray(),null,['class'=>'form-control select-property','placeholder'=>'Crear inmueble', 'data-select-property']) !!}
            </div>
        </div>
    @endif
    <input type="hidden" name="property[id]" value="{{count($properties) == 1 ? $properties[0]->id : ''}}" data-autocomplete-target="property" data-autocomplete-prop="id">

    <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="property_address">Dirección</label>
            <input class="@error('property.address') is-invalid @enderror form-control input-lg" placeholder="Ej.: Figueroa Alcorta 2263" type="text" name="property[address]" id="property_address" data-autocomplete-trigger="property" required value="{{count($properties) == 1 ? $properties[0]->address : ''}}">
        </div>

        <div class="mb-3 col-sm-6">
            <label class="form-label" for="property_area">Superficie en m<sup>2</sup></label>
            <input class="@error('property.area') is-invalid @enderror form-control input-lg" type="number" name="property[area]" id="property_area" data-autocomplete-target="property" data-autocomplete-prop="area" data-restrict-type="float" required value="{{count($properties) == 1 ? $properties[0]->area : ''}}">
        </div>
    </div>


    <div class="row">
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="property_description">Descripción del inmueble</label>
            <textarea rows="6" class="@error('property.description') is-invalid @enderror form-control" type="text" name="property[description]" id="property_description" data-autocomplete-target="property" data-autocomplete-prop="description">{{count($properties) == 1 ? $properties[0]->description : ''}}</textarea>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" for="property_accessories">Descripción de artefactos y accesorios del inmueble</label>
            <textarea rows="6" class="@error('property.accessories') is-invalid @enderror form-control" type="text" name="property[accessories]" id="property_accessories" data-autocomplete-target="property" data-autocomplete-prop="accessories" >{{count($properties) == 1 ? $properties[0]->accessories : ''}}</textarea>
        </div>
    </div>

    <div class="row">
        <div class="mb-3 col-sm-2 smart-form">
            <label class="form-label" class="toggle">
            <input type="checkbox" name="contract[commerce]" value="1" data-toggle-triger="commercial-use" data-toggle-prop="disabled">
            <i data-swchon-text="Si" data-swchoff-text="No" style="top: 60px; left: 0"></i>Destino comercial
            </label>
        </div>
        <div class="mb-3 col-sm-10">
            <label class="form-label" for="contract_activity">La actividad comercial consiste en:</label>
            <textarea class="@error('contract.activity') is-invalid @enderror form-control" data-toggle-target="commercial-use" rows="6" type="text" name="contract[activity]" id="contract_activity" disabled></textarea>
        </div>
    </div>

    <hr>

    <div class="smart-form">
        <label class="form-label" class="label">Servicios a cargo del locatario</label>
        <div class="row">
            @foreach ($services as $service)
            <div class="col col-md-2">
                <label class="form-label" class="checkbox">
                    <input type="checkbox" name="services[]" value="{{ $service->id }}">
                    <i></i>{{ $service->value }}
                </label>
            </div>
            @endforeach
        </div>
    </div>
</div>

@push("scripts")
    <script>
        var properties =
        @if($properties)
            {!! $properties !!}
        @else
            {}
        @endif

        $("[data-select-property]").change(function(){
            prop_id = (new Number($(this).val())).valueOf();
            if(!prop_id) return emptyProperty();
            prop = properties.find(p => p.id == prop_id)
            fillProperty(prop || {})
        })

        function fillProperty(chosen){
            $("[name='property[address]']").val(chosen.address || '')
            $("[name='property[area]']").val(chosen.area || '')
            $("[name='property[id]']").val(chosen.id || '')
            $("[name='property[description]']").val(chosen.description || '')
            $("[name='property[accessories]']").val(chosen.accessories || '')
        }

        function emptyProperty(){
            fillProperty({})
        }
    </script>
@endpush
