
    <header>
        <img src="https://pre-tor.com/img/pdf.png" alt="Logo" width="250" class="logo"/>
        <style type="text/css">
            .red {
                border: solid 1px red
            }
        </style>
    </header>

    {{--
    <footer class="information">
        <img src="https://pre-tor.com/img/pdf.png" alt="Logo" width="250" class="logo"/>
    </footer>
    --}}

    <div id="watermark">
        <img style="opacity:0.4;" src="https://pre-tor.com/img/watermark.png" height="100%" width="100%" />
    </div>

    <main class="invoice">
        <table width="100%">
           @foreach ($version->clauses()->with('version.contract')->get() as $clause)
                {{-- if $clause->type == ''editable', 'appendable'' --}}

                <div class="{{ $clause->edited ? 'red' : '' }}">
                    <h3>{{$clause->title}}</h3>
                    <p>
                        {!! nl2br($clause->render()) !!}
                    </p>
                </div>
                
            @endforeach
        </table>
    </main>