<span style="margin-left:15px;">
    -	SEGURO DE CAUCIÓN: En garantía del fiel y debido cumplimiento de las obligaciones contenidas en el presente CONTRATO, el/la LOCATARIO/A contrata y el/la LOCADOR/A lo acepta, un seguro de caución a favor del/de la LOCADOR/A con intervención de {{$contract->guarantor->name}} (en adelante la “ASEGURADORA”), póliza nro. {{$contract->guarantor->polize}}, con domicilio constituido a los efectos del presente en {{$contract->guarantor->address}}. En virtud de dicho contrato de seguro la ASEGURADORA, ante el incumplimiento del/de la LOCATARIO/A, garantiza al/ a la LOCADOR/a la debida percepción por éste del canon locativo previsto en el Contrato y/o el que resultase de conformidad a la renegociación del mismo oportunamente comunicado y aceptado por la ASEGURADORA, con más los gastos ocasionados por impuestos y servicios, de corresponder, obligándose el/la LOCADOR/A al cobrar estos importes, transferir a favor de la ASEGURADORA, los derechos que le correspondan respecto del/de la LOCATARIO/A hasta la suma abonada por la primera.
</span>
<br /><br />
<span style="margin-left:15px;">
    En caso de falta de pago del canon locativo mensual por el/la LOCATARIO/A, y para que el/la LOCADOR pueda reclamar a la ASEGURADORA los alquileres no cobrados, conforme a lo previsto por el Art. 827 del Código civil y Comercial de la Nación, deberá el/la LOCADOR/A, en forma fehaciente y luego de dos períodos impagos consecutivos, intimar al/a la LOCATARIO/A a que en el plazo de 15 días corridos abone la totalidad de las sumas adeudadas bajo apercibimiento de ejecución y desalojo de conformidad con lo previsto por el Art. 1222 y por el Art. 1219 inc. c) del Código Civil y Comercial de la Nación.
    El seguro de garantía referido, es condición de esta locación razón por la cual la tenencia del INMUEBLE le será entregada al/a la LOCATARIO/A una vez que el/la LOCADOR/A tenga en su poder la Póliza de Seguro aquí referido, debidamente extendido a su favor. El que pasará a formar parte de este CONTRATO como ANEXO I.
</span>
<br /><br />
<span style="margin-left:15px;">
    INTRANSFERIBILIDAD El presente contrato de locación es absolutamente intransferible y su trasgresión se considerara especial causal de desalojo, asimismo, le queda prohibido al LOCATARIO subarrendarlo total o parcialmente, ni dar el inmueble en préstamo aunque sea gratuito ni permitir su ocupación por terceros en ningún carácter.
</span>
<br /><br />
