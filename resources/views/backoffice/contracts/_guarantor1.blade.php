<span style="margin-left:15px;">
    -	Sin perjuicio de lo anterior, en garantía del cumplimiento de cada una de las obligaciones derivadas del CONTRATO, el/la LOCATARIO/A afecta en modo particular a esta garantía el inmueble de su propiedad sito en la calle {{$contract->guarantor->address}}, inscripto en el Registro de la Propiedad Inmueble bajo Matricula N° {{$contract->guarantor->plate}}. Por este motivo, el/la LOCATARIO/A asume el compromiso de no enajenar dicho inmueble sin el expreso y previo consentimiento del/de la LOCADOR/A -otorgado en la Plataforma de Administración Contractual-, debiendo en tal caso sustituir previamente la garantía con otro inmueble de su propiedad que garantice de modo suficiente las obligaciones asumidas en el CONTRATO.
</span>
<br /><br />
<span style="margin-left:15px;">
    El/la LOCATARIO/A garantizan que el título ostenta sobre dicha propiedad es legítimo, y que la misma se encuentra libre de gravamen de cualquier tipo.
</span>
