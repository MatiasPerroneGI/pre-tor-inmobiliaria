
    <header>
        <img src="https://pre-tor.com/img/pdf.png" alt="Logo" width="250" class="logo"/>
    </header>

    {{--
    <footer class="information">
        <img src="https://pre-tor.com/img/pdf.png" alt="Logo" width="250" class="logo"/>
    </footer>
    --}}

    <div id="watermark">
        <img style="opacity:0.4;" src="https://pre-tor.com/img/watermark.png" height="100%" width="100%" />
    </div>


    <main class="invoice">
        <h2 align="center">CONTRATO DE LOCACIÓN {{!$contract->commerce? 'DE VIVIENDA': 'PARA COMERCIO'}}</h2>
        <table width="100%">
            <p align="justify" style="width:60%;margin-left:20%">
                <span style="margin-left:45%">Entre:</span>
                <br /><br />
                -	@foreach ($contract->owners as $owner)
                    El/La Sr./a. {{$owner->lastname}} ,{{$owner->name}}  , D.N.I. {{$owner->dni}}, con domicilio en calle {{$owner->pivot->legal_address}} y
                    @endforeach
                    {{count($contract->owners) > 1? 'en adelante denominados/as   “LOCADORES/AS”;':'en adelante denominado/a   “LOCADOR/A”; '}}
                    <br />
                -   @foreach ($contract->tenants as $tenant)
                    El/La Sr./a. {{$tenant->lastname}} ,{{$tenant->name}}  , D.N.I. {{$tenant->dni}}, con domicilio en calle {{$tenant->pivot->legal_address}} y
                    @endforeach
                    {{count($contract->tenants) > 1? 'en adelante denominados/as “LOCATARIOS/AS”;':'en adelante denominado/a “LOCATARIO/A”;'}} <br />
                -	En lo sucesivo, al referirse en forma conjunta a LOCADOR/A y LOCATARIO/A, se<br /> lo hará bajo la locución “PARTES”; <br /><br />
            </p>
        </table>
        <table width="100%">
            <p align="justify" style="width:60%;margin-left:20%">
                <span class="underline bold" style="margin-left:37%;">-	Y Considerando:</span>
                <br /><br />
                -	1°.Que el/la LOCADOR/A es propietario/a de un inmueble ubicado en calle {{$contract->property->address}}. Y que es su intención arrendarlo para fines de {{!$contract->commerce? 'vivienda': 'comercio'}}.<br />
                -	2°. Que el/la LOCATARIO/A, en este acto declara conocer la intención de el/la
                <br /> LOCADOR/A y, a su vez, manifiesta su interés de locar el inmueble de propiedad de este/a último/a. <br /><br />
                <span style="font-weight:bold;"><span style="text-decoration:underline;">En consecuencia</span>, las PARTES de común acuerdo convienen celebrar el presente<br /> CONTRATO DE LOCACIÓN (el “CONTRATO”), que se regirá por el Código Civil y<br /> Comercial, los considerandos anteriores y las cláusulas que siguen:<br /></span>
            </p>
        </table>
        <table width="100%">
            <p align="justify" style="width:60%;margin-left:20%;">
                <span class="bold">
                    <span class="underline">
                        -   PRIMERA:
                    </span> LA COSA
                </span>
                <br /><br />

                Que el/la LOCADOR/A son propietarios de un inmueble situado en la dirección indicada en el considerando 1°, el cual consta de un total de {{$contract->property->area}} m2 (en adelante “el <span class="underline">INMUEBLE</span>”).
                <br/><br/>
                A)	<span class="underline">DESCRIPCIÓN DEL INMUEBLE:</span> {{$contract->property->description}}.
                <br/><br/>
                B)	<span class="underline">DESCRIPCIÓN DE ARTEFACTOS Y ACCESORIOS:</span> El INMUEBLE a su vez cuanta con las siguientes instalaciones: {{$contract->property->accesories}}. <br/><br/>
                El INMUEBLE se entrega en perfectas condiciones y, con todas sus dependencias y bienes en perfecto estado de conservación, funcionamiento y limpieza.

                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   SEGUNDA:
                    </span> OBJETO, ACEPTACIÓN Y DESTINO:
                </span>
                <br /><br />

                El/la LOCADOR/A loca el INMUEBLE al LOCATARIO/A, quien lo acepta en tal concepto, manifestando conocer el estado del mismo por haber revisado el mismo de forma exhaustiva, y se obliga a reintegrarlo a la finalización de la relación locativa, ya sea por vencimiento del plazo, resolución anticipada, rescisión, disposición judicial o cualquier otra causa, en igual estado de uso y conservación en que lo recibió, con más todas y cada una de las mejoras e incorporaciones de bienes realizadas con posterioridad, siendo responsabilidad suya, en forma exclusiva, la reparación y/o reposición de toda estructura, elemento y/o artefacto que falte y/o se encuentre deteriorado al mencionado tiempo de finalización.
                <br /><br />
                <span class="underline">DESTINO:</span>
                @if (!$contract->commerce)
                    El/la LOCATARIO/A afectará el INMUEBLE para exclusivos fines de vivienda, no pudiendo darle un uso distinto sin el consentimiento previo y escrito del/la LOCADOR/A.
                @else
                    El/la LOCATARIO/A afectará el INMUEBLE para el desarrollo de,su actividad comercial consistente en {{$contract->activity}}, no pudiendo darle un uso distinto sin el consentimiento previo y escrito del/la LOCADOR/A.
                @endif

                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   TERCERA:
                    </span> PLAZO, DEVOLUCIÓN Y MULTA:
                </span>
                <br /><br />

                El plazo de duración del CONTRATO será de {{$contract->duration}} MESES, a contar desde el día {{$contract->start}}. En consecuencia, su vencimiento operará el día {{$contract->end}}, de pleno derecho y sin necesidad de notificación alguna por parte del/de la LOCADOR/A; por lo que llegada esta fecha el/la LOCATARIO/A deberá restituir el INMUEBLE totalmente desocupado y en las condiciones establecidas en la cláusula anterior. En caso de que el/la LOCATARIO/A no cumpla con el estado de entrega del INMUEBLE, el/la LOCADOR/A tendrá derecho a no recibir el mismo hasta tanto se cumplan las condiciones de entrega pactadas en el presente.
                <br />
                Si una vez finalizada la relación locativa, independientemente del motivo de la finalización, el/la LOCATARIO/A retardara la devolución del INMUEBLE en las condiciones pactadas, deberá abonar a el/la LOCADOR/A, por cada día de atraso, una multa diaria equivalente al dos por ciento (3%) del canon locativo mensual, vigente a esa fecha como pago a cuenta de daños y perjuicios. Sin perjuicio del devengamiento del canon locativo por cada mes de retraso. El pago de dicha multa no obstará el ejercicio, por parte del/la LOCADOR/A, de cualquier acción judicial que pueda derivar del el incumplimiento.


                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   CUARTA:
                    </span> PRECIO Y MORA:
                </span>
                <br /><br />

                El precio total de la locación se fija en la suma de Pesos {{$contract->price}}; los que serán abonados por el/la LOCATARIO/A de la siguiente manera:
                @foreach ($contract->periods as $period)
                    ({{ chr(64 + $period->step) }}) Para el período comprendido entre el {{ $period->start->format('d-m-Y')}} y el {{$period->end->format('d-m-Y') }}, la suma de {{ $period->price }}.
                @endforeach
                en la suma de {{$contract->total}}.

                <br/><br/>
                El canon locativo correspondiente al primer mes será abonado por el/la LOCATARIO/A al momento de la firma del CONTRATO, sirviendo el presente de suficiente recibo. Con relación a cada canon posterior, el/la LOCATARIO/A deberá dejar constancia de su pago en la Plataforma de Administración Contractual mediante la carga del comprobante correspondiente.
                <br/><br/>

                Las PARTES acuerdan que el/la LOCATARIO/A deberán abonar el canon en efectivo por mes adelantado y del 1 al 10 de cada mes. Los pagos deberán realizarse [
                <br/><br/>

                @if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'TR')
                    <span style="margin-left:15px;">
                        -	Mediante deposito o transferencia bancaria a la Cuenta N° [___] del Banco [___], CBU [___], a nombre de [___], o en la cuenta bancaria donde éste indique en el futuro.
                    </span>
                    <br/><br/>
                @endif

                @if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'MP')
                    <span style="margin-left:15px;">
                        -	A través del sistema de pagos Mercado Pago, desde la plataforma de la Plataforma de Administración Contractual (conforme es definida más adelante), tras el registro y confirmación del CONTRATO.
                        <br/><br/>
                    </span>
                @endif
                @if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'EF')
                    <span style="margin-left:15px;">
                        -	En efectivo el domicilio de el/la LOCADOR/A o donde ellos lo indiquen en el futuro.
                    </span>
                    <br/><br/>
                @endif
                ]
                <br/><br/>
                El precio se pacta por mes completo y no es divisible, por lo que si el CONTRATO fuera resuelto por cualquier causa en un día que no coincida con el último del mes, el precio devengado será del canon mensual entero y no una fracción del mismo. Dicha circunstancia se mantendrá en caso de que el/la LOCATARIO/A se mantengan en la ocupación del INMUEBLE luego de que haya vencido el plazo para su devolución.<br/><br/>

                <span class="underline"> MULTA</span>.  El mero vencimiento del plazo sin que se efectúe el pago hará incurrir a el/la LOCATARIO/A en mora de pleno derecho, por lo que deberá abonar -por dicho atraso- intereses moratorios equivalentes al {{$contract->penalty}} %, los que se calcularan diariamente sobre el monto adeudado desde la producción de la mora.
                <br/><br/>

                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   QUINTA:
                    </span> ADMINISTRACIÓN Y REGISTRO DE LA CONTRATACIÓN:
                </span>
                <br /><br />

                En este acto las PARTES acuerdan centralizar la administración y registro del total desarrollo del CONTRATO bajo la plataforma online www.pre-tor.com (en adelante la “<span class="underline">Plataforma de Administración Contractual</span>”). En virtud de ello las PARTES se comprometen a dejar constancia dentro de la indicada plataforma online, y únicamente a través de los medios provistos por la misma, del cumplimiento o incumplimiento de todas las obligaciones establecidas por el presente, así como también de cualquier otra circunstancia que consideren de relevancia durante el desarrollo de la relación locativa.

                <br /><br />
                Las PARTES, en virtud del principio de buena fe contractual y por diligencia en cuanto al cumplimiento de las obligaciones asumidas en el CONTRATO, se comprometen a mantener accesos regulares a la referida Plataforma de Administración Contractual, durante el desarrollo de la totalidad de la relación locativa.

                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   SEXTA:
                    </span>
                    @if (!$contract->commerce)
                        SERVICIOS E IMPUESTOS.
                    @else
                        SERVICIOS, IMPUESTOS Y HABILITACIONES.
                    @endif
                </span>
                <br /><br />

                @php
                $text = '';
                foreach ($contract->services as $service) {
                    $text .= $service->value . ', ';
                }
                @endphp

                El/la LOCATARIO/A asume a su cargo el pago de todos los servicios utilizados por el INMUEBLE, a saber: {{$text}} Las obligaciones subsistirán hasta la efectiva restitución del INMUEBLE, con la conformidad del LOCADOR/A.

                <br /><br />

                El pago de los servicios e impuestos indicados son considerados por las PARTES como parte integrante de la prestación dineraria asumida por el/la LOCATARIO/A en virtud del presente contrato, en los términos del art. 1219 del Código Civil y Comercial.
                Al momento de efectuar el pago mensual de los cánones locativos, el/la LOCATARIO/A está obligado/a a acreditar en la Plataforma de Administración Contractual el pago de los servicios e impuestos indicados cuya fecha de vencimiento se corresponda con la del vencimiento mensual del cánon locativo a pagar.

                <br /><br />
                En caso de que el/la LOCATARIO/A no registre en la Plataforma de Administración Contractual los comprobantes de pago de los servicios e impuestos requeridos, el/la LOCADOR/A no estará obligado/a a recibir el pago del canon locativo. En ese caso se considerará incumplido el pago del mes o los meses respectivos, haciendo pasible a el/la LOCATARIO/A de todas las consecuencias previstas en el presente CONTRATO ante dicho incumplimiento.


                <br /><br />
                La recepción por parte de el/la LOCADOR/A del canon locativo no será entendida como la recepción de las constancias de pago de los conceptos previstos en esta cláusula, ni podrá bajo ningún punto de vista considerarse como una dispensa de dicha obligación del/de la LOCATARIO/A.


                <br /><br />

                @if ($contract->commerce)
                    <span class="underline">HABILITACIONES.</span>. El/La LOCATARIO/A se obliga a gestionar, mantener y pagar puntualmente cualquier otro gasto, tasa, impuesto y/o contribuciones necesarias y/o derivados de la obtención de las habilitaciones necesarias para el funcionamiento y/o ejercicio de la o las actividades comerciales a ser llevadas a cabo en el INMUEBLE. Las respectivas habilitaciones deberán ser obtenidas por el/la LOCATARIO/A a su exclusiva cuenta y riesgo, por lo que la demora en su tramitación o la negativa de su otorgamiento serán soportadas únicamente por este/a último/a, no pudiendo realizar reclamo alguno a el/la LOCADORA/A. El/La LOCATARIO/A se obliga a mantener indemne al/ a la LOCADOR/A por el pago de cualquier gravamen, tasa o impuesto derivado del desarrollo de la actividad comercial del/de la LOCATARIO/A en el INMUEBLE.


                    <br /><br />
                @endif
                Para el supuesto que el/la LOCATARIO/A contrate la prestación de servicios distintos a los indicados anteriormente (telefonía, televisión, internet, etc.), será responsable tanto de su alta como de la baja respectiva, y deberá acreditar ambas circunstancias en la Plataforma de Administración Contractual, antes de la restitución del INMUEBLE al/ a la LOCADOR/A, cualquiera sea causa por la que finalice la relación locativa.


                <br /><br />

                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   SEPTIMA:
                    </span> CONSERVACIÓN, Y MEJORAS.
                </span>
                <br /><br />

                El/la LOCADOR/A se obliga a conservar el INMUEBLE en el estado que permita al/ a la LOCATARIO/A usar y gozar del mismo conforme al destino previsto en la Cláusula Segunda del CONTRATO.
                <br /><br />
                El/la LOCATARIO/A asume la obligación de mantener la cosa en el estado que la recibió; de dar inmediata cuenta a el/la LOCADOR/A −a través de la Plataforma de Administración Contractual− de cualquier desperfecto que afecten la seguridad o el estado de conservación del INMUEBLE; y de permitir el libre acceso de el/la LOCADOR/A y/o a sus representantes a las dependencias del INMUEBLE, en la fecha a ser acordada a través de la referida plataforma para su inspección.
                <br /><br />
                <span class="underline">MEJORAS.</span> El/la LOCATARIO/A no podrá realizar mejora alguna en el INMUEBLE, ni obra que afecte o modifique la estructura construida, sin expresa autorización escrita de el/la LOCADOR/A, otorgada por medio de la Plataforma de Administración Contractual.
                <br /><br />
                Cualquier mejora que el/la LOCATARIO/A realice con la aprobación de el/la LOCADOR/A, podrá ser retirada por el/la LOCATARIO/A a la finalización de la relación locativa, siempre y cuando no afecte la estructura del INMUEBLE.
                <br /><br />
                El incumplimiento de cualquiera de las obligaciones asumidas por el/la LOCATARIO/A en la presente cláusula dará derecho a el/la LOCADOR/A a la resolución inmediata del presente CONTRATO y al reclamo de los daños y perjuicios emergentes de dichos incumplimientos.


                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   OCTAVA:
                    </span> INCUMPLIMIENTOS Y RESOLUCIÓN ANTICIPADA:
                </span>
                <br /><br />

                Cualquier incumplimiento del/de la LOCATARIO/A, sin perjuicio de las penalidades que se establecen en las demás cláusulas, habilitará al/a la LOCADOR/A a reclamar el cumplimiento de este CONTRATO y a la ejecución de las garantías establecidas en la Cláusula Décima.
                <br /><br />
                El/La LOCADOR/A podrá resolver el CONTRATO, por exclusiva culpa del LOCATARIO, cuando este último (i) afecte el INMUEBLE a un destino distinto al previsto en la cláusula Segunda, (ii) falte a su deber de conservar el INMUEBLE o haga abandono del mismo, o (iii) cuando el/la LOCATARIO/A incumpla con el pago de la prestación dineraria prevista en las clausulas Cuarta y Sexta, durante dos períodos consecutivos.
                <br /><br />
                En cualquiera de dichos casos el/la LOCADOR/A podrá accionar por el pago de lo adeudado y/o por el desalojo del INMUEBLE (art. 1086 CC y C), sin perjuicio de los daños y perjuicios cuyo reclamo correspondiera en cada caso. Previo a la resolución del contrato por la falta de pago del locatario en los términos indicados en el punto (iii) del párrafo anterior, el/la LOCADOR/A deberá intimar al/a la LOCATARIO/A, a través de la Plataforma de Administración Contractual, al pago de las sumas adeudadas en el plazo de 15 días corridos desde la recepción de dicha notificación.
                <br /><br />
                El/La LOCATARIO/A podrá reclamar el cumplimiento de este CONTRATO o resolverlo por exclusiva culpa del/de la LOCADOR/A, con el correspondiente reclamo de daños y perjuicios, cuando este último (i) incumpla con su obligación de conservar la cosa con aptitud para el use y goce del/de la LOCATARIO/A conforme al destino previsto en la Cláusula Segunda; o (ii) cuando el/la LOCATARIO/A se vea privado de la tenencia de la cosa por un tercero que resulte tener un mejor derecho sobre el INMUEBLE que el/la LOCADOR/A.
                <br /><br />
                El/la LOCATARIO/A podrá, trascurridos los seis (6) primeros meses de vigencia de la relación locativa, resolver el CONTRATO sin expresión de causa, debiendo notificar a través de la Plataforma de Administración Contractual su decisión a el/la LOCADOR/A con una antelación no menor a los sesenta días (60) anteriores a la fecha de restitución del INMUEBLE.
                <br /><br />
                En caso de que el/la LOCATARIO/A haga uso de la opción resolutoria citada dentro del primer año del CONTRATO, deberá abonar −en concepto de indemnización a favor de el/la LOCADOR/A− la suma equivalente a un mes y medio de locación al valor del canon que esté abonando al momento de hacer uso de dicha facultad, y la de un solo mes si la opción se ejercita con posterioridad al transcurso de dicho lapso. Para hacer uso de esta opción el/la LOCATARIO/A deberá estar al día en el cumplimiento de todas sus obligaciones. Además, deberá permitir al/a la LOCADOR/A a partir de la fecha de tal notificación, el ingreso al INMUEBLE para su exhibición a terceros interesados en un eventual alquiler.
                <br /><br />
                <span class="underline">ABANDONO:</span> Para el evento de que el/la LOCATARIO/A abandonare el INMUEBLE o depositare judicialmente las llaves, deberá abonar al/a la LOCADOR/A una multa igual al alquiler pactado desde la iniciación del juicio hasta el día en que el/la LOCADOR/A tome la libre y efectiva tenencia definitiva de la propiedad, debiendo indemnizarlo también por daños y perjuicios sufridos, siempre que dicha consignación de llaves no fuera imputable a conductas desplegadas por este último. Sin perjuicio de lo expuesto en caso de abandono, y para evitar los posibles deterioros que pudieran producirse y/o la ocupación ilegal de terceros, el/la LOCADOR/A queda facultado a retomar posesión del INMUEBLE por cualquier medio licito que resulte necesario a tal fin.
                <br /><br />


                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   NOVENA:
                    </span> RESPONSABILIDAD POR DAÑOS:
                </span>
                <br /><br />
                Ni el/la LOCADOR/A ni el/la LOCATARIO/A responderán frente a cualquier hecho ajeno a su voluntad que ocurriere con motivo de la locación, incluso en el supuesto de caso fortuito o fuerza mayor.
                <br /><br />
                <br/><br/>
                <span class="bold">
                    <span class="underline">
                        -   DECIMA:
                    </span> DEPÓSITO Y DEMAS GARANTIAS:
                </span>
                <br /><br />
                Para garantizar el pago inmediato de eventuales obligaciones relacionadas con roturas, daños, gastos de servicios de mantenimiento y conservación del INMUEBLE, materiales y mano de obra, pago de servicios e impuestos pendientes o vencidos y en general todo otro gasto relacionado con el INMUEBLE, el/la LOCATARIO/A entregará a el/la LOCADOR/A en concepto de depósito en garantía la suma de {{$contract->deposit_value}} equivalente al valor del canon de {{$contract->deposit_time}} meses, por cualquiera de los medios de pago consignados en la cláusula Cuarta. Queda expresamente prohibida la imputación de la suma de dinero entregada como depósito en garantía, al pago del canon locativo previsto en el CONTRATO, y en general a cualquier otra obligación diferente de las enumeradas anteriormente, salvo autorización por del/de la LOCADOR/A brindada por medio de la Plataforma de Administración Contractual.
                <br /><br />
                [
                <br /><br />
                {{-- START GARANTE TYPE 1 --}}

                <span style="margin-left:15px;">
                    -	Sin perjuicio de lo anterior, en garantía del cumplimiento de cada una de las obligaciones derivadas del CONTRATO, el/la LOCATARIO/A afecta en modo particular a esta garantía el inmueble de su propiedad sito en la calle {{$contract->guarantors->first()->guaranty_address}}, inscripto en el Registro de la Propiedad Inmueble bajo Matricula N° {{$contract->guarantors->first()->plate}}. Por lo que asume el compromiso de no enajenar dicho inmueble sin expreso y previo consentimiento del/de la LOCADOR/A, debiendo en tal caso sustituir la garantía con otro inmueble de su propiedad que garantice de modo suficiente las obligaciones asumidas en el CONTRATO.
                </span>
                <br /><br />
                <span style="margin-left:15px;">
                    El/la LOCATARIO/A garantizan que el título ostenta sobre dicha propiedad es legítimo, y que la misma se encuentra libre de gravamen de cualquier tipo.
                </span>
                {{-- END GARANTE TYPE 1 --}}
                <br /><br />
                {{-- START GARANTE TYPE 2 --}}

                <span style="margin-left:15px;">
                    -	SEGURO DE CAUCIÓN: En garantía del fiel y debido cumplimiento de las obligaciones contenidas en el presente CONTRATO, el/la LOCATARIO/A contrata y el/la LOCADOR/A lo acepta, un seguro de caución a favor del/de la LOCADOR/A con intervención de {{$contract->guarantors->first()->name}} (en adelante la “ASEGURADORA”), póliza nro. {{$contract->guarantors->first()->polize}}, con domicilio constituido a los efectos del presente en {{$contract->guarantors->first()->address}}. En virtud de dicho contrato de seguro la ASEGURADORA, ante el incumplimiento del/de la LOCATARIO/A, garantiza al/ a la LOCADOR/a la debida percepción por éste del canon locativo previsto en el Contrato y/o el que resultase de conformidad a la renegociación del mismo oportunamente comunicado y aceptado por la ASEGURADORA, con más los gastos ocasionados por impuestos y servicios, de corresponder, obligándose el/la LOCADOR/A al cobrar estos importes, transferir a favor de la ASEGURADORA, los derechos que le correspondan respecto del/de la LOCATARIO/A hasta la suma abonada por la primera.
                </span>
                <br /><br />
                <span style="margin-left:15px;">
                    En caso de falta de pago del canon locativo mensual por el/la LOCATARIO/A, y para que el/la LOCADOR pueda reclamar a la ASEGURADORA los alquileres no cobrados, conforme a lo previsto por el Art. 827 del Código civil y Comercial de la Nación, deberá el/la LOCADOR/A, en forma fehaciente y luego de dos períodos impagos consecutivos, intimar al/a la LOCATARIO/A a que en el plazo de 15 días corridos abone la totalidad de las sumas adeudadas bajo apercibimiento de ejecución y desalojo de conformidad con lo previsto por el Art. 1222 y por el Art. 1219 inc. c) del Código Civil y Comercial de la Nación.
                    El seguro de garantía referido, es condición de esta locación razón por la cual la tenencia del INMUEBLE le será entregada al/a la LOCATARIO/A una vez que el/la LOCADOR/A tenga en su poder la Póliza de Seguro aquí referido, debidamente extendido a su favor. El que pasará a formar parte de este CONTRATO como ANEXO I.
                </span>
                <br /><br />
                <span style="margin-left:15px;">
                    INTRANSFERIBILIDAD El presente contrato de locación es absolutamente intransferible y su trasgresión se considerara especial causal de desalojo, asimismo, le queda prohibido al LOCATARIO subarrendarlo total o parcialmente, ni dar el inmueble en préstamo aunque sea gratuito ni permitir su ocupación por terceros en ningún carácter.
                </span>
                <br /><br />
                <br /><br />
                {{-- END GARANTE TYPE 2 --}}
                {{-- START GARANTE TYPE 3 --}}

                {{-- END GARANTE TYPE 3 --}}





    <br/><br/>
    <span class="bold">
        <span class="underline">
            -   DECIMA PRIMERA:
        </span> NOTIFICACIONES Y DOMICILIOS:
    </span>
    <br /><br />

    Las PARTES por el presente acuerdan que desde la firma del CONTRATO y hasta la extinción de la relación locativa emergente del mismo, únicamente serán válidas las notificaciones electrónicas que las mismas se envíen por medio de la Plataforma de Administración Contractual, salvo que por motivos imputables a la referida plataforma, el envío de notificaciones no resulte posible. En dicho caso la parte que haya empleado un método de notificación diferente, se compromete a dejar registro en la Plataforma de Administración Contractual el contenido de la misma, una vez que su funcionamiento se haya restituido.

    <br /><br />

    Las PARTES acuerdan registrar el CONTRATO en la Plataforma de Administración Contractual bajo la denominación de {{$contract->name}}, y concentrar bajo dicho registro la totalidad de notificaciones cursadas con objeto de la relación contractual regida por el presente, las que solo podrán ser remitidas a las siguientes direcciones:
    <br /><br />

    <span style="margin-left:15px;" class="bold underline">
        -	LOCADOR/A: {{$contract->owners->first()->email}}.
    </span>
    <br />
    <span style="margin-left:15px;" class="bold underline">
        -	LOCATARIO/A: {{$contract->tenants->first()->email}}.
    </span>
    <br />
    <span style="margin-left:15px;" class="bold underline">
        -	FIADOR/A: {{$contract->guarantors->first()->email}}.
    </span>
    <br /><br />
    La falta de acceso y revisión a la Plataforma de Administración Contractual, no obstaculizará el perfeccionamiento de la notificación en cuestión, siempre y cuando, la referida plataforma confirme que el mensaje fue efectivamente receptado por el destinatario del mismo.
    <br /><br />

    <span class="underline">DOMICILIO:</span> Para todos los efectos legales y judiciales los firmantes constituyen los siguientes domicilios especiales, en los que serán válidas las notificaciones y diligencias que se practiquen en el supuesto que no puedan ser practicadas electrónicamente a través de la Plataforma de Administración Contractual por razones imputables a esta última:


    <br /><br />

    <span style="margin-left:15px;" class="bold underline">
        -	LOCADOR/A: {{$contract->owners->first()->pivot->legal_address}}.
    </span>
    <br />
    <span style="margin-left:15px;" class="bold underline">
        -	LOCATARIO/A: {{$contract->tenants->first()->pivot->legal_address}}.
    </span>
    <br />
    <span style="margin-left:15px;" class="bold underline">
        -	FIADOR/A: {{$contract->guarantors->first()->legal_address}}.
    </span>


    <br/><br/>
    <span class="bold">
        <span class="underline">
            -   DECIMO SEGUNDA:
        </span> SELLADO:
    </span>
    <br /><br />
    El impuesto de sellos (aforo) del presente CONTRATO será a cargo de las PARTES, en proporciones iguales. Cualquier otro gasto necesario para la ejecución del presente CONTRATO será asumido por la parte que lo haya generado.

    <br/><br/>
    <span class="bold">
        <span class="underline">
            -   DECIMO TERCERA:
        </span> COMPETENCIA Y PROCEDIMIENTOS DE DESALOJO:
    </span>
    <br /><br />

    Las PARTES acuerdan someter cualquier conflicto que emerja del presente CONTRATO a la jurisdicción y competencia de la Justicia Ordinaria de los Tribunales  de {{$contract->jurisdiction}}, con expresa renuncia a cualquier otro fuero y/o jurisdicción que pudiera corresponderles.

    <br /><br />

    <span class="bold">
        De conformidad, se firman tantos ejemplares como PARTES intervinientes de esta relación locativa, en {{$contract->place}}, a los {{\Carbon\Carbon::parse($contract->created_at)->day}} días del mes de {{\Carbon\Carbon::parse($contract->created_at)->locale('es')->monthName}} del {{\Carbon\Carbon::parse($contract->created_at)->year}}.
    </span>

</p>
</table>



</main>