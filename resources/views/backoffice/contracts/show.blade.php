@extends('admin.blank')

@section('content')
    <!-- RIBBON -->
    <div id="ribbon">

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Home</li>
            <li>Contratos</li>
            <li>{{ $contract->title }}</li>
        </ol>
        <!-- end breadcrumb -->

        <!-- You can also add more buttons to the
        ribbon for further usability

        Example below:

        <span class="ribbon-button-alignment pull-right">
        <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
        <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
        <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
        </span> -->

    </div>
    <!-- END RIBBON -->
    
    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa-fw fa fa-th-large"></i> 
                        {{ $contract->title }} 
                </h1>
            </div>
        </div>

        <section id="widget-grid" class="">

            <div class="row">
                @if ($contract->contratable->current_period)
                    <article class="col-xs-12 col-sm-4">
                        <div class="jarviswidget" id="wid-id-6" 
                                data-widget-colorbutton="false" 
                                data-widget-editbutton="false" 
                                data-widget-togglebutton="false" 
                                data-widget-deletebutton="false" 
                                data-widget-fullscreenbutton="false" 
                                data-widget-custombutton="false" 
                                data-widget-collapsed="false" 
                                data-widget-sortable="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-calendar text-info"></i> </span>
                                <h2>Mes en curso</h2>             
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <input class="form-control" type="text">    
                                </div>
                                <div class="widget-body">
                                    <h2 class="text-center">
                                        Canon actual: ${{ $contract->contratable->current_period->price }}
                                    </h2>
                                    <p class="text-center">
                                        Mes en curso {{ $contract->contratable->current_month }}&ordm;
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                @endif
                

                <article class="col-xs-12 col-sm-4">
                    <div class="jarviswidget" id="wid-id-6" 
                            data-widget-colorbutton="false" 
                            data-widget-editbutton="false" 
                            data-widget-togglebutton="false" 
                            data-widget-deletebutton="false" 
                            data-widget-fullscreenbutton="false" 
                            data-widget-custombutton="false" 
                            data-widget-collapsed="false" 
                            data-widget-sortable="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-clock-o text-info"></i> </span>
                            <h2>Duración</h2>             
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                                <input class="form-control" type="text">    
                            </div>
                            <div class="widget-body">
                                <h2 class="text-center">
                                    Finalización: {{ $contract->contratable->end->format('d-m-Y') }}
                                </h2>
                                <p class="text-center">
                                    Duración total: {{ $contract->contratable->duration }} meses
                                </p>
                            </div>
                        </div>
                    </div>
                </article>

                @if (\Auth('clients')->id() == $contract->contratable->owner_id)
                    <article class="col-xs-12 col-sm-4">
                        <div class="jarviswidget" id="wid-id-6" 
                                data-widget-colorbutton="false" 
                                data-widget-editbutton="false" 
                                data-widget-togglebutton="false" 
                                data-widget-deletebutton="false" 
                                data-widget-fullscreenbutton="false" 
                                data-widget-custombutton="false" 
                                data-widget-collapsed="false" 
                                data-widget-sortable="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-user text-info"></i> </span>
                                <h2>Locador</h2>             
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <input class="form-control" type="text">    
                                </div>
                                <div class="widget-body">
                                    <h2 class="text-center">
                                        {{ $contract->contratable->tenant->full_name }}
                                    </h2>
                                    <p class="text-center">
                                        <a href="/perfil/redirect-to-conversation/{{ $contract->contratable->tenant->id }}">Enviar mensaje</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                @else
                    <article class="col-xs-12 col-sm-4">
                        <div class="jarviswidget" id="wid-id-6" 
                                data-widget-colorbutton="false" 
                                data-widget-editbutton="false" 
                                data-widget-togglebutton="false" 
                                data-widget-deletebutton="false" 
                                data-widget-fullscreenbutton="false" 
                                data-widget-custombutton="false" 
                                data-widget-collapsed="false" 
                                data-widget-sortable="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-user text-info"></i> </span>
                                <h2>Locatario</h2>             
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <input class="form-control" type="text">    
                                </div>
                                <div class="widget-body">
                                    <h2 class="text-center">
                                        {{ $contract->contratable->owner->name }}
                                    </h2>
                                    <p class="text-center">
                                        <a href="/perfil/redirect-to-conversation/{{ $contract->contratable->owner->id }}">Enviar mensaje</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                @endif

                
            </div>

            <div class="row">
                <article class="col-xs-12 col-sm-8">
                    <div class="jarviswidget" id="wid-id-6" 
                            data-widget-colorbutton="false" 
                            data-widget-editbutton="false" 
                            data-widget-togglebutton="false" 
                            data-widget-deletebutton="false" 
                            data-widget-fullscreenbutton="false" 
                            data-widget-custombutton="false" 
                            data-widget-collapsed="false" 
                            data-widget-sortable="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bell text-info"></i> </span>
                            <h2>Notificaciones</h2>             
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <table id="datatable" class="table table-striped table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Asunto</th>
                                            <th>Ver</th>
                                        </tr>
                                    </thead>
                                    <tbody><!--Datatable--></tbody>
                                </table>

                                @include('clients.events._modal-show-event')
                            </div>
                        </div>
                    </div>
                </article>
            </div>


        </section>

    </div>
@endsection

@section('scripts')
    <script src="/js/clients/events.js"></script>
@endsection

