<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - #123</title>

    <style type="text/css">
        @page {
            margin: 0cm 0cm;
            size: A4 portrait;
            margin-top: 2cm;
            page: teacher;
            page-break-after: always;
        }

        #watermark {
            position: fixed;
            bottom:   37%;
            left:     25%;

            /** The width and height may change
            according to the dimensions of your letterhead
            **/
            width:    10cm;
            height:   10cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        header {
            position: fixed;
            top: -2cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            text-align: left;
            /* background-color: blue; */
            /* margin-bottom: 45px; */
        }
        header img {
            margin-top: 15px;
            margin-left: 35px;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }

        body {
            margin: 0;
            margin-top: 3cm;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice {
            margin-top: -2cm;
            margin-bottom: 4cm;
        }
        .invoice table {
            margin: 15px;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
        }

        /* .information .logo {
        margin: 5px;
        } */

        .information table {
            padding: 10px;
        }

        .underline{
            text-decoration:underline;
        }
        .bold{
            font-weight:bold;
        }

    </style>

</head>
<body>
    @include('backoffice.contracts._pdf')
</body>
</html>
