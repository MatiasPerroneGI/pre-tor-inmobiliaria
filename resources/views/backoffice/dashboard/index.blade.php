@extends('backoffice.layouts.app')

@section('subheader', '')
@section('section', 'Dashboard')

@section('extra-content')
	@php
		$user = \Auth()->user();
		$isAgency = $user->agency_id;
		$isOwner = count($user->owners);
		$isTenant = count($user->tenants);
	@endphp

	<div class="row">
		<div class="col-xl-10 col-sm-12 offset-xl-1">
		    <h2 class="title">Bienvenido {{\Auth()->user()->name}}</h2>

		    
		    <p class="w-80">At magnum periculum adiit in quo ignorare vos arbitrer, sed quia non quo enim ipsam per se texit, ne ad id ne ad modum, quaeso, interpretaris.</p>

			{{-- <div class="row mb-4">
				<div class="col-md-12 ">
					<a href="" class="btn btn-primary mr-2 mb-2">Button primary</a>
					<a href="" class="btn btn-secondary mr-2 mb-2">Button secondary</a>
					<a href="" class="btn btn-success mr-2 mb-2">Button success</a>
					<a href="" class="btn btn-info mr-2 mb-2">Button info</a>
					<a href="" class="btn btn-warning mr-2 mb-2">Button warning</a>
					<a href="" class="btn btn-danger mr-2 mb-2">Button danger</a>
					<a href="" class="btn btn-light mr-2 mb-2">Button light</a>
					<a href="" class="btn btn-dark mr-2 mb-2">Button dark</a>
				</div>
				<div class="col-md-12"><hr></div>
				<div class="col-md-12 ">	
					<a href="" class="btn btn-outline-primary mr-2 mb-2">Button outline primary</a>
					<a href="" class="btn btn-outline-secondary mr-2 mb-2">Button outline secondary</a>
					<a href="" class="btn btn-outline-success mr-2 mb-2">Button outline success</a>
					<a href="" class="btn btn-outline-info mr-2 mb-2">Button outline info</a>
					<a href="" class="btn btn-outline-warning mr-2 mb-2">Button outline warning</a>
					<a href="" class="btn btn-outline-danger mr-2 mb-2">Button outline danger</a>
					<a href="" class="btn btn-outline-light mr-2 mb-2">Button outline light</a>
					<a href="" class="btn btn-outline-dark mr-2 mb-2">Button outline dark</a>
				</div>

				<div class="col-md-12">
					<h1>Heading 1</h1>
					<h2>Heading 2</h2>
					<h3>Heading 3</h3>
					<h4>Heading 4</h4>
					<h5>Heading 5</h5>
					<h6>Heading 6</h6>
				</div>

				<div class="col-md-12 ">
					<a href="" class="badge badge-primary mr-2 mb-2">Badge primary</a>
					<a href="" class="badge badge-secondary mr-2 mb-2">Badge secondary</a>
					<a href="" class="badge badge-success mr-2 mb-2">Badge success</a>
					<a href="" class="badge badge-info mr-2 mb-2">Badge info</a>
					<a href="" class="badge badge-warning mr-2 mb-2">Badge warning</a>
					<a href="" class="badge badge-danger mr-2 mb-2">Badge danger</a>
					<a href="" class="badge badge-light mr-2 mb-2">Badge light</a>
					<a href="" class="badge badge-dark mr-2 mb-2">Badge dark</a>
				</div>

				<div class="col-md-12">
					<div action="" class="row">
						<div class="mb-3 col-md-3 col-sm-12">
				            <label class="form-label" for="email">Email</label>
				            <input name="email" type="text" class="form-control" id="email">
				            <div class="invalid-feedback"></div>
				        </div>
					</div>

					<div class="row">
						<div class="col-md-6">
					        <div class="custom-control custom-checkbox">
					        	<br>
				                <input class="custom-control-input" id="1" name="1" type="checkbox">
				                <label class="custom-control-label" for="1">Checbox</label>
				            </div>

				            <div class="custom-control custom-checkbox">
					        	<br>
				                <input class="custom-control-input" id="2" name="2" type="checkbox" checked="checked">
				                <label class="custom-control-label" for="2">Checked</label>
				            </div>

				            <div class="custom-control custom-checkbox">
					        	<br>
				                <input class="custom-control-input" id="3" name="3" type="checkbox" disabled="disabled">
				                <label class="custom-control-label" for="3">Disabled</label>
				            </div>

				            <div class="custom-control custom-checkbox">
					        	<br>
				                <input class="custom-control-input" id="4" name="4" type="checkbox" disabled="disabled" checked="checked">
				                <label class="custom-control-label" for="4">Disabled Checked</label>
				            </div>
						</div>

						<div class="col-md-6">
					        <div class="custom-control custom-radio">
					        	<br>
				                <input class="custom-control-input" id="5" name="radio" type="radio">
				                <label class="custom-control-label" for="5">Radio</label>
				            </div>

				            <div class="custom-control custom-radio">
					        	<br>
				                <input class="custom-control-input" id="6" name="radio" type="radio" checked="checked">
				                <label class="custom-control-label" for="6">Selected</label>
				            </div>

				            <div class="custom-control custom-radio">
					        	<br>
				                <input class="custom-control-input" id="7" name="radio" type="radio" disabled="disabled">
				                <label class="custom-control-label" for="7">Disabled</label>
				            </div>

				            <div class="custom-control custom-radio">
					        	<br>
				                <input class="custom-control-input" id="8" name="radio2" type="radio" disabled="disabled" checked="checked">
				                <label class="custom-control-label" for="8">Disabled selected</label>
				            </div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-6 alert alert-primary ">
					<p>Alert primary</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-secondary">
					<p>Alert secondary</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-success">
					<p>Alert success</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-info">
					<p>Alert info</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-warning">
					<p>Alert warning</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-danger">
					<p>Alert danger</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-light">
					<p>Alert light</p>
					<hr>
				</div>
				<div  class="col-md-6 alert alert-dark">
					<p>Alert dark</p>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 bg-primary p-4">
					bg primary
					<button type="button" class="bg-primary btn">Button</button>
				</div>
				<div class="col-md-6 bg-secondary p-4">
					bg secondary
					<button type="button" class="bg-secondary btn">Button</button>
				</div>
				<div class="col-md-6 bg-success p-4">
					bg success
					<button type="button" class="bg-success btn">Button</button>
				</div>
				<div class="col-md-6 bg-info p-4">
					bg info
					<button type="button" class="bg-info btn">Button</button>
				</div>
				<div class="col-md-6 bg-warning p-4">
					bg warning
					<button type="button" class="bg-warning btn">Button</button>
				</div>
				<div class="col-md-6 bg-danger p-4">
					bg danger
					<button type="button" class="bg-danger btn">Button</button>
				</div>
				<div class="col-md-6 bg-light p-4">
					bg light
					<button type="button" class="bg-light btn">Button</button>
				</div>
				<div class="col-md-6 bg-dark p-4">
					bg dark
					<button type="button" class="bg-dark btn">Button</button>
				</div>
			</div> --}}
		</div>
	</div>
	@includeWhen($isTenant,'backoffice.dashboard._servicios')

	@includeWhen($isTenant,'backoffice.dashboard._alquileres')
	
	@includeWhen(true,'backoffice.dashboard._mensajes')

	@includeWhen(true,'backoffice.dashboard._contratos-vencer')

	@includeWhen(true,'backoffice.dashboard._contratos-confirmar')

	@includeWhen(true,'backoffice.dashboard._contratos-activos')

	@includeWhen(true,'backoffice.dashboard._contratos-vencidos')

@endsection
