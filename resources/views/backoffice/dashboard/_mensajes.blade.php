<div class="row bg-grey">
	<div class="col-xl-10 col-sm-12 offset-xl-1">
		<div class="row">
			<div class="col-md-12"><h5 class="subtitle">Mensajes e intimaciones</h5></div>
			<div class="col-md-3">
				<div class="pretor-card bg-primary text-light">
					<div>
						<h1>08</h1>
						<span>Mensajes nuevos</span>
					</div>
					<div class="mt-3">
						<a href="" class="rounded-pill btn btn-light">Ver más</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class=" pretor-card bg-primary text-light">
					<div>
						<h1>00</h1>
						<span>Nuevas intimaciones</span>
					</div>
					<div class="mt-3">
						<a href="" class="rounded-pill btn btn-light">Ver más</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>