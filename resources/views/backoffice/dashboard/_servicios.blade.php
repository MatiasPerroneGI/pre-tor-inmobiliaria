<div class="row">
	<div class="col-xl-10 col-sm-12 offset-xl-1">
		<div class="row">
			<div class="col-md-12"><h5 class="subtitle">Servicios a pagar este mes</h5></div>
			<div class="col-md-3">
				<div class="pretor-card bg-primary text-light">
					<div>
						<h1>04</h1>
						<span>Vencidos</span>
					</div>
					<div class="mt-3">
						<a href="" class="rounded-pill btn btn-light">Ver más</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pretor-card bg-primary text-light">
					<div>
						<h1>21</h1>
						<span>A pagar este mes</span>
					</div>
					<div class="mt-3">
						<a href="" class="rounded-pill btn btn-light">Ver más</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pretor-card bg-primary text-light">
					<div>
						<h1>12</h1>
						<span>Para confirmar</span>
					</div>
					<div class="mt-3">
						<a href="" class="rounded-pill btn btn-light">Ver más</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>