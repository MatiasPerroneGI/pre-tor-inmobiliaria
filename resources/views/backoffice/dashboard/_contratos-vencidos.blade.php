<div class="row bg-grey">
	<div class="col-xl-10 col-sm-12 offset-xl-1">
		<div class="row">
			<div class="col-md-12"><h5 class="subtitle">Contratos vencidos</h5></div>
			<div class="col-md-12">
				<table id="datatable" class="table">
					<thead>
						<tr>
							<th>Estado</th>
							<th>Título del contrato</th>
							<th>Propietario</th>
							<th>Locatario</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Por vencer</td>
							<td>Titulo</td>
							<td>Propietario</td>
							<td>Locatario</td>
							<td>
								<a href=""><i class="fal fa-eye fa-lg"></i></a>
							</td>
						</tr>
						<tr>
							<td>Activo</td>
							<td>Titulo</td>
							<td>Propietario</td>
							<td>Locatario</td>
							<td>
								<a href=""><i class="fal fa-eye fa-lg"></i></a>
							</td>
						</tr>
						<tr>
							<td>Vencido</td>
							<td>Titulo</td>
							<td>Propietario</td>
							<td>Locatario</td>
							<td>
								<a href=""><i class="fal fa-eye fa-lg"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>