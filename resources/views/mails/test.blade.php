@component('mail::message')
# {{ $prop1 }}

{{ $prop2 }}

@component('mail::button', ['url' => 'https://google.com'])
Call to action
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent