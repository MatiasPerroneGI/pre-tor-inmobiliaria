@component('mail::message')
Ya confimaron el contrato "{{ $contract->name }}" y entrá en vigencia a partir del día {{ $contract->start->format('d-m-Y') }}.<br>
Podés ver tu contrato en la sección de contratos de tu perfil de admistración.

@component('mail::button', ['url' => url('backoffice/contracts'), 'color' => 'success'])
Mis contratos
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
