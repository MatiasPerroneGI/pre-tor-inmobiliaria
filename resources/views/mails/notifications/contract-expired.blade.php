{{-- TODO: Enrique: revisar redacción  --}}
@component('mail::message')
# {{ $contract->name }}

El contrato de alquiler del inmuelbe ubicado en {{ $contract->property->address }} finalizó.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent