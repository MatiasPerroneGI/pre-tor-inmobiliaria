@component('mail::message')
# {{ $contract->name }}

El contrato de alquiler del inmuelbe ubicado en {{ $contract->property->address }} registra el canon mensual impago. Por favor accedé a Pre-tor para poder abonarlo.

@component('mail::button', ['url' => url('backoffice/payments')])
Pagos y servicios
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent