{{-- TODO: Enrique: revisar redacción  --}}
@component('mail::message')
# {{ $contract->name }}

El contrato de alquiler del inmuelbe ubicado en {{ $contract->property->address }} entra en curso a partir del día de la fecha.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent