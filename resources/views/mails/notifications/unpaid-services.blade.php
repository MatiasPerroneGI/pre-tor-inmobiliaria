@component('mail::message')
# {{ $contract->name }}

El contrato de alquiler del inmuelbe ubicado en {{ $contract->property->address }} registra facturas impagas. Por favor accedé a Pre-tor para poder abonarlas.

@component('mail::button', ['url' => url('backoffice/payments')])
Pagos y servicios
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent