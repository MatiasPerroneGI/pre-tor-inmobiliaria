{{-- TODO: Enrique: revisar redacción  --}}
@component('mail::message')
# {{ $contract->name }}

El contrato de alquiler del inmuelbe ubicado en {{ $contract->property->address }} finalizará el día {{ $contract->end->date('d-m-Y') }}.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent