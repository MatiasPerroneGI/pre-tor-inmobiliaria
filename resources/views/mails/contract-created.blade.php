@component('mail::message')
{{ $contract->owners()->first()->full_name }} Ha creado un contrato donde sos parte.
Entrá en Pre-tor para poder verlo y aceptarlo.

@component('mail::button', ['url' => url('perfil/contracts'), 'color' => 'success'])
Mis contratos
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
