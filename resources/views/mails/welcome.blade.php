@component('mail::message')
# Bienvenido/a {{ $user->name }}

Te damos la bienvenida a Pre-tor.

¡Gracias!<br>
{{ config('app.name') }}
@endcomponent
