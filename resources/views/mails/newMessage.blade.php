@component('mail::message')
# Nuevo mensaje

<p>{{ $message->user->name }}<small> ({{$message->user->email}})</small></p>
<strong>Asunto:</strong> {{ $message->conversation->subject }} <br>
<strong>Mensaje:</strong> {!! nl2br($message->message) !!}

@component('mail::button', ['url' => route('chat.answer',$message->id),'color' => 'success'])
	Responder
@endcomponent

<br>
<small style="font-size: 11px">No responder este correo.</small>

@endcomponent