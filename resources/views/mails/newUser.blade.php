@component('mail::message')

Hola {{ $user->name }}, <br>
    Bienvenido a {{ config('app.name') }}
    Podes entrar a Pre-Tor ingresando en el botón<br>
    <br>
    Tus datos para ingresar: <br>
    Usuario: {{ $user->email }} <br>
    Contraseña: {{ $pass }}<br>

@component('mail::button', ['url' => config('app.url').'/backoffice','color' => 'success'])
    Ingresar
@endcomponent

<br>
¡Gracias!<br>
{{ config('app.name') }}

@endcomponent