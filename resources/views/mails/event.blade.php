@component('mail::message')
Nuevo evento
<br>
Asunto: {{ $event->subject }} <br>
Mensaje: {!! nl2br($event->message) !!}

¡Gracias!<br>
{{ config('app.name') }}
@endcomponent
