@component('mail::message')
Hola {{$user->name}} {{$user->lastname}}

Te hemos creado un usuario con el mail {{$user->email}}

@component('mail::button', ['url' => url('perfil/crear-password/' . $token), 'color' => 'success'])
Creá una contraseña para tu usuario
@endcomponent

¡Gracias!<br>
{{ config('app.name') }}
@endcomponent
