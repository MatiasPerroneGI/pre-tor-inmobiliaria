@component('mail::message')
    Actualización del plan {{ $plan->name }}
    <br>
	    @foreach($changed as $change => $value)
			{{trans('kameform.'.$change)}}: {{$plan->$change}} <br>
	    @endforeach
	<br>

¡Gracias!<br>
{{ config('app.name') }}
@endcomponent
