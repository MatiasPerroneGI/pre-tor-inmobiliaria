Las PARTES por el presente acuerdan que desde la firma del CONTRATO y hasta la extinción de la relación locativa emergente del mismo, únicamente serán válidas las notificaciones electrónicas que las mismas se envíen por medio de la Plataforma de Administración Contractual, salvo que por motivos imputables a la referida plataforma, el envío de notificaciones no resulte posible. En dicho caso la parte que haya empleado un método de notificación diferente, se compromete a dejar registro en la Plataforma de Administración Contractual el contenido de la misma, una vez que su funcionamiento se haya restituido.



Las PARTES acuerdan registrar el CONTRATO en la Plataforma de Administración Contractual bajo la denominación de {{$contract->name}}, y concentrar bajo dicho registro la totalidad de notificaciones cursadas con objeto de la relación contractual regida por el presente, las que solo podrán ser remitidas a las siguientes direcciones:
    -   LOCADOR/A: {{$contract->owners->first()->email}}.

    -   LOCATARIO/A: {{$contract->tenants->first()->email}}.

    -   FIADOR/A: {{$contract->guarantors->first()->email}}.


La falta de acceso y revisión a la Plataforma de Administración Contractual, no obstaculizará el perfeccionamiento de la notificación en cuestión, siempre y cuando, la referida plataforma confirme que el mensaje fue efectivamente receptado por el destinatario del mismo.


DOMICILIO: Para todos los efectos legales y judiciales los firmantes constituyen los siguientes domicilios especiales, en los que serán válidas las notificaciones y diligencias que se practiquen en el supuesto que no puedan ser practicadas electrónicamente a través de la Plataforma de Administración Contractual por razones imputables a esta última:


    -   LOCADOR/A: {{$contract->owners->first()->pivot->legal_address}}.

    -   LOCATARIO/A: {{$contract->tenants->first()->pivot->legal_address}}.

    -   FIADOR/A: {{$contract->guarantors->first()->legal_address}}.
                