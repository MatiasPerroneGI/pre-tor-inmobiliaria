El precio total de la locación se fija en la suma de Pesos {{$contract->price}}; los que serán abonados por el/la LOCATARIO/A de la siguiente manera:
@foreach ($contract->periods as $period)
    ({{ chr(64 + $period->step) }}) Para el período comprendido entre el {{ $period->start->format('d-m-Y')}} y el {{$period->end->format('d-m-Y') }}, la suma de {{ $period->price }}.
@endforeach
en la suma de {{$contract->total}}.

El canon locativo correspondiente al primer mes será abonado por el/la LOCATARIO/A al momento de la firma del CONTRATO, sirviendo el presente de suficiente recibo. Con relación a cada canon posterior, el/la LOCATARIO/A deberá dejar constancia de su pago en la Plataforma de Administración Contractual mediante la carga del comprobante correspondiente.

Las PARTES acuerdan que el/la LOCATARIO/A deberán abonar el canon en efectivo por mes adelantado y del 1 al 10 de cada mes. Los pagos deberán realizarse [

{{-- TODO: resolver, todavía no está integrada esta lógica --}}
@if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'TR')
        -   Mediante deposito o transferencia bancaria a la Cuenta N° [___] del Banco [___], CBU [___], a nombre de [___], o en la cuenta bancaria donde éste indique en el futuro.
@endif

@if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'MP')
        -   A través del sistema de pagos Mercado Pago, desde la plataforma de la Plataforma de Administración Contractual (conforme es definida más adelante), tras el registro y confirmación del CONTRATO.
@endif

@if ($contract->owners->first()->accounts->count() && $contract->owners->first()->accounts->first()->type == 'EF')
        -   En efectivo el domicilio de el/la LOCADOR/A o donde ellos lo indiquen en el futuro.
@endif
]

El precio se pacta por mes completo y no es divisible, por lo que si el CONTRATO fuera resuelto por cualquier causa en un día que no coincida con el último del mes, el precio devengado será del canon mensual entero y no una fracción del mismo. Dicha circunstancia se mantendrá en caso de que el/la LOCATARIO/A se mantengan en la ocupación del INMUEBLE luego de que haya vencido el plazo para su devolución.

MULTA.  El mero vencimiento del plazo sin que se efectúe el pago hará incurrir a el/la LOCATARIO/A en mora de pleno derecho, por lo que deberá abonar -por dicho atraso- intereses moratorios equivalentes al {{$contract->penalty}} %, los que se calcularan diariamente sobre el monto adeudado desde la producción de la mora.