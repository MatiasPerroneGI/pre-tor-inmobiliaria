El/la LOCATARIO/A asume a su cargo el pago de todos los servicios utilizados por el INMUEBLE, a saber: {{ $contract->services->pluck('value')->join(', ') }} Las obligaciones subsistirán hasta la efectiva restitución del INMUEBLE, con la conformidad del LOCADOR/A.


El pago de los servicios e impuestos indicados son considerados por las PARTES como parte integrante de la prestación dineraria asumida por el/la LOCATARIO/A en virtud del presente contrato, en los términos del art. 1219 del Código Civil y Comercial.
Al momento de efectuar el pago mensual de los cánones locativos, el/la LOCATARIO/A está obligado/a a acreditar en la Plataforma de Administración Contractual el pago de los servicios e impuestos indicados cuya fecha de vencimiento se corresponda con la del vencimiento mensual del cánon locativo a pagar.


En caso de que el/la LOCATARIO/A no registre en la Plataforma de Administración Contractual los comprobantes de pago de los servicios e impuestos requeridos, el/la LOCADOR/A no estará obligado/a a recibir el pago del canon locativo. En ese caso se considerará incumplido el pago del mes o los meses respectivos, haciendo pasible a el/la LOCATARIO/A de todas las consecuencias previstas en el presente CONTRATO ante dicho incumplimiento.


La recepción por parte de el/la LOCADOR/A del canon locativo no será entendida como la recepción de las constancias de pago de los conceptos previstos en esta cláusula, ni podrá bajo ningún punto de vista considerarse como una dispensa de dicha obligación del/de la LOCATARIO/A.


@if ($contract->commerce)
    HABILITACIONES.. El/La LOCATARIO/A se obliga a gestionar, mantener y pagar puntualmente cualquier otro gasto, tasa, impuesto y/o contribuciones necesarias y/o derivados de la obtención de las habilitaciones necesarias para el funcionamiento y/o ejercicio de la o las actividades comerciales a ser llevadas a cabo en el INMUEBLE. Las respectivas habilitaciones deberán ser obtenidas por el/la LOCATARIO/A a su exclusiva cuenta y riesgo, por lo que la demora en su tramitación o la negativa de su otorgamiento serán soportadas únicamente por este/a último/a, no pudiendo realizar reclamo alguno a el/la LOCADORA/A. El/La LOCATARIO/A se obliga a mantener indemne al/ a la LOCADOR/A por el pago de cualquier gravamen, tasa o impuesto derivado del desarrollo de la actividad comercial del/de la LOCATARIO/A en el INMUEBLE.
@endif

Para el supuesto que el/la LOCATARIO/A contrate la prestación de servicios distintos a los indicados anteriormente (telefonía, televisión,
internet, etc.), será responsable tanto de su alta como de la baja respectiva, y deberá acreditar ambas circunstancias en la Plataforma de 
Administración Contractual, antes de la restitución del INMUEBLE al/ a la LOCADOR/A, cualquiera sea causa por la que finalice la relación locativa.