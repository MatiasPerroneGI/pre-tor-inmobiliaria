-  Y Considerando:
-   1°.Que el/la LOCADOR/A es propietario/a de un inmueble ubicado en calle {{$contract->property->address}}. Y que es su intención arrendarlo para fines de {{!$contract->commerce? 'vivienda': 'comercio'}}.
-   2°. Que el/la LOCATARIO/A, en este acto declara conocer la intención de el/la
 LOCADOR/A y, a su vez, manifiesta su interés de locar el inmueble de propiedad de este/a último/a. 
En consecuencia, las PARTES de común acuerdo convienen celebrar el presente CONTRATO DE LOCACIÓN (el “CONTRATO”), que se regirá por el Código Civil y Comercial, los considerandos anteriores y las cláusulas que siguen: