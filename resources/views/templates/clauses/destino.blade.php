El/la LOCADOR/A loca el INMUEBLE al LOCATARIO/A, quien lo acepta en tal concepto, manifestando conocer el estado del mismo por haber revisado el mismo de forma exhaustiva, y se obliga a reintegrarlo a la finalización de la relación locativa, ya sea por vencimiento del plazo, resolución anticipada, rescisión, disposición judicial o cualquier otra causa, en igual estado de uso y conservación en que lo recibió, con más todas y cada una de las mejoras e incorporaciones de bienes realizadas con posterioridad, siendo responsabilidad suya, en forma exclusiva, la reparación y/o reposición de toda estructura, elemento y/o artefacto que falte y/o se encuentre deteriorado al mencionado tiempo de finalización.
                
DESTINO:
@if (!$contract->commerce)
    El/la LOCATARIO/A afectará el INMUEBLE para exclusivos fines de vivienda, no pudiendo darle un uso distinto sin el consentimiento previo y escrito del/la LOCADOR/A.
@else
    El/la LOCATARIO/A afectará el INMUEBLE para el desarrollo de,su actividad comercial consistente en {{$contract->activity}}, no pudiendo darle un uso distinto sin el consentimiento previo y escrito del/la LOCADOR/A.
@endif