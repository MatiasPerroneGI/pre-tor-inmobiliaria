Que el/la LOCADOR/A son propietarios de un inmueble situado en la dirección indicada en el considerando 1°, el cual consta de un total de {{$contract->property->area}} m2 (en adelante “el INMUEBLE”).

A)  DESCRIPCIÓN DEL INMUEBLE: {{$contract->property->description}}.

B)  DESCRIPCIÓN DE ARTEFACTOS Y ACCESORIOS: El INMUEBLE a su vez cuanta con las siguientes instalaciones: {{$contract->property->accesories}}.

El INMUEBLE se entrega en perfectas condiciones y, con todas sus dependencias y bienes en perfecto estado de conservación, funcionamiento y limpieza.