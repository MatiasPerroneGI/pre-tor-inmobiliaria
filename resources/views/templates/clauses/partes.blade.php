Entre:
-   @foreach ($contract->owners as $owner)
    El/La Sr./a. {{$owner->lastname}} ,{{$owner->name}}  , D.N.I. {{$owner->dni}}, con domicilio en calle {{$owner->pivot->legal_address}} y
    @endforeach
    {{count($contract->owners) > 1? 'en adelante denominados/as   “LOCADORES/AS”;':'en adelante denominado/a   “LOCADOR/A”; '}}
    
-   @foreach ($contract->tenants as $tenant)
    El/La Sr./a. {{$tenant->lastname}} ,{{$tenant->name}}  , D.N.I. {{$tenant->dni}}, con domicilio en calle {{$tenant->pivot->legal_address}} y
    @endforeach
    {{count($contract->tenants) > 1? 'en adelante denominados/as “LOCATARIOS/AS”;':'en adelante denominado/a “LOCATARIO/A”;'}} 
-   En lo sucesivo, al referirse en forma conjunta a LOCADOR/A y LOCATARIO/A, se lo hará bajo la locución “PARTES”;