<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ContractTest extends TestCase
{
    use WithFaker;

    public function testCreate()
    {
        $data = [
            'contract' => [
                'name' => $this->faker->sentence(3),
                'pay_method' => 'MP',
                'place' => $this->faker->city,
                'jurisdiction' => $this->faker->city,
                'duration' => 12,
                'start' => '01-10-2019',
                'end' => '01-10-2020',
                'price' => 10000,
                'deposit_time' => 1,
                'deposit_value' => 10000,
                'increase' => 10,
                'time' => 6,
                'total' => 136000,
                'anticipated_payment' => $this->faker->boolean,
                'penalty' => 10,
            ],
            'owner_address' => $this->faker->address,
            'owners' =>  [
                [
                    'dni' => $this->faker->numberBetween(20000000, 40000000),
                    'email' => $this->faker->email,
                    'name' => $this->faker->firstName,
                    'lastname' => $this->faker->lastName,
                    'cuit' => $this->faker->numerify('##-########-#'),
                ],
                [
                    'dni' => $this->faker->numberBetween(20000000, 40000000),
                    'email' => $this->faker->email,
                    'name' => $this->faker->firstName,
                    'lastname' => $this->faker->lastName,
                    'cuit' => $this->faker->numerify('##-########-#'),
                ]
            ],
            'tenants' =>  [
                [
                    'dni' => $this->faker->numberBetween(20000000, 40000000),
                    'email' => $this->faker->email,
                    'name' => $this->faker->firstName,
                    'lastname' => $this->faker->lastName,
                    'cuit' => $this->faker->numerify('##-########-#'),
                ],
                [
                    'dni' => $this->faker->numberBetween(20000000, 40000000),
                    'email' => $this->faker->email,
                    'name' => $this->faker->firstName,
                    'lastname' => $this->faker->lastName,
                    'cuit' => $this->faker->numerify('##-########-#'),
                ]
            ],
            'property' => [
                'address' => $this->faker->address,
                'area' => $this->faker->numberBetween(40, 400),
                'description' => $this->faker->sentence,
                'accessories' => $this->faker->sentence,
                'activity' => $this->faker->sentence,
            ],
            'services' => $this->faker->randomElements([1,2,3,4,5], 3),
            'guarantors' =>  [
                [
                    'type' => 3,
                    'name' => $this->faker->firstName,
                    'lastname' => $this->faker->lastName,
                    'legal_address' => $this->faker->address,
                    'email' => $this->faker->email,
                    'dni' => $this->faker->numberBetween(20000000, 40000000),
                ],
            ],
            'has_fire_insurance' =>  0,
            'has_pet' =>  0,
            'extra_data' =>  $this->faker->paragraph,

        ];

        $user = \App\User::find(1);

        $this->actingAs($user)->post('backoffice/contracts', $data)->assertRedirect('backoffice/contracts#new');
    }
}
